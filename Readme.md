# Locals Atlas Backend

This is the backend of the locals atlas app

## Installation for development

Use the nodejs package manager to install Locals Atlas Backend.

```bash
npm install
```

## Packages

The following packages were used

[expressjs](https://expressjs.com/)
[mongoosejs](https://mongoosejs.com/)
[bcryptjs](https://www.npmjs.com/package/bcryptjs)
[jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
[express-async-handler](https://www.npmjs.com/package/express-async-handler/v/1.1.4)
[dotenv](https://www.npmjs.com/package/dotenv)
[cors](https://www.npmjs.com/package/cors)
[multer](https://www.npmjs.com/package/multer)
[morgan](https://www.npmjs.com/package/morgan)
[nodemon](https://www.npmjs.com/package/nodemon)
[sendgrid](https://sendgrid.com/)