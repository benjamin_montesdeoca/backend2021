const express = require("express");
const app = express();
const path = require("path");
const dotenv = require("dotenv");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");
const { Server } = require("socket.io");

const connectDB = require("./config/db");
const { notFoundHandler, errorHandler } = require("./middleware/error");

const categoryRoutes = require("./routers/category");
const countryRoutes = require("./routers/country");
const userRoutes = require("./routers/user");
const profileRoutes = require("./routers/profile");
const businessRoutes = require("./routers/business");
const recommedationRoutes = require("./routers/recommedation");
const requestRoutes = require("./routers/request");
const friendRoutes = require("./routers/friend");
const claimRoutes = require("./routers/claim");
const reportRoutes = require("./routers/report");
const feeRoutes = require("./routers/fee");
const notificationRoutes = require("./routers/notification");
const trackingRoutes = require("./routers/tracking");
const adsRoutes = require("./routers/advertisement");
const extraRoutes = require("./routers/extra");
const versionRoutes = require("./routers/version");
const globalRoutes = require("./routers/global");
const errorRoutes = require("./routers/error");
const socialRouter = require("./routers/social");

dotenv.config();

connectDB();

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

const options = {
  origin: "*",
};

app.use(cors(options));

const server = http.createServer(app);

const io = new Server(server);

// app.use(express.json());
app.use(bodyParser.json());
// app.use(express.urlencoded({ extended: false }));

app.use("/api/categories", categoryRoutes);
app.use("/api/countries", countryRoutes);
app.use("/api/users", userRoutes);
app.use("/api/profiles", profileRoutes);
app.use("/api/businesses", businessRoutes);
app.use("/api/recommendations", recommedationRoutes);
app.use("/api/requests", requestRoutes);
app.use("/api/friends", friendRoutes);
app.use("/api/claims", claimRoutes);
app.use("/api/reports", reportRoutes);
app.use("/api/notifications", notificationRoutes);
app.use("/api/trackings", trackingRoutes);
app.use("/api/fees", feeRoutes);
app.use("/api/ads", adsRoutes);
app.use("/api/versions", versionRoutes);
app.use("/api/globals", globalRoutes);
app.use("/api/errors", errorRoutes);
app.use("/api/socials", socialRouter);

// EXTRAS
app.use("/api/extras", extraRoutes);

app.get("/", (req, res) => {
  res.send("API is running");
});

app.use(
  "/public/images",
  express.static(path.join(path.resolve(), "public/images"))
);

app.use(notFoundHandler);
app.use(errorHandler);

io.on("connection", (socket) => {
  console.log("connected");

  socket.on("disconnect", () => {
    console.log("User disconnected", socket.id);
  });
});

const PORT = process.env.PORT || 5000;

server.listen(PORT || 5000, () => {
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`);
});
