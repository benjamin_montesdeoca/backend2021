const asyncHandler = require("express-async-handler");

const Advertisement = require("../models/advertisement");

exports.createBusinessAd = asyncHandler(async (req, res) => {
  let { business, priority, description } = req.body;

  const ad = await Advertisement.create({
    business: business,
    priority: priority,
    description: description,
  });

  if (ad) {
    res.json(ad);
  } else {
    res.status(400);
    throw new Error("Ad was not created");
  }
});
