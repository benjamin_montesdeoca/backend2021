const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");
const { Expo } = require("expo-server-sdk");
const fs = require("fs");
require("dotenv").config();

const notifications = require("../helpers/notifications");
const sendPushNotification = require("../utils/pushToken");
const {
  stringIsAValidUrl,
  stringIsSocialUrl,
} = require("../utils/generalFunctions");

const User = require("../models/user");
const UserTracking = require("../models/userTracking");
const Business = require("../models/business");
const BusinessAudit = require("../models/businessAudit");
const BlackList = require("../models/blacklist");
const Recommendation = require("../models/recommendation");
const Tag = require("../models/tag");
const Global = require("../models/global");

const Notification = require("../models/notification");
const AdminAudit = require("../models/adminAudit");

// ######################################################################
// User APIs
// ######################################################################
//#region
exports.createBusiness = asyncHandler(async (req, res) => {
  let {
    name,
    description,
    mainAddress,
    phone,
    website,
    latitude,
    longitude,
    address,
  } = req.body;

  const businessExists = await Business.findOne({ name: name });

  if (businessExists) {
    res.status(400);
    throw new Error("Business already exists");
  }

  const nameArray = name.split(" ");
  const descriptionArray = description.split(" ");

  let behavior = 1;
  let usingBadWords = false;
  let newName = [];
  let newDescription = [];

  for (let index = 0; index < nameArray.length; index++) {
    const element = nameArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newName.push("****");
      usingBadWords = true;
    } else {
      newName.push(element);
    }
  }

  for (let index = 0; index < descriptionArray.length; index++) {
    const element = descriptionArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newDescription.push("****");
      usingBadWords = true;
    } else {
      newDescription.push(element);
    }
  }

  if (usingBadWords) {
    behavior = 2;
  } else {
    behavior = 1;
  }

  const business = await Business.create({
    name: newName.join(" "),
    description: newDescription.join(" "),
    mainAddress,
    phone,
    website,
    location: {
      type: "Point",
      coordinates: [longitude, latitude],
    },
    behavior: behavior,
    address,
    userCreator: req.user._id,
    ownerUser: process.env.ADMIN_USER_ID,
  });

  if (business) {
    res.status(201).json(business);
  } else {
    res.status(400);
    throw new Error("Business not created");
  }
});

exports.createBusinessPreLaunch = asyncHandler(async (req, res) => {
  let { name, description, mainAddress, phone, website, latitude, longitude } =
    req.body;

  const businessExists = await Business.findOne({ name: name });

  if (businessExists) {
    res.status(400);
    throw new Error("Business already exists");
  }

  const nameArray = name.split(" ");
  const descriptionArray = description.split(" ");

  let behavior = 1;
  let usingBadWords = false;
  let newName = [];
  let newDescription = [];

  for (let index = 0; index < nameArray.length; index++) {
    const element = nameArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newName.push("****");
      usingBadWords = true;
    } else {
      newName.push(element);
    }
  }

  for (let index = 0; index < descriptionArray.length; index++) {
    const element = descriptionArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newDescription.push("****");
      usingBadWords = true;
    } else {
      newDescription.push(element);
    }
  }

  if (usingBadWords) {
    behavior = 2;
  } else {
    behavior = 1;
  }

  const business = await Business.create({
    name: newName.join(" "),
    description: newDescription.join(" "),
    mainAddress: mainAddress,
    phone,
    website,
    location: {
      type: "Point",
      coordinates: [longitude, latitude],
    },
    behavior: behavior,
    address: mainAddress,
    userCreator: process.env.ADMIN_USER_ID,
    ownerUser: process.env.ADMIN_USER_ID,
  });

  if (business) {
    res.redirect("https://portal.localsatlas.com/download");
  } else {
    res.status(400);
    throw new Error("Business not created");
  }
});

exports.updateBusinessOpeningHours = asyncHandler(async (req, res) => {
  let { openingHours } = req.body;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  await Business.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        openingHours: openingHours,
      },
    },
    { new: true, upsert: true }
  );

  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.status(201).json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business not updated");
  }
});

exports.updateBusinessCategoriesAndTags = asyncHandler(async (req, res) => {
  let { categories, tags } = req.body;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  let behavior = 1;
  let isNewCategory = false;
  let categoriesBusinessIds = [];
  let categoriesIds = [];
  let tagsIds = [];

  if (businessExists.behavior !== 2) {
    for (let index = 0; index < tags.length; index++) {
      const element = tags[index];

      const tagExists = await Tag.findOne({ name: element });

      if (tagExists) {
        tagsIds.push(tagExists._id.toString());
      } else {
        const existBadword = await BlackList.findOne({
          word: element.toString().toLowerCase(),
        });

        let tag;

        if (existBadword) {
          behavior = 2;
        } else {
          isNewTag = true;
          tag = await Tag.create({
            name: element,
          });
          tagsIds.push(tag._id.toString());
          behavior = 1;
        }
      }
    }

    await Business.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          categories: categories,
          tags: tagsIds,
          behavior: behavior,
        },
      },
      { new: true, upsert: true }
    );
  } else {
    for (let index = 0; index < tags.length; index++) {
      const element = tags[index];

      const tagExists = await Tag.findOne({ name: element });

      if (tagExists) {
        tagsIds.push(tagExists._id.toString());
      } else {
        const existBadword = await BlackList.findOne({
          word: element.toString().toLowerCase(),
        });

        let tag;

        if (existBadword) {
          behavior = 2;
        } else {
          tag = await Tag.create({
            name: element,
          });
          tagsIds.push(tag._id.toString());
          behavior = 1;
        }
      }
    }

    await Business.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          categories: categories,
          tags: tagsIds,
        },
      },
      { new: true, upsert: true }
    );
  }

  let difference = businessExists.tags.filter((x) => !tagsIds.includes(x));

  if (
    difference.length === 0 ||
    businessExists.tags.length !== tagsIds.length
  ) {
    await BusinessAudit.create({
      business: businessExists._id,
      field: "tags",
      oldValues: businessExists.tags,
      newValues: tagsIds,
      type: 1,
    });
  }

  if (businessExists.categories.length >= 1) {
    for (let index = 0; index < categories.length; index++) {
      const element = categories[index];
      switch (index) {
        case 0:
          if (
            element.category.toString() !==
            businessExists.categories[0].category.toString()
          ) {
            isNewCategory = true;
          }
          break;
        case 1:
          if (
            element.category.toString() !==
            businessExists.categories[1].category.toString()
          ) {
            isNewCategory = true;
          }
        default:
          break;
      }
    }
  } else {
    isNewCategory = true;
  }

  if (isNewCategory) {
    for (let index = 0; index < businessExists.categories.length; index++) {
      const category = businessExists.categories[index];
      categoriesBusinessIds.push(category.category);
      for (
        let index = 0;
        index < businessExists.category.subcategories.length;
        index++
      ) {
        const subcategory = businessExists.category.subcategories[index];
        categoriesBusinessIds.push(subcategory.subcategory);
      }
    }

    for (let index = 0; index < categories.length; index++) {
      const category = categories[index];
      categoriesIds.push(category.category);
      for (let index = 0; index < category.subcategories.length; index++) {
        const subcategory = category.subcategories[index];
        categoriesIds.push(subcategory.subcategory);
      }
    }

    await BusinessAudit.create({
      business: businessExists._id,
      field: "categories",
      oldValues: categoriesBusinessIds,
      newValues: categoriesIds,
      type: 1,
    });
  }

  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.status(201).json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business not updated");
  }
});

exports.updateBusinessSocials = asyncHandler(async (req, res) => {
  let { socialLinks } = req.body;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const social = [];

  for (let index = 0; index < socialLinks.length; index++) {
    const element = socialLinks[index];

    if (stringIsAValidUrl(element.url) && stringIsSocialUrl(element.url)) {
      social.push({
        social: element.social,
        name: element.name,
        url: element.url,
        image: element.image,
      });
    }
  }

  await Business.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        socialLinks: social,
      },
    },
    { new: true, upsert: true }
  );

  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.status(201).json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business not updated");
  }
});

exports.updateBusiness = asyncHandler(async (req, res) => {
  let {
    name,
    description,
    mainAddress,
    phone,
    website,
    latitude,
    longitude,
    openingHours,
    socialLinks,
    address,
    categories,
    tags,
  } = req.body;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const nameArray = name.split(" ");
  const descriptionArray = description.split(" ");

  let auditArray = [];
  let isNewCategory = false;
  let categoriesBusinessIds = [];
  let categoriesIds = [];
  let tagsIds = [];

  if (businessExists.name.toString() !== name.toString()) {
    auditArray.push({
      business: req.params.id,
      field: "name",
      oldValues: [businessExists.name],
      newValues: [name],
      action: 1,
    });
  }

  if (businessExists.description.toString() !== description.toString()) {
    auditArray.push({
      business: req.params.id,
      field: "description",
      oldValues: [businessExists.description],
      newValues: [description],
      action: 1,
    });
  }

  if (businessExists.mainAddress.toString() !== mainAddress.toString()) {
    auditArray.push({
      business: req.params.id,
      field: "mainAddress",
      oldValues: [businessExists.mainAddress],
      newValues: [mainAddress],
      action: 1,
    });
  }

  if (businessExists.phone.toString() !== phone.toString()) {
    auditArray.push({
      business: req.params.id,
      field: "phone",
      oldValues: [businessExists.phone],
      newValues: [phone],
      action: 1,
    });
  }

  if (businessExists.website.toString() !== website.toString()) {
    auditArray.push({
      business: req.params.id,
      field: "website",
      oldValues: [businessExists.website],
      newValues: [website],
      action: 1,
    });
  }

  if (
    businessExists.location.coordinates[0].toString() !== longitude.toString()
  ) {
    auditArray.push({
      business: req.params.id,
      field: "address",
      oldValues: [
        businessExists.location.coordinates[1],
        businessExists.location.coordinates[0],
        businessExists.address,
      ],
      newValues: [latitude, longitude, address],
      action: 1,
    });
  }

  let usingBadWords = false;
  let behavior = 1;
  let newName = [];
  let newDescription = [];
  let newSocial = [];

  for (let index = 0; index < nameArray.length; index++) {
    const element = nameArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newName.push("****");
      usingBadWords = true;
    } else {
      newName.push(element);
    }
  }

  for (let index = 0; index < descriptionArray.length; index++) {
    const element = descriptionArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newDescription.push("****");
      usingBadWords = true;
    } else {
      newDescription.push(element);
    }
  }

  if (usingBadWords) {
    behavior = 2;
  } else {
    behavior = 1;
  }

  for (let index = 0; index < tags.length; index++) {
    const element = tags[index];

    const tagExists = await Tag.findOne({ name: element });

    if (tagExists) {
      tagsIds.push(tagExists._id);
    } else {
      const existBadword = await BlackList.findOne({
        word: { $regex: new RegExp(`${element}`), $options: "i" },
      });

      let tag;

      if (!existBadword) {
        tag = await Tag.create({
          name: element,
        });
        tagsIds.push(tag._id);
        behavior = 1;
      }
    }
  }

  for (let index = 0; index < socialLinks.length; index++) {
    const element = socialLinks[index];
    newSocial.push({
      social: element.social,
      name: element.name,
      url: element.url,
      image: element.image,
    });
  }

  await Business.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        name: newName.join(" "),
        description: newDescription.join(" "),
        mainAddress: mainAddress,
        phone: phone,
        website: website,
        location: {
          type: "Point",
          coordinates: [longitude, latitude],
        },
        openingHours: openingHours,
        socialLinks: newSocial,
        address: address,
        tags: tagsIds,
        categories: categories,
        behavior: behavior,
      },
    }
  );

  if (auditArray.length > 0) {
    for (let index = 0; index < auditArray.length; index++) {
      const element = auditArray[index];
      await BusinessAudit.create(element);
    }
  }

  let difference = businessExists.tags.filter((x) => !tagsIds.includes(x));

  if (difference.length === 0) {
    await BusinessAudit.create({
      business: businessExists._id,
      field: "tags",
      oldValues: businessExists.tags,
      newValues: tagsIds,
      type: 1,
    });
  }

  if (businessExists.tags.length !== tagsIds.length) {
    await BusinessAudit.create({
      business: businessExists._id,
      field: "tags",
      oldValues: businessExists.tags,
      newValues: tagsIds,
      type: 1,
    });
  }

  if (businessExists.categories.length >= 1) {
    for (let index = 0; index < categories.length; index++) {
      const element = categories[index];
      switch (index) {
        case 0:
          if (
            element.category.toString() !==
            businessExists.categories[0].category.toString()
          ) {
            isNewCategory = true;
          }
          break;
        case 1:
          try {
            if (
              element.category.toString() !==
              businessExists.categories[1].category.toString()
            ) {
              isNewCategory = true;
            }
          } catch (error) {}
        default:
          break;
      }
    }
  } else {
    isNewCategory = true;
  }

  if (isNewCategory) {
    for (let index = 0; index < businessExists.categories.length; index++) {
      const category = businessExists.categories[index];
      categoriesBusinessIds.push(category.category);
      for (
        let index = 0;
        index < businessExists.category.subcategories.length;
        index++
      ) {
        const subcategory = businessExists.category.subcategories[index];
        categoriesBusinessIds.push(subcategory.subcategory);
      }
    }

    for (let index = 0; index < categories.length; index++) {
      const category = categories[index];
      categoriesIds.push(category.category);
      for (let index = 0; index < category.subcategories.length; index++) {
        const subcategory = category.subcategories[index];
        categoriesIds.push(subcategory.subcategory);
      }
    }

    await BusinessAudit.create({
      business: businessExists._id,
      field: "categories",
      oldValues: categoriesBusinessIds,
      newValues: categoriesIds,
      type: 1,
    });
  }

  const user = await User.findById(req.user._id);

  const global = await Global.findOne({ user: req.user._id });

  if (behavior === 2) {
    if (Expo.isExpoPushToken(user.notificationToken)) {
      await sendPushNotification(
        user.notificationToken,
        notifications.notificationPushMessage(global.language, 1)
      );
    }

    await Notification.create({
      user: req.user._id,
      title: notifications.notificationTitle(2),
      message: notifications.notificationMessage(2, businessExists.name),
      isState: 0,
      type: 2,
    });

    await Notification.create({
      user: process.env.ADMIN_USER_ID,
      title: notifications.notificationTitleForAdmin(2),
      message: notifications.notificationMessage(2, businessExists.name),
      review: `${businessExists._id}`,
      isState: 0,
      type: 2,
    });
  }

  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        openingHours: 1,
        socialLinks: 1,
        mainAddress: 1,
        phone: 1,
        website: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business not updated");
  }
});

exports.updateBusinessImage = asyncHandler(async (req, res) => {
  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const file = req.file;

  if (!file) {
    res.status(404);
    throw new Error("Image not found");
  }

  const basePath = "public/images/businesses/";
  const fileName = file.filename;

  await Business.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { image: `${basePath}${fileName}` } },
    { new: true, upsert: true }
  );

  if (businessExists.behavior === 2) {
    await Notification.create({
      user: req.user._id,
      title: "Business temporarily banned",
      message: `Your business "${business.name}" has been reported for using offensive words`,
      isState: 0,
      type: 2,
    });

    await Notification.create({
      user: process.env.ADMIN_USER_ID,
      title: "Business reported",
      message: `The business called "${business.name}" has been reported for using offensive words`,
      review: `${business._id}`,
      isState: 0,
      type: 2,
    });
  } else {
    await Notification.create({
      user: req.user._id,
      title: notifications.notificationTitle(1),
      message: notifications.notificationMessage(1, businessExists.name),
      isState: 0,
      type: 1,
    });
  }

  await BusinessAudit.create({
    business: req.params.id,
    field: "image",
    oldValues: [businessExists.image],
    newValues: [`${basePath}${fileName}`],
    action: 1,
  });

  const business = await Business.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business image was not updated");
  }
});

exports.updateBusinessImages = asyncHandler(async (req, res) => {
  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const files = req.files;

  if (!files) {
    res.status(404);
    throw new Error("Images not found");
  }

  const basePath = "public/images/businesses/";
  const images = [];

  for (let index = 0; index < files.length; index++) {
    const element = files[index];
    images.push(`${basePath}${element.filename}`);
  }

  await Business.findOneAndUpdate(
    { _id: req.params.id },
    { $push: { images: images } },
    { new: true, upsert: true }
  );

  await BusinessAudit.create({
    business: req.params.id,
    field: "images",
    oldValues: businessExists.images,
    newValues: images,
    action: 1,
  });

  const business = await Business.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        website: 1,
        phone: 1,
        address: 1,
        openingHours: 1,
        socialLinks: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business images were not updated");
  }
});

exports.deleteBusinessImages = asyncHandler(async (req, res) => {
  let { images } = req.body;

  await Business.findOneAndUpdate(
    { _id: req.params.id },
    { $pull: { images: { $in: images } } },
    { multi: true }
  );

  for (let index = 0; index < images.length; index++) {
    const element = images[index];

    fs.stat(`./${element}`, function (err, stats) {
      if (err) {
        return console.error(err);
      }

      fs.unlink(`./${element}`, function (err) {
        if (err) return console.log(err);
        console.log("File deleted successfully");
      });
    });
  }

  const business = await Business.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        website: 1,
        phone: 1,
        address: 1,
        openingHours: 1,
        socialLinks: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business) {
    res.json(business[0]);
  } else {
    res.status(400);
    throw new Error("Business images were not updated");
  }
});

exports.getAllMyBusiness = asyncHandler(async (req, res) => {
  const myCreatedBusiness = await Business.find({
    $and: [
      { userCreator: req.user._id },
      { ownerUser: mongoose.Types.ObjectId(process.env.ADMIN_USER_ID) },
    ],
  });

  const myClaimedBusiness = await Business.find({
    $and: [{ ownerUser: req.user._id }],
  });

  const businesses = [];

  for (let index = 0; index < myCreatedBusiness.length; index++) {
    const element = myCreatedBusiness[index];
    businesses.push(element._id);
  }

  for (let index = 0; index < myClaimedBusiness.length; index++) {
    const element = myClaimedBusiness[index];
    businesses.push(element._id);
  }

  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: { $in: businesses },
          },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business.length > 0) {
    res.json(business);
  } else {
    res.json({});
  }
});

exports.getTopRecommendedBusiness = asyncHandler(async (req, res) => {
  const pageSize = 10;
  const page = Number(req.query.pagenumber) || 1;
  const pageSkip = Number(pageSize * (page - 1));
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;
  const distance = req.query.distance ? Number(req.query.distance) : 20;

  const business = await Business.aggregate([
    {
      $geoNear: {
        near: { type: "Point", coordinates: [longitude, latitude] },
        spherical: true,
        distanceField: "distance",
        maxDistance: distance * 1000,
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }, { status: 1 }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        recommendation: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
    { $sort: { atlers: -1 } },
    { $skip: pageSkip },
    { $limit: pageSize },
  ]);

  if (business.length > 0) {
    res.json(business);
  } else {
    res.json([]);
  }
});

exports.getBusinessForMyAtlas = asyncHandler(async (req, res) => {
  const pageSize = 10;
  const page = Number(req.query.pagenumber) || 1;
  const pageSkip = Number(pageSize * (page - 1));
  const name = req.query.name
    ? {
        $regexMatch: {
          input: "$name",
          regex: req.query.name,
          options: "i",
        },
      }
    : {};
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;
  const distance = req.query.distance ? Number(req.query.distance) : 20;

  const recommendation = await Recommendation.find({
    user: req.user._id,
    status: 1,
  })
    .limit(pageSize)
    .skip(pageSize * (page - 1));

  const recomendationByUser = recommendation.map((item) => item.business);

  const business = await Business.aggregate([
    {
      $geoNear: {
        near: { type: "Point", coordinates: [longitude, latitude] },
        spherical: true,
        distanceField: "distance",
        maxDistance: distance * 1000,
      },
    },
    {
      $match: {
        $expr: {
          $and: [
            {
              $in: ["$_id", recomendationByUser],
            },
            name,
            { isActive: true },
          ],
        },
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id", user: "$user" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ["$business", "$$business"] },
                  { $eq: ["$user", mongoose.Types.ObjectId(req.user._id)] },
                ],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendationsForAtlers",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        recommendation: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendationsForAtlers" },
            then: { $size: "$recommendationsForAtlers" },
            else: "NA",
          },
        },
      },
    },
    { $skip: pageSkip },
    { $limit: pageSize },
  ]);

  if (business.length > 0) {
    res.json(business);
  } else {
    res.json([]);
  }
});

exports.getBusinessForLocalsAtlas = asyncHandler(async (req, res) => {
  const pageSize = 10;
  const page = Number(req.query.pagenumber) || 1;
  const pageSkip = Number(pageSize * (page - 1));
  const name = req.query.name
    ? { name: { $regex: req.query.name, $options: "i" } }
    : {};
  const category = req.query.name
    ? {
        "categories.name": { $regex: req.query.name, $options: "i" },
      }
    : {};
  const subcategory = req.query.name
    ? {
        "categories.subcategories.name": {
          $regex: req.query.name,
          $options: "i",
        },
      }
    : {};
  const tag = req.query.name
    ? { "tags.name": { $regex: req.query.name, $options: "i" } }
    : {};
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;
  const distance = req.query.distance ? Number(req.query.distance) : 20;

  const business = await Business.aggregate([
    {
      $geoNear: {
        near: { type: "Point", coordinates: [longitude, latitude] },
        spherical: true,
        distanceField: "distance",
        maxDistance: distance * 1000,
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            "recommendation.user": {
              $ne: mongoose.Types.ObjectId(req.user._id),
            },
          },
          { $or: [{ status: { $eq: 1 } }, { status: { $eq: 3 } }] },
          { $or: [name, category, subcategory, tag] },
          { behavior: 1 },
          { isActive: true },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        distance: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
    { $skip: pageSkip },
    { $limit: pageSize },
  ]);

  if (business.length > 0) {
    res.json(business);
  } else {
    res.json([]);
  }
});

exports.getBusinessForFriendsAtlas = asyncHandler(async (req, res) => {
  let myFriends = [];
  let myFriendsRecommendation = [];
  const pageSize = 10;
  const page = Number(req.query.pagenumber) || 1;
  const pageSkip = Number(pageSize * (page - 1));
  const name = req.query.name
    ? { name: { $regex: req.query.name, $options: "i" } }
    : {};

  const user = await User.find({
    friends: {
      $elemMatch: {
        user: mongoose.Types.ObjectId(req.user._id),
        status: 3,
      },
    },
  })
    .select("-password")
    .select("-friends");

  user.forEach((element) => {
    myFriends.push(element._id);
  });

  const recommendation = await Recommendation.find({
    user: { $in: myFriends },
    typeRecommendation: { $ne: 0 },
  });

  recommendation.forEach((element) => {
    myFriendsRecommendation.push(element.business);
  });

  const recommendedBusinesses = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "user",
              foreignField: "_id",
              as: "user",
            },
          },
          { $sort: { createdAt: -1 } },
          {
            $project: {
              askFor: 1,
              tryThis: 1,
              best: 1,
              isShown: 1,
              typeRecommendation: 1,
              usedAs: 1,
              recommendedBy: 1,
              review: 1,
              createdAt: 1,
              updatedAt: 1,
              user: { _id: 1, username: 1, profile: 1 },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $match: {
        $and: [
          { _id: { $in: myFriendsRecommendation } },
          name,
          { $or: [{ status: { $eq: 1 } }, { status: { $eq: 3 } }] },
          { behavior: 1 },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        location: 1,
        recommendation: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
    { $skip: pageSkip },
    { $limit: pageSize },
  ]);

  if (recommendedBusinesses) {
    res.json(recommendedBusinesses);
  } else {
    res.json([]);
  }
});

exports.getBusinessRecommendedForHome = asyncHandler(async (req, res) => {
  const pageSize = 10;
  const page = Number(req.query.pagenumber) || 1;
  const pageSkip = Number(pageSize * (page - 1));
  const name = req.query.name
    ? { name: { $regex: req.query.name, $options: "i" } }
    : {};
  const distance = req.query.distance ? Number(req.query.distance) : 10;
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;

  const recommendedBusinesses = await Recommendation.find({
    user: { $ne: mongoose.Types.ObjectId(req.user._id) },
  })
    .limit(pageSize)
    .skip(pageSize * (page - 1));

  const businessesToLookFor = []; // TODO

  for (let index = 0; index < recommendedBusinesses.length; index++) {
    const element = recommendedBusinesses[index];
    businessesToLookFor.push(element.business);
  }

  const business = await Business.aggregate([
    {
      $geoNear: {
        near: { type: "Point", coordinates: [longitude, latitude] },
        spherical: true,
        distanceField: "distance",
        maxDistance: distance * 1000,
      },
    },
    {
      $match: {
        $and: [
          {
            _id: { $in: businessesToLookFor }, // check in case of finding duplicates in search
          },
          { $or: [{ status: { $eq: 1 } }, { status: { $eq: 3 } }] },
          { isActive: true },
          name,
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "user",
              foreignField: "_id",
              as: "user",
            },
          },
          {
            $project: {
              _id: 0,
              askFor: 1,
              tryThis: 1,
              best: 1,
              isShown: 1,
              typeRecommendation: 1,
              usedAs: 1,
              recommendedBy: 1,
              review: 1,
              createdAt: 1,
              updatedAt: 1,
              myCount: 1,
              user: { username: 1, profile: 1 },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "advertisements",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "ads",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        recommendation: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        ads: 1,
        locals: {
          $size: {
            $filter: {
              input: "$recommendation",
              as: "r",
              cond: {
                $eq: ["$$r.recommendedBy", 1],
              },
            },
          },
        },
        travellers: {
          $size: {
            $filter: {
              input: "$recommendation",
              as: "r",
              cond: {
                $eq: ["$$r.recommendedBy", 2],
              },
            },
          },
        },
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
    { $skip: pageSkip },
    { $limit: pageSize },
  ]);

  if (business) {
    res.json(business);
  } else {
    res.json([]);
  }
});

exports.getRecommendationsFromBusiness = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 10;

  const recommendation = await Recommendation.aggregate([
    {
      $match: {
        $expr: {
          $and: [
            { $eq: ["$business", mongoose.Types.ObjectId(req.params.id)] },
            { $ne: ["$typeRecommendation", 0] },
          ],
        },
      },
    },
    {
      $lookup: {
        from: "comments",
        let: { recommendation: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$recommendation", "$$recommendation"] }],
              },
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "user",
              foreignField: "_id",
              as: "user",
            },
          },
          {
            $project: {
              comment: 1,
              createdAt: 1,
              user: { _id: 1, username: 1, profile: 1 },
            },
          },
          { $sort: { createdAt: -1 } },
        ],
        as: "comments",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $project: {
        _id: 1,
        askFor: 1,
        tryThis: 1,
        best: 1,
        isShown: 1,
        review: 1,
        business: 1,
        comments: 1,
        typeRecommendation: 1,
        usedAs: 1,
        recommendedBy: 1,
        createdAt: 1,
        updatedAt: 1,
        user: {
          _id: 1,
          username: 1,
          profile: { firstname: 1, lastname: 1, image: 1 },
        },
      },
    },
    { $sort: { createdAt: -1 } },
    { $skip: Number(pageSize * (pageNumber - 1)) },
    { $limit: pageSize },
  ]);

  if (recommendation) {
    res.json(recommendation);
  } else {
    res.json([]);
  }
});

exports.getBusinessForCarousel = asyncHandler(async (req, res) => {
  const pageSize = 10;
  const pageNumber = 1;
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;
  const distance = req.query.distance ? Number(req.query.distance) : 20;

  const business = await Business.aggregate([
    {
      $geoNear: {
        near: { type: "Point", coordinates: [longitude, latitude] },
        spherical: true,
        distanceField: "distance",
        maxDistance: distance * 1000,
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $project: {
        name: 1,
        image: 1,
        website: 1,
      },
    },
    { $sort: { atlers: -1 } },
    { $skip: Number(pageSize * (pageNumber - 1)) },
    { $limit: pageSize },
  ]);

  const carousel = [];

  business.forEach((element) => {
    carousel.push({
      _id: element._id,
      title: element.name,
      imageUrl: element.image,
      urlWeb: element.website,
    });
  });

  if (carousel.length > 0) {
    res.json(carousel);
  } else {
    res.json([]);
  }
});

exports.getBusinessesForSale = asyncHandler(async (req, res) => {
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;
  const distance = req.query.distance ? Number(req.query.distance) : 20;

  const business = await Business.aggregate([
    {
      $geoNear: {
        near: { type: "Point", coordinates: [longitude, latitude] },
        spherical: true,
        distanceField: "distance",
        maxDistance: distance * 1000,
      },
    },
    {
      $match: {
        $expr: { $ne: ["$userCreator", mongoose.Types.ObjectId(req.user._id)] },
      },
    },
    { $sample: { size: 2 } },
  ]);

  res.json(business);
});

exports.saveBusinessVisitedByUser = asyncHandler(async () => {
  const userTracking = await UserTracking.create({
    user: req.user._id,
    business: req.body.business,
    action: 1,
  });

  if (userTracking) {
    res.json(userTracking);
  } else {
    res.json({});
  }
});
//#endregion

// ######################################################################
// Administrator or operator APIS
// ######################################################################
//#region
exports.activateBusiness = asyncHandler(async (req, res) => {
  const { observation } = req.body;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const business = await Business.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { isActive: true } },
    { new: true }
  );

  await AdminAudit.create({
    user: req.user._id,
    observation: observation,
    object: "business",
    objectId: `${businessExists._id}`,
    field: "isActive",
    oldValues: [`${businessExists.isActive}`],
    newValues: ["true"],
    action: "1",
  });

  const user = await User.findById(businessExists.userCreator);

  const global = await Global.findOne({ user: req.user._id });

  if (business && business.isActive === true) {
    if (Expo.isExpoPushToken(user.notificationToken)) {
      await sendPushNotification(
        user.notificationToken,
        notifications.notificationPushMessage(
          global.language,
          3,
          businessExists.name
        ),
        "businessHome"
      );
    }

    res.json(business);
  } else {
    res.status(400);
    throw new Error("Something went wrong");
  }
});

exports.updateBusinessById = asyncHandler(async (req, res) => {
  const { observation, name, description, status, behavior } = req.body;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const business = await Business.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        name: name,
        description: description,
        status: status,
        behavior: behavior,
      },
    },
    { new: true, upsert: true }
  );

  if (businessExists.name !== name) {
    await AdminAudit.create({
      user: req.user._id,
      object: "business",
      objectId: `${businessExists._id}`,
      field: "name",
      observation: observation,
      oldValues: [`${businessExists.name}`],
      newValues: [`${name}`],
      action: "1",
    });
  } else if (businessExists.description !== description) {
    await AdminAudit.create({
      user: req.user._id,
      object: "business",
      objectId: `${businessExists._id}`,
      field: "description",
      observation: observation,
      oldValues: [`${businessExists.description}`],
      newValues: [`${description}`],
      action: "1",
    });
  } else if (businessExists.status !== status) {
    await AdminAudit.create({
      user: req.user._id,
      object: "business",
      objectId: `${businessExists._id}`,
      field: "status",
      observation: observation,
      oldValues: [`${businessExists.status}`],
      newValues: [`${status}`],
      action: "1",
    });
  } else if (businessExists.behavior !== behavior) {
    await AdminAudit.create({
      user: req.user._id,
      object: "business",
      objectId: `${businessExists._id}`,
      field: "behavior",
      observation: observation,
      oldValues: [`${businessExists.behavior}`],
      newValues: [`${behavior}`],
      action: "1",
    });
  }

  const user = await User.findById(businessExists.userCreator);

  const global = await Global.findOne({ user: req.user._id });

  if (business) {
    if (Expo.isExpoPushToken(user.notificationToken)) {
      await sendPushNotification(
        user.notificationToken,
        notifications.notificationPushMessage(
          global.language,
          4,
          businessExists.name
        ),
        "businessHome"
      );
    }
    res.json(business);
  } else {
    res.status(400);
    throw new Error("Something went wrong");
  }
});

exports.getAllBusiness = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 5;
  const keyword = req.query.keyword
    ? { name: { $regex: req.query.keyword, $options: "i" } }
    : {};

  const totalDocuments = await Business.countDocuments({ ...keyword });

  const businesses = await Business.find({ ...keyword })
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({ createdAt: -1 });

  res.json({ businesses, pageNumber, pageSize, totalDocuments });
});

exports.getBusinessById = asyncHandler(async (req, res) => {
  const business = await Business.findById(req.params.id);

  if (business) {
    res.json(business);
  } else {
    res.status(404);
    throw new Error("Business not found");
  }
});

exports.getUserCreatorFromBusiness = asyncHandler(async (req, res) => {
  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const userCreator = await User.findById(businessExists.userCreator).select(
    "-password"
  );

  res.json(userCreator);
});

exports.getOwnerUserFromBusiness = asyncHandler(async (req, res) => {
  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const ownerUser = await User.findById(businessExists.ownerUser).select(
    "-password"
  );

  res.json(ownerUser);
});

exports.getBusinessRecommendationCount = asyncHandler(async (req, res) => {
  const recommendation = await Recommendation.aggregate([
    {
      $match: {
        business: mongoose.Types.ObjectId(req.params.id),
      },
    },
    {
      $group: { _id: null, recommendations: { $sum: 1 } },
    },
    { $project: { _id: 0 } },
  ]);

  if (recommendation.length > 0) {
    res.json(recommendation[0]);
  } else {
    res.json({ recommendations: 0 });
  }
});

exports.getTop5Business = asyncHandler(async (req, res) => {
  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "userCreator",
        foreignField: "_id",
        as: "userCreator",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        userCreator: { profile: { firstname: 1, lastname: 1 } },
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
    {
      $sort: { atlers: -1 },
    },
  ]);

  res.json(business);
});
//#endregion
