const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");

const Category = require("../models/category");
const Subcategory = require("../models/subcategory");

exports.getAllCategories = asyncHandler(async (req, res) => {
  const category = await Category.aggregate([
    {
      $addFields: {
        check: false,
      },
    },
    {
      $project: { name: 1, check: 1 },
    },
  ]);

  if (category.length > 0) {
    res.json(category);
  } else {
    res.json({ message: "Categories not found" });
  }
});

exports.getAllSubcategories = asyncHandler(async (req, res) => {
  const subcategory = await Subcategory.aggregate([
    {
      $addFields: {
        check: false,
      },
    },
    {
      $project: { name: 1, check: 1 },
    },
  ]);

  if (subcategory.length > 0) {
    res.json(subcategory);
  } else {
    res.json({ message: "Subcategories not found" });
  }
});

exports.getAllCategoriesWithSubcategories = asyncHandler(async (req, res) => {
  const category = await Category.aggregate([
    {
      $lookup: {
        from: "subcategories",
        let: { category: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$category", "$$category"] }],
              },
            },
          },
        ],
        as: "subcategories",
      },
    },
  ]);

  if (category) {
    res.json(category);
  } else {
    res.json({ message: "Subcategory not found" });
  }
});

// ######################################################################
// Administrator or operator APIs
// ######################################################################
//#region
exports.createCategory = asyncHandler(async (req, res) => {
  const { name } = req.body;

  const category = await Category.create({
    name: name,
  });

  res.json(category);
});

exports.getCategories = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 5;
  const keyword = req.query.keyword
    ? { name: { $regex: req.query.keyword, $options: "i" } }
    : {};

  const totalDocuments = await Category.countDocuments({ ...keyword });

  const categories = await Category.find({ ...keyword })
    .limit(pageSize)
    .skip(Number(pageSize * (pageNumber - 1)));

  res.json({ categories, pageNumber, pageSize, totalDocuments });
});

exports.getCategoryById = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id);

  res.json(category);
});

exports.getCategoryAndSubcategories = asyncHandler(async (req, res) => {
  const category = await Category.aggregate([
    {
      $lookup: {
        from: "subcategories",
        let: { category: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$category", "$$category"] }],
              },
            },
          },
        ],
        as: "subcategories",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
  ]);

  const subcategories = [];

  if (category.length > 0) {
    for (let index = 0; index < category[0].subcategories.length; index++) {
      const element = category[0].subcategories[index];
      subcategories.push({ _id: element._id, name: element.name[0] });
    }

    res.json({
      _id: category[0]._id,
      name: category[0].name[0],
      subcategories: subcategories,
    });
  } else {
    res.json({});
  }
});

exports.createSubcategoriesForCategory = asyncHandler(async (req, res) => {
  let { name } = req.body;

  const categoryExists = await Category.findById(req.params.id);

  if (!categoryExists) {
    res.status(404);
    throw new Error("Category not found");
  }

  const subcategory = await Subcategory.create({
    category: req.params.id,
    name: name,
  });

  res.json(subcategory);
});
//#endregion
