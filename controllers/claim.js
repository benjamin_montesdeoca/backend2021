require("dotenv").config();
const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");
const { Expo } = require("expo-server-sdk");
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const sendPushNotification = require("../utils/pushToken");
const { generateCode } = require("../utils/mathCalculations");

const User = require("../models/user");
const Business = require("../models/business");
const BusinessAudit = require("../models/businessAudit");
const Claim = require("../models/claim");

const { getMilles } = require("../utils/mathCalculations");

exports.businessClaimStatus = asyncHandler(async (req, res) => {
  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(400);
    throw new Error("This business does not exists");
  }

  const claimExists = await Claim.findOne({ business: req.params.id });

  if (claimExists) {
    res.json({
      message: "This business has been claimed by you",
      status: 0,
    });
  } else {
    res.json({
      message: "This business is free to reclaim",
      status: -1,
    });
  }
});

exports.createBusinessClaim = asyncHandler(async (req, res) => {
  let { email, phone, latitude, longitude } = req.body;
  let { status, paid } = 0;

  const businessExists = await Business.findById(req.params.id);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  const claimExists = await Claim.findOne({ business: req.params.id });

  if (claimExists) {
    if (claimExists.user.toString() === req.user._id.toString()) {
      res.status(400);
      throw new Error(
        "This business has already been claimed by you, check the status of your process in the same app"
      );
    } else {
      res.status(400);
      throw new Error("This business has already been claimed");
    }
  } else {
    status = 1;
    paid = 0;
  }

  const distance = getMilles(
    latitude,
    longitude,
    businessExists.location.coordinates[1],
    businessExists.location.coordinates[0]
  );

  if (distance <= 10) {
    await Business.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: { status: 3, ownerUser: req.user._id } }
    );

    await Claim.create({
      user: req.user._id,
      business: req.params.id,
      location: {
        type: "Point",
        coordinates: [longitude, latitude],
      },
      businessInfo: {
        phone: phone,
        email: email,
      },
      status: status,
      paid: paid,
    });

    await BusinessAudit.create({
      business: businessExists._id,
      field: "ownerUser",
      oldValues: [`${businessExists.ownerUser}`],
      newValues: [`${req.user._id}`],
      type: 1,
    });

    const business = await Business.aggregate([
      {
        $lookup: {
          from: "recommendations",
          let: { business: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$business", "$$business"] }],
                },
              },
            },
          ],
          as: "recommendation",
        },
      },
      {
        $lookup: {
          from: "tags",
          localField: "tags",
          foreignField: "_id",
          as: "tags",
        },
      },
      {
        $match: {
          $and: [
            {
              _id: {
                $eq: mongoose.Types.ObjectId(req.params.id),
              },
            },
          ],
        },
      },
      {
        $project: {
          name: 1,
          userCreator: 1,
          ownerUser: 1,
          image: 1,
          images: 1,
          description: 1,
          mainAddress: 1,
          openingHours: 1,
          socialLinks: 1,
          website: 1,
          phone: 1,
          address: 1,
          location: 1,
          status: 1,
          behavior: 1,
          categories: 1,
          tags: 1,
          createdAt: 1,
          updatedAt: 1,
          atlers: {
            $cond: {
              if: { $isArray: "$recommendation" },
              then: { $size: "$recommendation" },
              else: "NA",
            },
          },
        },
      },
    ]);

    if (business) {
      res.json(business[0]);
    } else {
      res.json({});
    }
  } else {
    res.status(400);
    throw new Error("This business is out of scope to be claimed");
  }
});

exports.sendClaimActivationCode = asyncHandler(async (req, res) => {
  const { phone } = req.body;
  const code = generateCode();

  const accountSid = process.env.TWILIO_ACCOUNT_SID;
  const authToken = process.env.TWILIO_AUTH_TOKEN;
  const client = require("twilio")(accountSid, authToken);

  client.messages
    .create({
      body: `This is your code for claiming a business: ${code}`,
      messagingServiceSid: `${process.env.TWILIO_MESSAGING_SERVICES_ID}`,
      to: `${phone}`,
    })
    .then((message) => console.log(message.sid))
    .done();

  res.json({ code: code });
});

// ----------------------------------------------------------------------
// Start: API for clients, on standby
// ----------------------------------------------------------------------

exports.paymentToClaimBusiness = asyncHandler(async (req, res) => {
  const business = await Business.findById(req.query.business);
  const user = await User.findById(req.query.user).select("-password");

  const session = await stripe.checkout.sessions.create({
    customer_email: `${user.email}`,
    payment_method_types: ["card"],
    line_items: [
      {
        price_data: {
          currency: "usd",
          product_data: {
            name: `${business.name}`,
            description:
              business.description !== ""
                ? `${business.description}`
                : "No description available",
            images: [`https://www.localsatlas.com/${business.image}`],
          },
          unit_amount: 2000,
        },
        quantity: 1,
      },
    ],
    mode: "payment",
    success_url: `https://admin.localsatlas.com/success/${business._id}/${user._id}`,
    cancel_url:
      "https://angry-feynman.62-151-179-114.plesk.page/nuevo/index.html",
  });

  res.json({ url: session.url });
});

exports.updateBusinessStatusAfterClaim = asyncHandler(async (req, res) => {
  await Business.findOneAndUpdate(
    { _id: req.query.business },
    { $set: { status: 2 } }
  );
  const claim = await Claim.findOneAndUpdate(
    { _id: req.query.claim },
    { $set: { paid: 1 } },
    { new: true }
  );

  if (claim) {
    res.json(claim);
  } else {
    res.json({});
  }
});

// ----------------------------------------------------------------------
// End: API for clients, on standby
// ----------------------------------------------------------------------

// ######################################################################
// Administrator or operator APIS
// ######################################################################

exports.getAllClaims = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 5;
  const keyword = req.query.keyword
    ? { "business.name": { $regex: req.query.keyword, $options: "i" } }
    : {};

  const totalDocuments = await Claim.aggregate([
    {
      $lookup: {
        from: "businesses",
        localField: "business",
        foreignField: "_id",
        as: "business",
      },
    },
    { $match: keyword },
    { $group: { _id: null, totalDocuments: { $sum: 1 } } },
    { $project: { _id: 0 } },
  ]);

  const allClaims = await Claim.aggregate([
    {
      $lookup: {
        from: "businesses",
        localField: "business",
        foreignField: "_id",
        as: "business",
      },
    },
    {
      $match: keyword,
    },
    { $limit: pageSize },
    { $skip: pageSize * (pageNumber - 1) },
    { $sort: { createdAt: -1 } },
  ]);

  const claims = [];

  allClaims.forEach((everyClaim) => {
    claims.push({
      _id: everyClaim._id,
      location: everyClaim.location,
      user: everyClaim.user,
      businessInfo: everyClaim.businessInfo,
      status: everyClaim.status,
      paid: everyClaim.paid,
      createdAt: everyClaim.createdAt,
      business: {
        _id: everyClaim.business[0]._id,
        image: everyClaim.business[0].image,
        name: everyClaim.business[0].name,
        description: everyClaim.business[0].description,
        mainAddress: everyClaim.business[0].mainAddress,
        address: everyClaim.business[0].address,
        phone: everyClaim.business[0].phone,
        website: everyClaim.business[0].website,
        status: everyClaim.business[0].status,
        location: everyClaim.business[0].location,
      },
    });
  });

  res.json({
    claims,
    pageNumber,
    pageSize,
    totalDocuments: totalDocuments[0].totalDocuments,
  });
});

exports.getClaimById = asyncHandler(async (req, res) => {
  const claimExists = await Claim.findById(req.params.id);

  if (!claimExists) {
    res.status(404);
    throw new Error("Claim does not exist");
  }

  const existingClaim = await Claim.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id),
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $lookup: {
        from: "businesses",
        let: { business: "$business" },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$_id", "$$business"],
              },
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "userCreator",
              foreignField: "_id",
              as: "userCreator",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "ownerUser",
              foreignField: "_id",
              as: "ownerUser",
            },
          },
          {
            $project: {
              image: 1,
              name: 1,
              description: 1,
              mainAddress: 1,
              address: 1,
              phone: 1,
              website: 1,
              status: 1,
              location: 1,
              userCreator: { _id: 1, username: 1, email: 1, profile: 1 },
              ownerUser: { _id: 1, username: 1, email: 1, profile: 1 },
            },
          },
        ],
        as: "business",
      },
    },
    {
      $project: {
        location: 1,
        businessInfo: 1,
        status: 1,
        paid: 1,
        createdAt: 1,
        user: { _id: 1, username: 1, profile: 1, email: 1 },
        business: 1,
      },
    },
  ]);

  let claim = {};

  existingClaim.forEach((element) => {
    claim = {
      _id: element._id,
      location: element.location,
      businessInfo: element.businessInfo,
      status: element.status,
      paid: element.paid,
      createdAt: element.createdAt,
    };
    claim.business = {
      _id: element.business[0]._id,
      image: element.business[0].image,
      name: element.business[0].name,
      description: element.business[0].description,
      mainAddress: element.business[0].mainAddress,
      address: element.business[0].address,
      phone: element.business[0].phone,
      website: element.business[0].website,
      status: element.business[0].status,
      status: element.business[0].behavior,
      location: element.business[0].location,
    };
    claim.userCreator = {
      _id: element.business[0].userCreator[0]._id,
      profile: element.business[0].userCreator[0].profile,
      username: element.business[0].userCreator[0].username,
      email: element.business[0].userCreator[0].email,
    };
    claim.ownerUser = {
      _id: element.business[0].ownerUser[0]._id,
      profile: element.business[0].ownerUser[0].profile,
      username: element.business[0].ownerUser[0].username,
      email: element.business[0].ownerUser[0].email,
    };
    claim.claimantUser = {
      _id: element.user[0]._id,
      profile: element.user[0].profile,
      username: element.user[0].username,
      email: element.user[0].email,
    };
  });

  if (claim) {
    res.json(claim);
  } else {
    res.json(null);
  }
});

// TO DO: Improve this API when needed
exports.approveBusinessClaim = asyncHandler(async (req, res) => {
  const claimExists = await Claim.findById(req.params.id);

  if (!claimExists) {
    res.status(404);
    throw new Error("This claim does not exist");
  }

  const claim = await Claim.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { status: 1 } },
    { new: true }
  );

  const business = await Business.findOneAndUpdate(
    { _id: claimExists.business },
    { $set: { status: 6 } },
    { new: true }
  );

  const user = await User.findById(claimExists.user);

  if (claim && claim.status === 1) {
    if (Expo.isExpoPushToken(user.notificationToken)) {
      await sendPushNotification(
        user.notificationToken,
        `The claim for the ${business.name} business has been approved`,
        "businessHome"
      );
    }
  }

  if (claim) {
    res.json({ message: "Claim updated correctly" });
  } else {
    res.json({ message: "Error updating claim" });
  }
});
