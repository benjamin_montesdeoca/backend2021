const asyncHandler = require("express-async-handler");

const Country = require("../models/country");

exports.getCountries = asyncHandler(async (req, res) => {
  const country = await Country.find();

  if (country) {
    res.json(country);
  } else {
    res.json({ message: "No countries found" });
  }
});
