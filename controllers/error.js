const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");

const ErrorAudit = require("../models/errorAudit");

// ######################################################################
// User APIs
// ######################################################################
//#region
exports.createError = asyncHandler(async (req, res) => {
  const { data } = req.body;
  let allDataUploaded = false;

  for (let index = 0; index < data.length; index++) {
    const element = data[index];
    await ErrorAudit.create({
      errorScreen: element.errorScreen,
      errorFunction: element.errorFunction,
      errorName: element.errorName,
      errorText: element.errorText,
      deviceBrand: element.deviceBrand,
      deviceModel: element.deviceModel,
      deviceOsVersion: element.deviceOsVersion,
      deviceOsName: element.deviceOsName,
      errorDate: element.errorDate,
      user: req.user._id,
    });
    allDataUploaded = true;
  }

  if (allDataUploaded) {
    res.json({ message: "All data was updated succesfully", status: 1 });
  } else {
    res.json({ message: "Error when uploading data", status: -1 });
  }
});
//#endregion

// ######################################################################
// Administrator or operator APIS
// ######################################################################
//#region
exports.getErrors = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 5;
  const keyword = req.query.keyword
    ? {
        $or: [
          { errorScreen: { $regex: req.query.keyword, $options: "i" } },
          { errorText: { $regex: req.query.keyword, $options: "i" } },
        ],
      }
    : {};

  const totalDocuments = await ErrorAudit.countDocuments({ ...keyword });

  const errors = await ErrorAudit.find({ ...keyword })
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({ errorDate: -1 });

  res.json({ errors, pageNumber, pageSize, totalDocuments });
});

exports.getErrorById = asyncHandler(async (req, res) => {
  const errorExists = await ErrorAudit.findById(req.params.id);

  if (!errorExists) {
    res.status(404);
    throw new Error("Error does not exist");
  }

  const existingError = await ErrorAudit.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id),
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $project: {
        _id: 1,
        errorScreen: 1,
        errorFunction: 1,
        errorName: 1,
        errorText: 1,
        deviceBrand: 1,
        deviceModel: 1,
        deviceOsVersion: 1,
        deviceOsName: 1,
        errorDate: 1,
        user: {
          _id: 1,
          username: 1,
          profile: { firstname: 1, lastname: 1, image: 1 },
          email: 1,
        },
      },
    },
  ]);

  let error = {
    _id: existingError[0]._id,
    errorScreen: existingError[0].errorScreen,
    errorFunction: existingError[0].errorFunction,
    errorName: existingError[0].errorName,
    errorText: existingError[0].errorText,
    deviceBrand: existingError[0].deviceBrand,
    deviceModel: existingError[0].deviceModel,
    deviceOsVersion: existingError[0].deviceOsVersion,
    deviceOsName: existingError[0].deviceOsName,
    errorDate: existingError[0].errorDate,
    user: existingError[0].user[0],
  };

  if (error) {
    res.json(error);
  } else {
    res.json({});
  }
});
//#endregion
