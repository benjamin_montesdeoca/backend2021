const asyncHandler = require("express-async-handler");

const Fee = require("../models/fee");

// Clients functions

exports.getClaimFee = asyncHandler(async (req, res) => {
  const fee = await Fee.findOne({ name: "Claim" });

  if (fee) {
    res.json(fee);
  } else {
    res.json({});
  }
});

// Administrator or operators functions

exports.createFee = asyncHandler(async (req, res) => {
  const { name, price, description } = req.body;

  const feeExists = await Fee.findOne({ name });

  if (feeExists) {
    res.status(400);
    throw new Error("Fee already exists");
  }

  const fee = await Fee.create({
    name: name,
    price: price,
    description: description,
  });

  if (fee) {
    res.json(fee);
  } else {
    res.status(400);
  }
});

exports.updateClaimFee = asyncHandler(async (req, res) => {
  const fee = await Fee.findOneAndUpdate(
    { name: "Claim" },
    { $set: { price: req.body.price } },
    { new: true, upsert: true }
  );

  if (fee) {
    res.json(fee);
  } else {
    throw new Error("The fee was not updated");
  }
});
