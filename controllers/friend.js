const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");
const { Expo } = require("expo-server-sdk");

const sendPushNotification = require("../utils/pushToken");
const notificacion = require("../helpers/notifications");

const User = require("../models/user");
const Notification = require("../models/notification");
const Global = require("../models/global");

exports.createFriendRequest = asyncHandler(async (req, res) => {
  const friendRequestExists = await User.find({
    _id: req.user._id,
    friends: { $elemMatch: { user: req.params.id } },
  });

  if (friendRequestExists.length > 0) {
    res.status(400);
    throw Error("You have a friend request with this user");
  }

  const sender = await User.findOneAndUpdate(
    { _id: req.user._id },
    {
      $push: { friends: { user: req.params.id, status: 1 } },
    },
    { new: true }
  );

  const receiver = await User.findOneAndUpdate(
    { _id: req.params.id },
    {
      $push: { friends: { user: req.user._id, status: 2 } },
    },
    { new: true }
  );

  const global = await Global.findOne({ user: receiver._id });

  if (sender && receiver) {
    if (Expo.isExpoPushToken(receiver.notificationToken)) {
      await sendPushNotification(
        receiver.notificationToken,
        notificacion.notificationPushMessage(
          global.language,
          5,
          sender.username
        ),
        "friendshipRequests"
      );
    }

    await Notification.create({
      user: req.params.id,
      title: notificacion.notificationTitle(4),
      message: notificacion.notificationMessage(4, sender.username),
      isState: 0,
      type: 4,
    });

    const posibleFriend = await User.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(req.params.id),
        },
      },
      {
        $project: {
          _id: 1,
          profile: 1,
          mainAddress: 1,
          optionalAddress01: 1,
          optionalAddress02: 1,
          isActive: 1,
          username: 1,
          status: 1,
          type: 1,
          mainLocation: 1,
          optionalLocation01: 1,
          optionalLocation02: 1,
          friends: {
            $filter: {
              input: "$friends",
              as: "friend",
              cond: {
                $and: [
                  {
                    $eq: [
                      "$$friend.user",
                      mongoose.Types.ObjectId(req.user._id),
                    ],
                  },
                  {
                    $or: [
                      { $eq: ["$$friend.status", 3] },
                      { $eq: ["$$friend.status", 2] },
                    ],
                  },
                ],
              },
            },
          },
        },
      },
    ]);

    let user = {};

    for (let index = 0; index < posibleFriend.length; index++) {
      const element = posibleFriend[index];

      user = {
        _id: element._id,
        profile: element.profile,
        mainAddress: element.mainAddress,
        optionalAddress01: element.optionalAddress01,
        optionalAddress02: element.optionalAddress02,
        isActive: element.isActive,
        username: element.username,
        status: element.status,
        type: element.type,
        mainLocation: element.mainLocation,
        optionalLocation01: element.optionalLocation01,
        optionalLocation02: element.optionalLocation02,
        friend: element.friends[0],
      };
    }

    res.json(user);
  } else {
    res.status(400);
    throw new Error("Friend request was not sent");
  }
});

exports.acceptFriendRequest = asyncHandler(async (req, res) => {
  const sender = await User.findOneAndUpdate(
    { _id: req.user._id, "friends.user": req.params.id },
    { $set: { "friends.$.status": 3 } },
    { new: true }
  );

  const receiver = await User.findOneAndUpdate(
    { _id: req.params.id, "friends.user": req.user._id },
    { $set: { "friends.$.status": 3 } },
    { new: true }
  ).select("-password");

  if (sender && receiver) {
    await Notification.create({
      user: req.user._id,
      title: notificacion.notificationTitle(5),
      message: notificacion.notificationMessage(5, receiver.username),
      isState: 0,
      type: 5,
    });

    await Notification.create({
      user: req.params.id,
      title: "Friend request accepted",
      message: notificacion.notificationMessage(5, sender.username),
      isState: 0,
      type: 5,
    });

    res.json(receiver);
  }
});

exports.deleteFriendRequest = asyncHandler(async (req, res) => {
  const sender = await User.findOneAndUpdate(
    { _id: req.user._id },
    { $pull: { friends: { user: mongoose.Types.ObjectId(req.params.id) } } },
    { new: true, upsert: true }
  ).select("-password");

  const receiver = await User.findOneAndUpdate(
    { _id: req.params.id },
    { $pull: { friends: { user: mongoose.Types.ObjectId(req.user._id) } } },
    { new: true, upsert: true }
  );

  if (sender && receiver) {
    res.json(sender);
  }
});

exports.deleteFriendship = asyncHandler(async (req, res) => {
  const sender = await User.findOneAndUpdate(
    { _id: req.user._id },
    { $pull: { friends: { user: mongoose.Types.ObjectId(req.params.id) } } },
    { new: true, upsert: true }
  );

  const receiver = await User.findOneAndUpdate(
    { _id: req.params.id },
    { $pull: { friends: { user: mongoose.Types.ObjectId(req.user._id) } } },
    { new: true, upsert: true }
  );

  if (sender && receiver) {
    res.json(receiver);
  } else {
    res.json({ message: "Friendship was not deleted" });
  }
});

exports.getFriendsFromCurrentUser = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 10;
  const username = req.query.keyword
    ? { username: { $regex: req.query.keyword, $options: "i" } }
    : {};
  const firstname = req.query.keyword
    ? { "profile.firstname": { $regex: req.query.keyword, $options: "i" } }
    : {};
  const lastname = req.query.keyword
    ? { "profile.lastname": { $regex: req.query.keyword, $options: "i" } }
    : {};

  const currentUsers = await User.aggregate([
    {
      $match: {
        $and: [
          {
            friends: {
              $elemMatch: {
                user: mongoose.Types.ObjectId(req.user._id),
                $or: [{ status: 3 }, { status: 2 }],
              },
            },
          },
          { $or: [firstname, lastname, username] },
        ],
      },
    },
    {
      $project: {
        _id: 1,
        profile: 1,
        mainAddress: 1,
        optionalAddress01: 1,
        optionalAddress02: 1,
        isActive: 1,
        type: 1,
        username: 1,
        mainLocation: 1,
        optionalLocation01: 1,
        optionalLocation02: 1,
        friends: {
          $filter: {
            input: "$friends",
            as: "friend",
            cond: {
              $and: [
                {
                  $eq: ["$$friend.user", mongoose.Types.ObjectId(req.user._id)],
                },
                {
                  $or: [
                    { $eq: ["$$friend.status", 3] },
                    { $eq: ["$$friend.status", 2] },
                  ],
                },
              ],
            },
          },
        },
      },
    },
    { $skip: Number(pageSize * (pageNumber - 1)) },
    { $limit: pageSize },
  ]);

  const users = [];

  for (let index = 0; index < currentUsers.length; index++) {
    const element = currentUsers[index];

    const user = {
      _id: element._id,
      profile: element.profile,
      mainAddress: element.mainAddress,
      optionalAddress01: element.optionalAddress01,
      optionalAddress02: element.optionalAddress02,
      type: element.type,
      isActive: element.isActive,
      username: element.username,
      mainLocation: element.mainLocation,
      optionalLocation01: element.optionalLocation01,
      optionalLocation02: element.optionalLocation02,
      friend: element.friends[0],
    };

    users.push(user);
  }

  if (users.length > 0) {
    res.json(users);
  } else {
    res.json([]);
  }
});

exports.getMyFriendRequests = asyncHandler(async (req, res) => {
  const user = await User.find({
    friends: {
      $elemMatch: { user: mongoose.Types.ObjectId(req.user._id), status: 1 },
    },
  })
    .select("-password")
    .select("-friends");

  if (user.length > 0) {
    res.json(user);
  } else {
    res.json([]);
  }
});

exports.getNonFriendsUsers = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 10;
  const username = req.query.keyword
    ? { username: { $regex: req.query.keyword, $options: "i" } }
    : {};
  const firstname = req.query.keyword
    ? { "profile.firstname": { $regex: req.query.keyword, $options: "i" } }
    : {};
  const lastname = req.query.keyword
    ? { "profile.lastname": { $regex: req.query.keyword, $options: "i" } }
    : {};

  const user = await User.find({
    $and: [
      { "friends.user": { $ne: req.user._id } },
      { status: 1 },
      { _id: { $ne: req.user._id } },
      { _id: { $ne: mongoose.Types.ObjectId(process.env.ADMIN_USER_ID) } },
      { _id: { $ne: mongoose.Types.ObjectId(process.env.GUEST_USER_ID) } },
      { $or: [firstname, lastname, username] },
    ],
  })
    .select("-password")
    .select("-friends")
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1));

  if (user.length > 0) {
    res.json(user);
  } else {
    res.json([]);
  }
});
