require("dotenv").config();
const asyncHandler = require("express-async-handler");

const Global = require("../models/global");

exports.updateGlobalLanguageForUser = asyncHandler(async (req, res) => {
  const { language } = req.body;

  const userExists = await Global.findOne({ user: req.user._id });

  if (!userExists) {
    res.status(400);
    throw new Error("User not found");
  }

  let global = {};

  if (userExists.user.toString() !== process.env.GUEST_USER_ID) {
    global = await Global.findOneAndUpdate(
      { user: req.user._id },
      { $set: { language: language } },
      { new: true, upsert: true }
    );
    res.json(global);
  } else {
    res.json(global);
  }
});

exports.getLanguageOfUser = asyncHandler(async (req, res) => {
  const userExists = await Global.findOne({ user: req.user._id });

  if (!userExists) {
    res.status(400);
    throw new Error("User not found");
  }

  res.json({ language: userExists.language });
});

exports.updateIsNewUser = asyncHandler(async (req, res) => {
  const userExists = await Global.findOne({ user: req.user._id });

  if (!userExists) {
    res.status(400);
    throw new Error("User not found");
  }

  const global = await Global.findOneAndUpdate(
    { user: req.user._id },
    { $set: { isNewUser: 0 } },
    { new: true, upsert: true }
  );

  res.json(global);
});

exports.getIsNewUser = asyncHandler(async (req, res) => {
  const userExists = await Global.findOne({ user: req.user._id });

  if (!userExists) {
    res.status(400);
    throw new Error("User not found");
  }

  res.json({ isNewUser: userExists.isNewUser });
});
