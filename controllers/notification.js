const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");

const Notification = require("../models/notification");
const Global = require("../models/global");
const User = require("../models/user");
const Answer = require("../models/answer");

exports.getUserNotifications = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 10;

  const notificationList = [];

  const global = await Global.findOne({ user: req.user._id });

  const notification = await Notification.find({ user: req.user._id })
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({
      createdAt: -1,
    });

  switch (global.language) {
    case 0:
      notification.forEach((element) => {
        notificationList.push({
          _id: element._id,
          user: element.user,
          isState: element.isState,
          title: element.title[0],
          message: element.message[0],
          type: element.type,
          review: element.review,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,
        });
      });
      break;
    case 1:
      notification.forEach((element) => {
        notificationList.push({
          _id: element._id,
          user: element.user,
          isState: element.isState,
          title: element.title[1],
          message: element.message[1],
          review: element.review,
          type: element.type,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,
        });
      });
      break;
    default:
      notification.forEach((element) => {
        notificationList.push({
          _id: element._id,
          user: element.user,
          isState: element.isState,
          title: element.title[0],
          message: element.message[0],
          type: element.type,
          review: element.review,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,
        });
      });
      break;
  }

  if (notificationList) {
    res.json(notificationList);
  } else {
    res.json([]);
  }
});

exports.updateNotificationStatus = asyncHandler(async (req, res) => {
  const notificationExists = await Notification.findById(req.params.id);

  if (!notificationExists) {
    res.status(404);
    throw new Error("Notification not found");
  }

  let updatedNotification = {};

  const global = await Global.findOne({ user: req.user._id });

  const notification = await Notification.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { isState: 1 } },
    { new: true, upsert: true }
  );

  switch (global.language) {
    case 0:
      updatedNotification = {
        _id: notification._id,
        user: notification.user,
        isState: notification.isState,
        title: notification.title[0],
        message: notification.message[0],
        type: notification.type,
        review: notification.review,
        createdAt: notification.createdAt,
        updatedAt: notification.updatedAt,
      };

      break;
    case 1:
      updatedNotification = {
        _id: notification._id,
        user: notification.user,
        isState: notification.isState,
        title: notification.title[1],
        message: notification.message[1],
        review: notification.review,
        type: notification.type,
        createdAt: notification.createdAt,
        updatedAt: notification.updatedAt,
      };
      break;
    default:
      updatedNotification = {
        _id: notification._id,
        user: notification.user,
        isState: notification.isState,
        title: notification.title[0],
        message: notification.message[0],
        type: notification.type,
        review: notification.review,
        createdAt: notification.createdAt,
        updatedAt: notification.updatedAt,
      };

      break;
  }

  res.json(updatedNotification);
});

exports.getNotificationCount = asyncHandler(async (req, res) => {
  const notification = await Notification.find({
    user: mongoose.Types.ObjectId(req.user._id),
    isState: 0,
  });

  const user = await User.find({
    friends: {
      $elemMatch: { user: mongoose.Types.ObjectId(req.user._id), status: 1 },
    },
  })
    .select("-password")
    .select("-friends");

  const answer = await Answer.find({ user: req.user._id, status: 0 });

  let myNotificationCount = { notifications: 0, friends: 0, requests: 0 };

  for (let index = 0; index < notification.length; index++) {
    myNotificationCount.notifications += 1;
  }

  for (let index = 0; index < user.length; index++) {
    myNotificationCount.friends += 1;
  }

  for (let index = 0; index < answer.length; index++) {
    myNotificationCount.requests += 1;
  }

  res.json(myNotificationCount);
});

// ######################################################################
// Administrator or operators API
// ######################################################################

exports.getNotifications = asyncHandler(async (req, res) => {
  const notificationList = [];

  const notification = await Notification.find({
    user: process.env.ADMIN_USER_ID,
  }).sort({
    createdAt: -1,
  });

  notification.forEach((element) => {
    notificationList.push({
      _id: element._id,
      user: element.user,
      isState: element.isState,
      title: element.title[0],
      message: element.message[0],
      review: element.review,
      createdAt: element.createdAt,
      updatedAt: element.updatedAt,
    });
  });

  if (notificationList) {
    res.json(notificationList);
  } else {
    res.json([]);
  }
});
