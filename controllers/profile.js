const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");

const User = require("../models/user");
const UserAudit = require("../models/userAudit");

exports.updateProfile = asyncHandler(async (req, res) => {
  let { firstname, lastname, phone, coordinates, mainInterests } = req.body;

  const userExists = await User.findById(req.user._id);

  if (!userExists) {
    res.status(404);
    throw new Error("User does not exist");
  }

  let auditArray = [];

  if (userExists.profile.firstname.toString() !== firstname.toString()) {
    auditArray.push({
      user: req.user._id,
      field: "firstname",
      oldValues: [userExists.profile.firstname],
      newValues: [firstname],
      action: 1,
    });
  }

  if (userExists.profile.lastname.toString() !== lastname.toString()) {
    auditArray.push({
      user: req.user._id,
      field: "lastname",
      oldValues: [userExists.profile.lastname],
      newValues: [lastname],
      action: 1,
    });
  }

  if (userExists.profile.phone.toString() !== phone.toString()) {
    auditArray.push({
      user: req.user._id,
      field: "phone",
      oldValues: [userExists.profile.phone],
      newValues: [phone],
      action: 1,
    });
  }

  for (let index = 0; index < coordinates.length; index++) {
    const element = coordinates[index];
    switch (index) {
      case 0:
        if (
          userExists.mainLocation.coordinates[0].toString() !==
          element.longitude.toString()
        ) {
          auditArray.push({
            user: req.user._id,
            field: "mainAddress",
            oldValues: [
              userExists.mainLocation.coordinates[1],
              userExists.mainLocation.coordinates[0],
              userExists.mainAddress,
            ],
            newValues: [element.latitude, element.longitude, element.address],
            action: 1,
          });
        }
        break;
      case 1:
        if (
          userExists.optionalLocation01.coordinates[0].toString() !==
          element.longitude.toString()
        ) {
          auditArray.push({
            user: req.user._id,
            field: "optionalAddress01",
            oldValues: [
              userExists.optionalLocation01.coordinates[1],
              userExists.optionalLocation01.coordinates[0],
              userExists.optionalAddress01,
            ],
            newValues: [element.latitude, element.longitude, element.address],
            action: 1,
          });
        }
        break;
      case 2:
        if (
          userExists.optionalLocation02.coordinates[0].toString() !==
          element.longitude.toString()
        ) {
          auditArray.push({
            user: req.user._id,
            field: "optionalAddress02",
            oldValues: [
              userExists.optionalLocation02.coordinates[1],
              userExists.optionalLocation02.coordinates[0],
              userExists.optionalAddress02,
            ],
            newValues: [element.latitude, element.longitude, element.address],
            action: 1,
          });
        }
      default:
        break;
    }
  }

  let difference = userExists.profile.mainInterests.filter(
    (x) => !mainInterests.includes(x)
  );

  if (
    difference.length === 0 ||
    userExists.profile.mainInterests.length !== mainInterests.length
  ) {
    await auditArray.push({
      user: userExists._id,
      field: "mainInterests",
      oldValues: userExists.mainInterests,
      newValues: mainInterests,
      type: 1,
    });
  }

  userExists.profile.firstname = firstname;
  userExists.profile.lastname = lastname;
  userExists.profile.phone = phone;
  userExists.profile.mainInterests = mainInterests;
  for (let index = 0; index < coordinates.length; index++) {
    const element = coordinates[index];
    switch (index) {
      case 0:
        userExists.mainLocation = {
          type: "Point",
          coordinates: [element.longitude, element.latitude],
        };
        userExists.mainAddress = element.address;
        break;
      case 1:
        userExists.optionalLocation01 = {
          type: "Point",
          coordinates: [element.longitude, element.latitude],
        };
        userExists.optionalAddress01 = element.address;
        break;
      case 2:
        userExists.optionalLocation02 = {
          type: "Point",
          coordinates: [element.longitude, element.latitude],
        };
        userExists.optionalAddress02 = element.address;
      default:
        break;
    }
  }

  await userExists.save();

  if (auditArray.length > 0) {
    for (let index = 0; index < auditArray.length; index++) {
      const element = auditArray[index];
      await UserAudit.create(element);
    }
  }

  const user = await User.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.user._id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        localField: "_id",
        foreignField: "user",
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "categories",
        localField: "profile.mainInterests",
        foreignField: "_id",
        as: "mainInterests",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "friends.user",
        foreignField: "_id",
        as: "friends",
      },
    },
    {
      $project: {
        username: 1,
        email: 1,
        type: 1,
        facebookToken: 1,
        googleToken: 1,
        notificationToken: 1,
        isActive: 1,
        authenticationMethod: 1,
        mainLocation: 1,
        mainAddress: 1,
        optionalAddress01: 1,
        optionalAddress02: 1,
        optionalLocation01: 1,
        optionalLocation02: 1,
        createdAt: 1,
        roles: 1,
        profile: {
          firstname: 1,
          lastname: 1,
          phone: 1,
          image: 1,
          mainInterests: "$mainInterests",
        },
        numberOfRecommendations: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
        numberOfFriends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (user.length > 0) {
    res.json(user[0]);
  } else {
    res.status(400);
    throw new Error("Profile not updated");
  }
});

exports.updateProfileInterests = asyncHandler(async (req, res) => {
  let { mainInterests } = req.body;

  const userExists = await User.findById(req.user._id);

  if (!userExists) {
    res.status(404);
    throw new Error("User does not exist");
  }

  let mainInterestObject = {};

  for (let index = 0; index < mainInterests.length; index++) {
    const element = mainInterests[index];
    switch (index) {
      case 0:
        if (userExists.profile.mainInterests[0] !== element) {
          mainInterestObject = {
            user: req.user._id,
            field: "mainInterests",
            oldValues: userExists.profile.mainInterests,
            newValues: mainInterests,
            action: 1,
          };
        }
        break;
      case 1:
        if (userExists.profile.mainInterests[1] !== element) {
          mainInterestObject = {
            user: req.user._id,
            field: "mainInterests",
            oldValues: userExists.profile.mainInterests,
            newValues: mainInterests,
            action: 1,
          };
        }
        break;
      case 2:
        if (userExists.profile.mainInterests[1] !== element) {
          mainInterestObject = {
            user: req.user._id,
            field: "mainInterests",
            oldValues: userExists.profile.mainInterests,
            newValues: mainInterests,
            action: 1,
          };
        }
        break;
      default:
        break;
    }
  }

  userExists.profile.mainInterests = mainInterests;

  await userExists.save();

  await UserAudit.create(mainInterestObject);

  const user = await User.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.user._id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        localField: "_id",
        foreignField: "user",
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "categories",
        localField: "profile.mainInterests",
        foreignField: "_id",
        as: "mainInterests",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "friends.user",
        foreignField: "_id",
        as: "friends",
      },
    },
    {
      $project: {
        username: 1,
        email: 1,
        type: 1,
        facebookToken: 1,
        googleToken: 1,
        notificationToken: 1,
        isActive: 1,
        authenticationMethod: 1,
        mainLocation: 1,
        mainAddress: 1,
        optionalAddress01: 1,
        optionalAddress02: 1,
        optionalLocation01: 1,
        optionalLocation02: 1,
        createdAt: 1,
        roles: 1,
        profile: {
          firstname: 1,
          lastname: 1,
          phone: 1,
          image: 1,
          mainInterests: "$mainInterests",
        },
        numberOfRecommendations: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
        numberOfFriends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (user.length > 0) {
    res.json(user[0]);
  } else {
    res.status(400);
    throw new Error("Interests not updated");
  }
});

exports.updateProfileImage = asyncHandler(async (req, res) => {
  const file = req.file;

  if (!file) {
    res.status(404);
    throw new Error("No image found");
  }

  const basePath = "public/images/profiles/";
  const fileName = file.filename;

  const userExists = await User.findById({ _id: req.user._id });

  if (!userExists) {
    res.status(404);
    throw new Error("User does not exist");
  }

  userExists.profile.image = `${basePath}${fileName}`;

  await userExists.save();

  await UserAudit.create({
    user: req.user._id,
    field: "image",
    oldValues: userExists.profile.image,
    newValues: `${basePath}${fileName}`,
    action: 1,
  });

  const user = await User.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.user._id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        localField: "_id",
        foreignField: "user",
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "categories",
        localField: "profile.mainInterests",
        foreignField: "_id",
        as: "mainInterests",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "friends.user",
        foreignField: "_id",
        as: "friends",
      },
    },
    {
      $project: {
        username: 1,
        email: 1,
        type: 1,
        facebookToken: 1,
        googleToken: 1,
        notificationToken: 1,
        isActive: 1,
        authenticationMethod: 1,
        mainLocation: 1,
        mainAddress: 1,
        optionalAddress01: 1,
        optionalAddress02: 1,
        optionalLocation01: 1,
        optionalLocation02: 1,
        createdAt: 1,
        roles: 1,
        profile: {
          firstname: 1,
          lastname: 1,
          phone: 1,
          image: 1,
          mainInterests: "$mainInterests",
        },
        numberOfRecommendations: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
        numberOfFriends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (user.length > 0) {
    res.json(user[0]);
  } else {
    res.status(400);
    throw new Error("Profile image was not updated");
  }
});
