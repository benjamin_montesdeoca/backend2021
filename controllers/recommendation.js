require("dotenv").config();
const asyncHandler = require("express-async-handler");
const { Expo } = require("expo-server-sdk");
const mongoose = require("mongoose");

const Recommendation = require("../models/recommendation");
const RecommendationAudit = require("../models/recommendationAudit");
const RecommendationMetric = require("../models/recommendationMetric");
const Comment = require("../models/comment");
const Business = require("../models/business");
const Notification = require("../models/notification");
const BlackList = require("../models/blacklist");
const User = require("../models/user");
const UserTracking = require("../models/userTracking");
const Global = require("../models/global");

const sendPushNotification = require("../utils/pushToken");
const {
  getMilles,
  completeMonthsForYear,
  completeWeeksForYear,
} = require("../utils/mathCalculations");
const notification = require("../helpers/notifications");

// ######################################################################
// User APIs
// ######################################################################

//#region
exports.createRecommendation = asyncHandler(async (req, res) => {
  let {
    business,
    typeRecommendation,
    askFor,
    tryThis,
    best,
    isShow,
    usedAs,
    review,
  } = req.body;
  let recommendedBy = 0;

  const recommendationExists = await Recommendation.findOne({
    user: mongoose.Types.ObjectId(req.user._id),
    business: mongoose.Types.ObjectId(business),
  });

  if (recommendationExists) {
    res.status(400);
    throw new Error("You have already recommended this business");
  }

  const askforArray = askFor.split(" ");
  const tryThisArray = tryThis.split(" ");
  const bestArray = best.split(" ");
  const reviewArray = review.split(" ");

  const newAskFor = [];
  const newTryThis = [];
  const newBest = [];
  const newReview = [];

  for (let index = 0; index < reviewArray.length; index++) {
    const element = reviewArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newReview.push("****");
    } else {
      newReview.push(element);
    }
  }

  for (let index = 0; index < askforArray.length; index++) {
    const element = askforArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newAskFor.push("****");
    } else {
      newAskFor.push(element);
    }
  }

  for (let index = 0; index < tryThisArray.length; index++) {
    const element = tryThisArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newTryThis.push("****");
    } else {
      newTryThis.push(element);
    }
  }

  for (let index = 0; index < bestArray.length; index++) {
    const element = bestArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newBest.push("****");
    } else {
      newBest.push(element);
    }
  }

  const user = await User.findById(req.user._id);

  const existingBusiness = await Business.findById(business);

  if (!existingBusiness) {
    res.status(400);
    throw new Error("This business does not exist");
  }

  const distance = getMilles(
    user.mainLocation.coordinates[1],
    user.mainLocation.coordinates[0],
    existingBusiness.location.coordinates[1],
    existingBusiness.location.coordinates[0]
  );

  if (distance > 50) {
    recommendedBy = 2;
  } else {
    recommendedBy = 1;
  }

  await Recommendation.create({
    user: req.user._id,
    business,
    typeRecommendation,
    askFor: newAskFor.join(" "),
    tryThis: newTryThis.join(" "),
    best: newBest.join(" "),
    review: newReview.join(" "),
    isShow,
    usedAs,
    recommendedBy,
  });

  await UserTracking.create({
    user: req.user._id,
    business: business,
    action: 2,
  });

  await RecommendationMetric.create({
    user: req.user._id,
    business: business,
    score: 1,
  });

  const recommendedBusiness = await Business.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(business),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ["$business", "$$business"] },
                  { $eq: ["$user", mongoose.Types.ObjectId(req.user._id)] },
                ],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        recommendation: 1,
        status: 1,
        categories: 1,
        tags: 1,
        userCreator: 1,
        ownerUser: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  let chosenRecommendation;

  recommendedBusiness.forEach((element) => {
    chosenRecommendation = element;
  });

  const nearUsers = await User.find({
    $and: [
      {
        mainLocation: {
          $near: {
            $geometry: {
              type: "Point",
              coordinates: [
                existingBusiness.location.coordinates[0],
                existingBusiness.location.coordinates[1],
              ],
            },
            $maxDistance: 50 * 1000,
          },
        },
      },
      {
        _id: { $ne: mongoose.Types.ObjectId(req.user._id) },
      },
    ],
  });

  if (existingBusiness.ownerUser === process.env.ADMIN_USER_ID) {
    await Notification.create({
      user: existingBusiness.userCreator,
      title: notification.notificationTitle(9),
      message: notification.notificationMessage(9, existingBusiness.name),
      isState: 0,
      type: 9,
    });
  } else {
    await Notification.create({
      user: existingBusiness.ownerUser,
      title: notification.notificationTitle(9),
      message: notification.notificationMessage(9, existingBusiness.name),
      isState: 0,
      type: 9,
    });
  }

  for (let index = 0; index < nearUsers.length; index++) {
    const element = nearUsers[index];

    const global = await Global.findOne({ user: element._id });

    if (Expo.isExpoPushToken(element.notificationToken)) {
      await sendPushNotification(
        element.notificationToken,
        notification.notificationPushMessage(
          global.language,
          7,
          existingBusiness.name
        )
      );
    }
  }

  if (chosenRecommendation) {
    res.status(201).json(chosenRecommendation);
  } else {
    res.status(400);
    throw new Error("Recommendation not created");
  }
});

exports.createRecommendationFromContact = asyncHandler(async (req, res) => {
  const {
    number,
    name,
    lastname,
    latitude,
    longitude,
    address,
    typeRecommendation,
  } = req.body;
  let recommendedBy = 0;

  const business = await Business.findOne({
    phone: number,
  });

  if (business) {
    const recommendationExists = await Recommendation.findOne({
      user: mongoose.Types.ObjectId(req.user._id),
      business: mongoose.Types.ObjectId(business._id),
    });

    if (recommendationExists) {
      res.status(400);
      throw new Error("You have already recommended this business");
    }

    const user = await User.findById(req.user._id);

    const distance = getMilles(
      user.mainLocation.coordinates[1],
      user.mainLocation.coordinates[0],
      business.location.coordinates[1],
      business.location.coordinates[0]
    );

    if (distance > 50) {
      recommendedBy = 2;
    } else {
      recommendedBy = 1;
    }

    await Recommendation.create({
      user: req.user._id,
      business: business._id,
      typeRecommendation: typeRecommendation,
      askFor: "",
      tryThis: "",
      best: "",
      review: "",
      isShow: 0,
      usedAs: 2,
      recommendedBy,
    });

    await UserTracking.create({
      user: req.user._id,
      business: business._id,
      action: 2,
    });

    await RecommendationMetric.create({
      user: req.user._id,
      business: business._id,
      score: 1,
    });

    const recommendedBusiness = await Business.aggregate([
      {
        $match: {
          $and: [
            {
              _id: {
                $eq: mongoose.Types.ObjectId(business._id),
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: "recommendations",
          let: { business: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$business", "$$business"] },
                    { $eq: ["$user", mongoose.Types.ObjectId(req.user._id)] },
                  ],
                },
              },
            },
          ],
          as: "recommendation",
        },
      },
      {
        $lookup: {
          from: "tags",
          localField: "tags",
          foreignField: "_id",
          as: "tags",
        },
      },
      {
        $project: {
          name: 1,
          image: 1,
          images: 1,
          description: 1,
          mainAddress: 1,
          openingHours: 1,
          socialLinks: 1,
          website: 1,
          phone: 1,
          address: 1,
          location: 1,
          recommendation: 1,
          status: 1,
          categories: 1,
          tags: 1,
          userCreator: 1,
          ownerUser: 1,
          createdAt: 1,
          updatedAt: 1,
          atlers: {
            $cond: {
              if: { $isArray: "$recommendation" },
              then: { $size: "$recommendation" },
              else: "NA",
            },
          },
        },
      },
    ]);

    let chosenRecommendation;

    recommendedBusiness.forEach((element) => {
      chosenRecommendation = element;
    });

    if (chosenRecommendation) {
      res.status(201).json(chosenRecommendation);
    } else {
      res.status(400);
      throw new Error("Recommendation not created");
    }
  } else {
    const newBusiness = await Business.create({
      name: `${name} ${lastname}`,
      description: "",
      mainAddress: "",
      phone: number,
      website: "",
      location: {
        type: "Point",
        coordinates: [longitude, latitude],
      },
      behavior: 1,
      address: address,
      userCreator: req.user._id,
      ownerUser: process.env.ADMIN_USER_ID,
    });

    const user = await User.findById(req.user._id);

    const distance = getMilles(
      user.mainLocation.coordinates[1],
      user.mainLocation.coordinates[0],
      newBusiness.location.coordinates[1],
      newBusiness.location.coordinates[0]
    );

    if (distance > 50) {
      recommendedBy = 2;
    } else {
      recommendedBy = 1;
    }

    await Recommendation.create({
      user: req.user._id,
      business: newBusiness._id,
      typeRecommendation: typeRecommendation,
      askFor: "",
      tryThis: "",
      best: "",
      review: "",
      isShow: 0,
      usedAs: 2,
      recommendedBy,
    });

    await UserTracking.create({
      user: req.user._id,
      business: newBusiness._id,
      action: 2,
    });

    await RecommendationMetric.create({
      user: req.user._id,
      business: newBusiness._id,
      score: 1,
    });

    const recommendedBusiness = await Business.aggregate([
      {
        $match: {
          $and: [
            {
              _id: {
                $eq: mongoose.Types.ObjectId(newBusiness._id),
              },
            },
          ],
        },
      },
      {
        $lookup: {
          from: "recommendations",
          let: { business: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$business", "$$business"] },
                    { $eq: ["$user", mongoose.Types.ObjectId(req.user._id)] },
                  ],
                },
              },
            },
          ],
          as: "recommendation",
        },
      },
      {
        $lookup: {
          from: "tags",
          localField: "tags",
          foreignField: "_id",
          as: "tags",
        },
      },
      {
        $project: {
          name: 1,
          image: 1,
          images: 1,
          description: 1,
          mainAddress: 1,
          openingHours: 1,
          socialLinks: 1,
          website: 1,
          phone: 1,
          address: 1,
          location: 1,
          recommendation: 1,
          status: 1,
          categories: 1,
          tags: 1,
          userCreator: 1,
          ownerUser: 1,
          createdAt: 1,
          updatedAt: 1,
          atlers: {
            $cond: {
              if: { $isArray: "$recommendation" },
              then: { $size: "$recommendation" },
              else: "NA",
            },
          },
        },
      },
    ]);

    let chosenRecommendation;

    recommendedBusiness.forEach((element) => {
      chosenRecommendation = element;
    });

    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;
    const client = require("twilio")(accountSid, authToken);

    client.messages
      .create({
        body: `Algún amigo tuyo te ha incluido en Locals Atlas como negocio o servicio, reclama tu propiedad ya`,
        messagingServiceSid: `${process.env.TWILIO_MESSAGING_SERVICES_ID}`,
        to: `${number}`,
      })
      .then((message) => {
        console.log(message.sid);
      })
      .catch((error) => {
        res.json({ error: error });
      });

    if (chosenRecommendation) {
      res.status(201).json(chosenRecommendation);
    } else {
      res.status(400);
      throw new Error("Recommendation not created");
    }
  }
});

exports.updateRecommendation = asyncHandler(async (req, res) => {
  let { typeRecommendation, askFor, tryThis, best, isShow, usedAs, review } =
    req.body;

  const recommendationExists = await Recommendation.findById(req.params.id);

  if (!recommendationExists) {
    res.status(400);
    throw new Error("Recommendation not found");
  }

  const recommendation = await Recommendation.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        typeRecommendation: typeRecommendation,
        askFor: askFor,
        tryThis: tryThis,
        best: best,
        isShow: isShow,
        usedAs: usedAs,
        review: review,
      },
    },
    {
      new: true,
      upsert: true,
    }
  );

  if (recommendation) {
    res.json(recommendation);
  } else {
    res.status(400);
    throw new Error("The recommendation was not updated");
  }
});

exports.removeRecommendation = asyncHandler(async (req, res) => {
  const { reason } = req.body;

  const recommendationExists = await Recommendation.findById(req.params.id);

  if (!recommendationExists) {
    res.status(400);
    throw new Error("Recommendation not found");
  }

  await RecommendationAudit.create({
    user: recommendationExists.user,
    business: recommendationExists.business,
    typeRecommendation: recommendationExists.typeRecommendation,
    askFor: recommendationExists.askFor,
    tryThis: recommendationExists.tryThis,
    best: recommendationExists.best,
    isShown: recommendationExists.isShow,
    usedAs: recommendationExists.usedAs,
    recommendedBy: recommendationExists.recommendedBy,
    review: recommendationExists.review,
    reason: reason,
  });

  await RecommendationMetric.create({
    user: recommendationExists.user,
    business: recommendationExists.business,
    score: -1,
  });

  const businessExists = await Business.findById(recommendationExists.business);

  await Recommendation.findByIdAndRemove(req.params.id);

  if (businessExists.ownerUser.toString() === process.env.ADMIN_USER_ID) {
    await Notification.create({
      user: businessExists.userCreator,
      title: notification.notificationTitle(12),
      message: notification.notificationMessage(12, businessExists.name),
      isState: 0,
      type: 12,
    });

    const user = await User.findById(businessExists.userCreator);

    const global = await Global.findOne({ user: user._id });

    await sendPushNotification(
      user.notificationToken,
      notification.notificationPushMessage(
        global.language,
        6,
        businessExists.name
      )
    );
  } else {
    await Notification.create({
      user: businessExists.ownerUser,
      title: notification.notificationTitle(12),
      message: notification.notificationMessage(12, businessExists.name),
      isState: 0,
      type: 12,
    });

    const user = await User.findById(businessExists.ownerUser);

    const global = await Global.findOne({ user: user._id });

    await sendPushNotification(
      user.notificationToken,
      notification.notificationPushMessage(
        global.language,
        6,
        businessExists.name
      )
    );
  }

  const business = await Business.aggregate([
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(recommendationExists.business),
            },
          },
        ],
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        status: 1,
        behavior: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (business.length > 0) {
    res.json(business[0]);
  } else {
    res.status(400);
    throw new Error("The recommendation was not updated");
  }
});

exports.createComment = asyncHandler(async (req, res) => {
  const { comment, recommendation } = req.body;

  const recommendationExists = await Recommendation.findById(recommendation);

  if (!recommendationExists) {
    res.status(400);
    throw new Error("Recommendation not found");
  }

  const commentArray = comment.split(" ");

  const newComment = [];
  let usingBadWords = false;

  for (let index = 0; index < commentArray.length; index++) {
    const element = commentArray[index];
    const existBadword = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (existBadword) {
      newComment.push("****");
      usingBadWords = true;
    } else {
      newComment.push(element);
    }
  }

  const myComment = await Comment.create({
    user: req.user._id,
    recommendation: recommendation,
    comment: newComment.join(" "),
  });

  const user = await User.findById(req.user._id);

  const global = await Global.findOne({ user: user._id });

  if (Expo.isExpoPushToken(user.notificationToken)) {
    await sendPushNotification(
      user.notificationToken,
      notification.notificationPushMessage(global.language, 12, user.username)
    );
  }

  if (usingBadWords) {
    await Notification.create({
      user: req.user._id,
      title: notification.notificationTitle(10),
      message: notification.notificationMessage(10),
      review: "Use of bad language may be punishable by account banning",
      isState: 0,
      type: 10,
    });
  }

  if (myComment) {
    const recommendation = await Recommendation.aggregate([
      {
        $lookup: {
          from: "comments",
          let: { recommendation: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$recommendation", "$$recommendation"] }],
                },
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "user",
                foreignField: "_id",
                as: "user",
              },
            },
            {
              $project: {
                comment: 1,
                createdAt: 1,
                user: { _id: 1, username: 1, profile: 1 },
              },
            },
          ],
          as: "comments",
        },
      },
    ]);

    res.json(recommendation);
  } else {
    res.json([]);
  }
});

exports.getRecommendationFromUser = asyncHandler(async (req, res) => {
  const recommendation = await Recommendation.aggregate([
    {
      $match: {
        $and: [
          {
            user: { $eq: mongoose.Types.ObjectId(req.params.user) },
            business: { $eq: mongoose.Types.ObjectId(req.params.business) },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $lookup: {
        from: "comments",
        let: { recommendation: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$recommendation", "$$recommendation"] }],
              },
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "user",
              foreignField: "_id",
              as: "user",
            },
          },
          {
            $project: {
              comment: 1,
              createdAt: 1,
              user: { _id: 1, username: 1, profile: 1 },
            },
          },
          { $sort: { createdAt: -1 } },
        ],
        as: "comments",
      },
    },
    {
      $project: {
        _id: 1,
        status: 1,
        askFor: 1,
        tryThis: 1,
        best: 1,
        isShown: 1,
        review: 1,
        business: 1,
        typeRecommendation: 1,
        usedAs: 1,
        recommendedBy: 1,
        createdAt: 1,
        updatedAt: 1,
        comments: 1,
        user: {
          _id: 1,
          username: 1,
          profile: { firstname: 1, lastname: 1, image: 1 },
        },
      },
    },
    { $sort: { createdAt: -1 } },
  ]);

  if (recommendation) {
    res.json(recommendation);
  } else {
    res.json([]);
  }
});

exports.isThisBusinessRecommendedByMe = asyncHandler(async (req, res) => {
  const business = await Recommendation.findOne({
    user: mongoose.Types.ObjectId(req.user._id),
    business: mongoose.Types.ObjectId(req.params.id),
  });

  if (business) {
    res.json({ isRecommended: true });
  } else {
    res.json({ isRecommended: false });
  }
});

exports.countIfLocalsOrTravellers = asyncHandler(async (req, res) => {
  const locals = await Recommendation.find({
    recommendedBy: 1,
    business: mongoose.Types.ObjectId(req.params.id),
  }).countDocuments();
  const travellers = await Recommendation.find({
    recommendedBy: 2,
    business: mongoose.Types.ObjectId(req.params.id),
  }).countDocuments();

  res.json({ areLocals: locals, areTravellers: travellers });
});

exports.countMonthlyAtlers = asyncHandler(async (req, res) => {
  const currentYear = new Date().getFullYear();

  const atlersBeforeCurrentYear = await RecommendationMetric.aggregate([
    { $match: { business: mongoose.Types.ObjectId(req.params.id) } },
    {
      $group: {
        _id: { year: { $year: "$createdAt" } },
        y: { $sum: "$score" },
      },
    },
    { $match: { "_id.year": { $lt: currentYear } } },
    { $sort: { "_id.year": 1 } },
  ]);

  let totalAtlersBeforeCurrentYear = 0;

  atlersBeforeCurrentYear.forEach((element) => {
    totalAtlersBeforeCurrentYear += element.y;
  });

  const currentYearAtlers = await RecommendationMetric.aggregate([
    {
      $match: { business: mongoose.Types.ObjectId(req.params.id) },
    },
    {
      $group: {
        _id: { year: { $year: "$createdAt" }, month: { $month: "$createdAt" } },
        y: { $sum: "$score" },
      },
    },
    { $match: { "_id.year": currentYear } },
    { $sort: { "_id.year": 1, "_id.month": 1 } },
  ]);

  let auxCurrentYearAtlers = [{ _id: 0, y: 0 }];

  currentYearAtlers.forEach((element) => {
    auxCurrentYearAtlers.push({ _id: element._id.month, y: element.y });
  });

  const completeCurrentYearAtlers = completeMonthsForYear(auxCurrentYearAtlers);

  let auxSum = 0;
  let allAtlers = [];

  if (totalAtlersBeforeCurrentYear > 0) {
    auxSum = totalAtlersBeforeCurrentYear;
  } else {
    auxSum = 0;
  }

  for (let k = 0; k < completeCurrentYearAtlers.length; k++) {
    auxSum += completeCurrentYearAtlers[k].y;
    allAtlers.push({ _id: completeCurrentYearAtlers[k]._id, y: auxSum });
  }

  if (completeCurrentYearAtlers.length <= 1) {
    res.json([]);
  } else {
    res.json(allAtlers);
  }
});

exports.countWeeklyAtlers = asyncHandler(async (req, res) => {
  const currentYear = new Date().getFullYear();

  const atlersBeforeCurrentYear = await RecommendationMetric.aggregate([
    { $match: { business: mongoose.Types.ObjectId(req.params.id) } },
    {
      $group: {
        _id: { year: { $year: "$createdAt" } },
        y: { $sum: "$score" },
      },
    },
    { $match: { "_id.year": { $lt: currentYear } } },
    { $sort: { "_id.year": 1 } },
  ]);

  let totalAtlersBeforeCurrentYear = 0;

  atlersBeforeCurrentYear.forEach((element) => {
    totalAtlersBeforeCurrentYear += element.y;
  });

  const currentWeekAtlers = await RecommendationMetric.aggregate([
    {
      $match: { business: mongoose.Types.ObjectId(req.params.id) },
    },
    {
      $group: {
        _id: {
          year: { $year: "$createdAt" },
          month: { $month: "$createdAt" },
          week: { $week: "$createdAt" },
        },
        y: { $sum: "$score" },
      },
    },
    { $match: { "_id.year": currentYear } },
    { $sort: { "_id.year": 1, "_id.month": 1 } },
  ]);

  const auxCurrentWeeksAtlers = [{ _id: 0, y: 0 }];

  currentWeekAtlers.forEach((element) => {
    auxCurrentWeeksAtlers.push({ _id: element._id.week, y: element.y });
  });

  const completeCurrentWeeksAtlers = completeWeeksForYear(
    auxCurrentWeeksAtlers
  );

  let auxSum = 0;
  let allAtlers = [];

  if (totalAtlersBeforeCurrentYear > 0) {
    auxSum = totalAtlersBeforeCurrentYear;
  } else {
    auxSum = 0;
  }

  for (let k = 0; k < completeCurrentWeeksAtlers.length; k++) {
    auxSum += completeCurrentWeeksAtlers[k].y;
    allAtlers.push({ _id: completeCurrentWeeksAtlers[k]._id, y: auxSum });
  }

  if (completeCurrentWeeksAtlers.length > 1) {
    if (allAtlers.length <= 5) {
      res.json(allAtlers);
    } else {
      const atlers = allAtlers.slice(-5);

      res.json(atlers);
    }
  } else {
    res.json([]);
  }
});

//#endregion

// ######################################################################
// Admin APIs
// ######################################################################

//#region
exports.createRecommendationByAdmin = asyncHandler(async (req, res) => {
  let { typeRecommendation, askFor, tryThis, best, isShow, usedAs, review } =
    req.body;
  let recommendedBy = 0;

  const recommendationExists = await Recommendation.findOne({
    user: mongoose.Types.ObjectId(req.user._id),
    business: mongoose.Types.ObjectId(req.params.id),
  });

  if (recommendationExists) {
    res.status(400);
    throw new Error("You have already recommended this business");
  }

  const askforArray = askFor.split(" ");
  const tryThisArray = tryThis.split(" ");
  const bestArray = best.split(" ");
  const reviewArray = review.split(" ");

  const newAskFor = [];
  const newTryThis = [];
  const newBest = [];
  const newReview = [];

  for (let index = 0; index < reviewArray.length; index++) {
    const element = reviewArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newReview.push("****");
    } else {
      newReview.push(element);
    }
  }

  for (let index = 0; index < askforArray.length; index++) {
    const element = askforArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newAskFor.push("****");
    } else {
      newAskFor.push(element);
    }
  }

  for (let index = 0; index < tryThisArray.length; index++) {
    const element = tryThisArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newTryThis.push("****");
    } else {
      newTryThis.push(element);
    }
  }

  for (let index = 0; index < bestArray.length; index++) {
    const element = bestArray[index];

    const badwordExists = await BlackList.findOne({
      word: element.toString().toLowerCase(),
    });

    if (badwordExists) {
      newBest.push("****");
    } else {
      newBest.push(element);
    }
  }

  const user = await User.findById(req.user._id);

  const existingBusiness = await Business.findById(req.params.id);

  if (!existingBusiness) {
    res.status(400);
    throw new Error("This business does not exist");
  }

  const distance = getMilles(
    user.mainLocation.coordinates[1],
    user.mainLocation.coordinates[0],
    existingBusiness.location.coordinates[1],
    existingBusiness.location.coordinates[0]
  );

  if (distance > 50) {
    recommendedBy = 2;
  } else {
    recommendedBy = 1;
  }

  await Recommendation.create({
    user: req.user._id,
    business: req.params.id,
    typeRecommendation,
    askFor: newAskFor.join(" "),
    tryThis: newTryThis.join(" "),
    best: newBest.join(" "),
    review: newReview.join(" "),
    isShow,
    usedAs,
    recommendedBy,
  });

  await UserTracking.create({
    user: req.user._id,
    business: req.params.id,
    action: 2,
  });

  await RecommendationMetric.create({
    user: req.user._id,
    business: req.params.id,
    score: 1,
  });

  const recommendedBusiness = await Business.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ["$business", "$$business"] },
                  { $eq: ["$user", mongoose.Types.ObjectId(req.user._id)] },
                ],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        openingHours: 1,
        socialLinks: 1,
        website: 1,
        phone: 1,
        address: 1,
        location: 1,
        recommendation: 1,
        status: 1,
        categories: 1,
        tags: 1,
        userCreator: 1,
        ownerUser: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  let chosenRecommendation;

  recommendedBusiness.forEach((element) => {
    chosenRecommendation = element;
  });

  const nearUsers = await User.find({
    $and: [
      // {
      //   mainLocation: {
      //     $near: {
      //       $geometry: {
      //         type: "Point",
      //         coordinates: [
      //           existingBusiness.location.coordinates[0],
      //           existingBusiness.location.coordinates[1],
      //         ],
      //       },
      //       $maxDistance: 50 * 1000,
      //     },
      //   },
      // },
      {
        _id: { $ne: mongoose.Types.ObjectId(req.user._id) },
      },
    ],
  });

  if (existingBusiness.ownerUser === process.env.ADMIN_USER_ID) {
    await Notification.create({
      user: existingBusiness.userCreator,
      title: notification.notificationTitle(9),
      message: notification.notificationMessage(9, existingBusiness.name),
      isState: 0,
      type: 9,
    });
  } else {
    await Notification.create({
      user: existingBusiness.ownerUser,
      title: notification.notificationTitle(9),
      message: notification.notificationMessage(9, existingBusiness.name),
      isState: 0,
      type: 9,
    });
  }

  for (let index = 0; index < nearUsers.length; index++) {
    const element = nearUsers[index];

    const global = await Global.findOne({ user: element._id });

    if (Expo.isExpoPushToken(element.notificationToken)) {
      await sendPushNotification(
        element.notificationToken,
        notification.notificationPushMessage(
          global.language,
          7,
          existingBusiness.name
        )
      );
    }
  }

  if (chosenRecommendation) {
    res.status(201).json(chosenRecommendation);
  } else {
    res.status(400);
    throw new Error("Recommendation not created");
  }
});
//#endregion
