const asyncHandler = require("express-async-handler");
const { Expo } = require("expo-server-sdk");

const sendPushNotification = require("../utils/pushToken");
const notification = require("../helpers/notifications");

const Business = require("../models/business");
const BusinessReasonsReport = require("../models/businessReasonsReport");
const Report = require("../models/report");
const Notification = require("../models/notification");
const User = require("../models/user");
const UserReasonsReport = require("../models/userReasonsReport");
const Behavior = require("../models/behavior");
const Global = require("../models/global");

exports.reportABusiness = asyncHandler(async (req, res) => {
  const { business, type, reason, email, phone, review } = req.body;

  const businessExists = await Business.findById(business);

  if (!businessExists) {
    res.status(404);
    throw new Error("Business not found");
  }

  let report = {};

  if (type === 2) {
    report = await Report.create({
      user: req.user._id,
      business,
      type,
      reason: "61e17a95d1f87f3074528c1d",
      review,
      email: email ? email : null,
      phone: phone ? phone : null,
    });
  }

  if (type === 1) {
    report = await Report.create({
      user: req.user._id,
      business,
      type,
      reason,
      review,
      email: email ? email : null,
      phone: phone ? phone : null,
    });
  }

  let reasonText = await BusinessReasonsReport.findById(reason);

  await Business.findOneAndUpdate(
    { _id: business },
    { $set: { behavior: 4 } },
    { new: true, upsert: true }
  );

  if (businessExists.ownerUser === process.env.ADMIN_USER_ID) {
    const user = await User.findById(businessExists.userCreator);

    const global = await Global.findOne({ user: user._id });

    if (Expo.isExpoPushToken(user.notificationToken)) {
      await sendPushNotification(
        user.notificationToken,
        notification.notificationPushMessage(
          global.language,
          8,
          businessExists.name
        ),
        "businessHome"
      );
    }

    await Notification.create({
      user: user._id,
      title: notification.notificationTitle(2),
      message: notification.notificationMessage(2, businessExists.name),
      review: review,
      isState: 0,
      type: 2,
    });

    await Notification.create({
      user: process.env.ADMIN_USER_ID,
      title: notification.notificationTitle(2),
      message: notification.notificationMessage(2, businessExists.name),
      review: `${businessExists._id}`,
      isState: 0,
      type: 2,
    });
  } else {
    const user = await User.findById(businessExists.ownerUser);

    const global = await Global.findOne({ user: user._id });

    if (Expo.isExpoPushToken(user.notificationToken)) {
      await sendPushNotification(
        user.notificationToken,
        notification.notificationPushMessage(
          global.language,
          8,
          businessExists.name
        ),
        "businessHome"
      );
    }

    if (user._id !== process.env.ADMIN_USER_ID) {
      await Notification.create({
        user: user._id,
        title: "Business report",
        message: notification.notificationMessage(2, businessExists.name),
        review: review,
        isState: 0,
        type: 2,
      });
    }

    await Notification.create({
      user: process.env.ADMIN_USER_ID,
      title: notification.notificationTitle(2),
      message: notification.notificationMessage(2, businessExists.name),
      review: `${businessExists._id}`,
      isState: 0,
      type: 2,
    });
  }

  const myNotification = await Notification.create({
    user: req.user._id,
    title: notification.notificationTitle(8),
    message: notification.notificationMessage(8, businessExists.name),
    review: review,
    isState: 0,
    type: 8,
  });

  const myReport = await Report.aggregate([
    {
      $match: {
        _id: report._id,
      },
    },
    {
      $addFields: {
        notification: myNotification,
      },
    },
  ]);

  if (myReport.length > 0) {
    res.json(myReport[0]);
  } else {
    res.json({});
  }
});

exports.reportAUser = asyncHandler(async (req, res) => {
  const { reason, review } = req.body;

  const userExists = await User.findById(req.params.id);

  if (!userExists) {
    res.status(404);
    throw new Error("User not found");
  }

  const behaviorExists = await Behavior.findOne({
    reportedUser: req.params.id,
    reportingUser: req.user._id,
  });

  if (behaviorExists) {
    res.status(400);
    throw new Error("You have reported this user before");
  }

  const behavior = await Behavior.create({
    reportedUser: req.params.id,
    reportingUser: req.user._id,
    behavior: 3,
    reason: reason,
    status: 2,
    review: review,
  });

  const user = await User.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { status: 3 } },
    { new: true, upsert: true }
  );

  const global = await Global.findOne({ user: user._id });

  if (Expo.isExpoPushToken(user.notificationToken)) {
    await sendPushNotification(
      user.notificationToken,
      notification.notificationPushMessage(global.language, 9),
      "businessHome"
    );
  }

  await Notification.create({
    user: req.params.id,
    title: notification.notificationTitle(10),
    message: notification.notificationMessage(10, user.username),
    review: review,
    type: 10,
  });

  await Notification.create({
    user: process.env.ADMIN_USER_ID,
    title: notification.notificationTitleForAdmin(10),
    message: notification.notificationMessage(10, user.username),
    review: `${req.params.id}`,
    isState: 0,
    type: 10,
  });

  const myNotification = await Notification.create({
    user: req.user._id,
    title: notification.notificationTitle(11),
    message: notification.notificationMessage(11),
    review: review,
    isState: 0,
    type: 11,
  });

  const myReportBehavior = await Behavior.aggregate([
    {
      $match: {
        _id: behavior._id,
      },
    },
    {
      $addFields: {
        notification: myNotification,
      },
    },
  ]);

  if (myReportBehavior.length > 0) {
    res.json(myReportBehavior[0]);
  } else {
    res.json({});
  }
});

exports.createBusinessReasonsReport = asyncHandler(async (req, res) => {
  let { description } = req.body;

  const reasonExists = await BusinessReasonsReport.findOne({
    description: description,
  });

  if (reasonExists) {
    res.status(400);
    throw new Error("Reason already exists");
  }

  const reason = await BusinessReasonsReport.create({ description });

  if (reason) {
    res.status(201).json(reason);
  } else {
    res.status(400);
    throw new Error("Reason not created");
  }
});

exports.getBusinessReasonsReport = asyncHandler(async (req, res) => {
  const reasons = await BusinessReasonsReport.find();

  res.json(reasons);
});

exports.createUserReasonsReport = asyncHandler(async (req, res) => {
  let { description } = req.body;

  const reasonExists = await UserReasonsReport.findOne({
    description: description,
  });

  if (reasonExists) {
    res.status(400);
    s;
    throw new Error("Reason already exists");
  }

  const reason = await UserReasonsReport.create({ description });

  if (reason) {
    res.status(201).json(reason);
  } else {
    res.status(400);
    throw new Error("Reason not created");
  }
});

exports.getUserReasonsReport = asyncHandler(async (req, res) => {
  const reasons = await UserReasonsReport.find();

  res.json(reasons);
});
