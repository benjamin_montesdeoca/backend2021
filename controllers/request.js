const asyncHandler = require("express-async-handler");
const { Expo } = require("expo-server-sdk");
const mongoose = require("mongoose");

const User = require("../models/user");
const UserTracking = require("../models/userTracking");
const Request = require("../models/request");
const Business = require("../models/business");
const Answer = require("../models/answer");
const Notification = require("../models/notification");

const sendPushNotification = require("../utils/pushToken");
const Global = require("../models/global");
const notification = require("../helpers/notifications");

exports.createRequest = asyncHandler(async (req, res) => {
  let {
    lookingFor,
    latitude,
    longitude,
    distance,
    status,
    typeRequest,
    users,
  } = req.body;

  let request = {};
  let numberOfUsersSent = 0;
  let numberOfResponses = 0;

  const newRequest = await Request.create({
    lookingFor,
    location: {
      type: "Point",
      coordinates: [longitude, latitude],
    },
    distance,
    status,
    typeRequest,
    user: req.user._id,
  });

  const senderUser = await User.findById(req.user._id);

  for (let index = 0; index < users.length; index++) {
    const element = users[index];
    numberOfUsersSent += 1;
    const an = await Answer.create({
      user: element,
      request: newRequest._id,
    });

    const receiverUser = await User.findById(mongoose.Types.ObjectId(element));

    const global = await Global.findOne({ user: receiverUser._id });

    const requestForNotification = {
      _id: newRequest._id,
      userSender: senderUser._id,
      latitude: latitude,
      longitude: longitude,
      answerId: an._id,
      distance: newRequest.distance,
      firstname: senderUser.profile.firstname,
      lastname: senderUser.profile.lastname,
      image: senderUser.profile.image,
      lookingFor: newRequest.lookingFor,
      status: newRequest.status,
      createdAt: newRequest.createdAt,
      typeRequest: newRequest.typeRequest,
    };

    if (Expo.isExpoPushToken(receiverUser.notificationToken)) {
      await sendPushNotification(
        receiverUser.notificationToken,
        notification.notificationPushMessage(
          global.language,
          10,
          senderUser.username
        ),
        "mainrequest",
        requestForNotification
      );

      await Notification.create({
        user: receiverUser._id,
        title: notification.notificationTitle(6),
        message: notification.notificationMessage(6),
        isState: 0,
        type: 6,
      });
    }
  }

  const user = await User.findById(req.user._id);

  request = {
    _id: newRequest._id,
    lookingFor: newRequest.lookingFor,
    location: newRequest.location,
    distance: newRequest.distance,
    status: newRequest.status,
    createdAt: newRequest.createdAt,
    typeRequest: newRequest.typeRequest,
    user: user._id,
    firstname: user.profile.firstname,
    lastname: user.profile.lastname,
    image: user.profile.image,
    numberOfUsersSent: numberOfUsersSent,
    numberOfResponses: numberOfResponses,
  };

  if (request) {
    res.json(request);
  } else {
    res.status(400);
    throw new Error("Request not created");
  }
});

exports.answerRequest = asyncHandler(async (req, res) => {
  let { business } = req.body;

  const answerExists = await Answer.findById(req.params.id);

  if (!answerExists) {
    res.status(400);
    throw new Error("This answer does not exist");
  }

  await Answer.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: { status: 1, business: business },
    },
    { new: true, upsert: true }
  );

  await UserTracking.create({
    user: req.user._id,
    business: business,
    action: 3,
  });

  const newAnswer = await Answer.find({ _id: req.params.id }).populate("user");

  const request = await Request.findById(newAnswer[0].request);

  await UserTracking.create({
    user: request.user,
    business: business,
    action: 4,
  });

  const user = await User.findById(mongoose.Types.ObjectId(request.user));

  const global = await Global.findOne({ user: user._id });

  if (Expo.isExpoPushToken(user.notificationToken)) {
    await sendPushNotification(
      user.notificationToken,
      notification.notificationPushMessage(global.language, 11),
      "mainrequest"
    );

    await Notification.create({
      user: user._id,
      title: notification.notificationTitle(7),
      message: notification.notificationMessage(7),
      isState: 0,
      type: 7,
    });
  }

  const newBusiness = await Business.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(business),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "tags",
        localField: "tags",
        foreignField: "_id",
        as: "tags",
      },
    },
    {
      $project: {
        name: 1,
        userCreator: 1,
        ownerUser: 1,
        image: 1,
        images: 1,
        description: 1,
        mainAddress: 1,
        website: 1,
        phone: 1,
        address: 1,
        openingHours: 1,
        socialLinks: 1,
        location: 1,
        status: 1,
        categories: 1,
        tags: 1,
        createdAt: 1,
        updatedAt: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  const answer = {
    _id: request._id,
    userSender: user._id,
    firstname: user.profile.firstname,
    lastname: user.profile.lastname,
    image: user.profile.image,
    distance: request.distance,
    location: request.location,
    createdAt: request.createdAt,
    typeRequest: request.typeRequest,
    lookingFor: request.lookingFor,
    status: newAnswer[0].status,
    business: newBusiness,
    answerId: newAnswer[0]._id,
  };

  if (answer) {
    res.json(answer);
  } else {
    res.status(400);
    throw new Error("Answer was not updated");
  }
});

exports.createRequestAndAnswer = async (req, res, next) => {
  const { users, business, latitude, longitude } = req.body;
  const sendingUserId = req.user._id;

  const session = await mongoose.startSession();

  let existingSendingUser;
  let existingReceivingUsers;
  let existingBusiness;
  let sanitizedUsers = [];
  let createdRequests = [];
  let createdAnswers = [];
  let createdReceivingUsersNotification = [];
  let createdReceivingUsersTracking = [];
  let createdSendingUserTracking;
  let existingRequests;
  let existingSendingUserAnswers;
  let existingSendingUserAnswersId = [];
  let existingAnswers;

  try {
    existingSendingUser = await User.findById(sendingUserId);
  } catch (err) {
    // const error = new HttpError(
    //   "Creating request answer failed, please try again.",
    //   500,
    //   err.stack
    // );
    // return next(error);
    res.status(400);
    throw new Error("Request not created");
  }

  if (!existingSendingUser) {
    // const error = new HttpError(
    //   "Creating request answer, sending user not found.",
    //   404,
    //   null
    // );
    // return next(error);
  }

  try {
    existingBusiness = await Business.findById(business);
  } catch (err) {
    // const error = new HttpError(
    //   "Creating request answer, please try again.",
    //   500,
    //   err.stack
    // );
    // return next(error);
    console.log(err);
  }

  if (!existingBusiness) {
    // const error = new HttpError(
    //   "Creating request answer, business not found.",
    //   404,
    //   null
    // );
    // return next(error);
  }

  for (let index = 0; index < users.length; index++) {
    const user = users[index];
    sanitizedUsers.push(mongoose.Types.ObjectId(user));
  }

  try {
    existingReceivingUsers = await User.aggregate([
      {
        $match: {
          _id: { $in: sanitizedUsers },
        },
      },
      {
        $lookup: {
          from: "globals",
          localField: "_id",
          foreignField: "user",
          as: "globalUserCharacteristics",
        },
      },
      {
        $project: {
          _id: 1,
          username: 1,
          notificationToken: 1,
          globalUserCharacteristics: { $first: "$globalUserCharacteristics" },
        },
      },
    ]);
  } catch (err) {
    // const error = new HttpError(
    //   "Creating request answer, please try again.",
    //   500,
    //   err.stack
    // );
    // return next(error);
    console.log(err);
  }

  try {
    for (let index = 0; index < existingReceivingUsers.length; index++) {
      const user = existingReceivingUsers[index];

      createdRequests.push({
        lookingFor: "Direct",
        location: {
          type: "Point",
          coordinates: [longitude, latitude],
        },
        distance: 0,
        status: 1,
        typeRequest: 3,
        user: user._id,
      });
    }

    existingRequests = await Request.insertMany(createdRequests, {
      session: session,
    });

    for (let index = 0; index < existingRequests.length; index++) {
      const request = existingRequests[index];

      createdAnswers.push({
        user: sendingUserId,
        request: request._id,
        business: business,
        status: 1,
      });
    }

    existingSendingUserAnswers = await Answer.insertMany(createdAnswers, {
      session: session,
    });

    for (let index = 0; index < existingSendingUserAnswers.length; index++) {
      const answer = existingSendingUserAnswers[index];
      existingSendingUserAnswersId.push(answer._id);
    }

    for (let index = 0; index < existingReceivingUsers.length; index++) {
      const user = existingReceivingUsers[index];

      createdReceivingUsersNotification.push({
        user: user,
        title: notification.notificationTitle(13),
        message: notification.notificationMessage(13),
        isState: 0,
        type: 13,
      });
    }

    await Notification.insertMany(createdReceivingUsersNotification, {
      session: session,
    });

    for (let index = 0; index < existingReceivingUsers.length; index++) {
      const user = existingReceivingUsers[index];

      createdReceivingUsersTracking.push({
        user: user,
        business: business,
        action: 4,
      });
    }

    await UserTracking.insertMany(createdReceivingUsersTracking, {
      session: session,
    });

    createdSendingUserTracking = new Notification({
      user: sendingUserId,
      business: business,
      action: 3,
    });

    await createdSendingUserTracking.save({ session: session });

    existingAnswers = await Answer.aggregate([
      {
        $match: {
          _id: { $in: existingSendingUserAnswersId },
        },
      },
      {
        $lookup: {
          from: "businesses",
          let: { business: "$business" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$_id", "$$business"] }],
                },
              },
            },
            {
              $lookup: {
                from: "recommendations",
                let: { business: "$_id" },
                pipeline: [
                  {
                    $match: {
                      $expr: {
                        $and: [{ $eq: ["$business", "$$business"] }],
                      },
                    },
                  },
                ],
                as: "recommendation",
              },
            },
            {
              $lookup: {
                from: "tags",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
              },
            },
            {
              $project: {
                name: 1,
                userCreator: 1,
                ownerUser: 1,
                image: 1,
                images: 1,
                description: 1,
                mainAddress: 1,
                website: 1,
                phone: 1,
                address: 1,
                openingHours: 1,
                socialLinks: 1,
                location: 1,
                status: 1,
                categories: 1,
                tags: 1,
                createdAt: 1,
                updatedAt: 1,
                atlers: {
                  $cond: {
                    if: { $isArray: "$recommendation" },
                    then: { $size: "$recommendation" },
                    else: "NA",
                  },
                },
              },
            },
          ],
          as: "business",
        },
      },
      {
        $lookup: {
          from: "requests",
          localField: "request",
          foreignField: "_id",
          as: "request",
        },
      },
      {
        $project: {
          _id: { $first: "$request._id" },
          distance: { $first: "$request.distance" },
          location: { $first: "$request.location" },
          typeRequest: { $first: "$request.typeRequest" },
          lookingFor: { $first: "$request.lookingFor" },
          business: { $first: "$business" },
          answerId: "$_id",
        },
      },
      {
        $addFields: {
          userSender: existingSendingUser._id,
          firstname: existingSendingUser.profile.firstname,
          lastname: existingSendingUser.profile.lastname,
          image: existingSendingUser.profile.image,
          status: 1,
        },
      },
    ]);
  } catch (err) {
    // const error = new HttpError(
    //   "Creating request answer, please try again.",
    //   500,
    //   err.stack
    // );
    // return next(error);
    console.log(err);
  }

  res.json(existingAnswers);
};

exports.markAnswerAsAnswered = asyncHandler(async (req, res) => {
  const answerExists = await Answer.findById(req.params.id);

  if (!answerExists) {
    res.status(400);
    throw new Error("This answer does not exist");
  }

  const answer = await Answer.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: { status: 2 },
    },
    { new: true, upsert: true }
  );

  const numberOfNoResponses = await Answer.find({
    $and: [
      {
        request: mongoose.Types.ObjectId(answerExists.request),
      },
      { status: 0 },
    ],
  }).countDocuments();

  if (numberOfNoResponses === 0) {
    await Request.findOneAndUpdate(
      { _id: answer.request },
      {
        $set: { status: 1 },
      },
      { new: true, upsert: true }
    );
  }

  const request = await Request.findById(answer.request);

  const result = {
    _id: answer._id,
    answerStatus: answer.status,
    user: answer.user,
    request: answer.request,
    createdAt: answer.createAt,
    updatedAt: answer.updatedAt,
    requestStatus: request.status,
  };

  if (result) {
    res.json(result);
  } else {
    res.json([]);
  }
});

exports.getAllMyRequests = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 10;

  let request = {};
  let requests = [];

  const myRequests = await Request.find({ user: req.user._id })
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({ createdAt: -1 });

  const user = await User.findById(req.user._id);

  for (let index = 0; index < myRequests.length; index++) {
    const element = myRequests[index];

    const numberOfResponses = await Answer.find({
      $and: [
        {
          request: mongoose.Types.ObjectId(element._id),
        },
        { status: 1 },
      ],
    }).countDocuments();

    const numberOfNoResponses = await Answer.find({
      $and: [
        {
          request: mongoose.Types.ObjectId(element._id),
        },
        { status: 0 },
      ],
    }).countDocuments();

    if (numberOfNoResponses === 0) {
      await Request.findOneAndUpdate(
        { _id: element._id },
        {
          $set: { status: 1 },
        },
        { new: true, upsert: true }
      );
    }

    const numberOfUsersSent = await Answer.find({
      request: mongoose.Types.ObjectId(element._id),
    }).countDocuments();

    request = {
      _id: element._id,
      lookingFor: element.lookingFor,
      location: element.location,
      distance: element.distance,
      status: element.status,
      createdAt: element.createdAt,
      typeRequest: element.typeRequest,
      status: element.status,
      user: user._id,
      firstname: user.profile.firstname,
      lastname: user.profile.lastname,
      image: user.profile.image,
      numberOfUsersSent: numberOfUsersSent,
      numberOfResponses: numberOfResponses,
    };

    requests.push(request);
  }

  if (requests) {
    res.json(requests);
  } else {
    res.json([]);
  }
});

exports.getAllRequestsMadeToMe = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 10;

  const answer = await Answer.find({ user: req.user._id })
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({ createdAt: -1 });

  const requestIds = [];

  for (let index = 0; index < answer.length; index++) {
    const element = answer[index];
    requestIds.push(mongoose.Types.ObjectId(element.request));
  }

  const myRequests = await Request.find({ _id: { $in: requestIds } }).populate(
    "user"
  );

  const requests = [];

  for (let index = 0; index < myRequests.length; index++) {
    const element = myRequests[index];
    const myAnswer = await Answer.find({
      $and: [
        { user: mongoose.Types.ObjectId(req.user._id) },
        { request: mongoose.Types.ObjectId(element._id) },
      ],
    });

    let business = [];

    if (myAnswer[0].business) {
      business = await Business.aggregate([
        {
          $match: {
            $and: [
              {
                _id: {
                  $eq: mongoose.Types.ObjectId(myAnswer[0].business),
                },
              },
            ],
          },
        },
        {
          $lookup: {
            from: "recommendations",
            let: { business: "$_id" },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ["$business", "$$business"] }],
                  },
                },
              },
            ],
            as: "recommendation",
          },
        },
        {
          $lookup: {
            from: "tags",
            localField: "tags",
            foreignField: "_id",
            as: "tags",
          },
        },
        {
          $project: {
            name: 1,
            userCreator: 1,
            ownerUser: 1,
            image: 1,
            images: 1,
            description: 1,
            mainAddress: 1,
            website: 1,
            phone: 1,
            address: 1,
            openingHours: 1,
            socialLinks: 1,
            location: 1,
            status: 1,
            categories: 1,
            tags: 1,
            createdAt: 1,
            updatedAt: 1,
            atlers: {
              $cond: {
                if: { $isArray: "$recommendation" },
                then: { $size: "$recommendation" },
                else: "NA",
              },
            },
          },
        },
      ]);
    } else {
      business = [];
    }

    const request = {
      _id: element._id,
      userSender: element.user._id,
      firstname: element.user.profile.firstname,
      lastname: element.user.profile.lastname,
      image: element.user.profile.image,
      distance: element.distance,
      location: element.location,
      createdAt: element.createdAt,
      typeRequest: element.typeRequest,
      lookingFor: element.lookingFor,
      status: myAnswer[0].status,
      business: business,
      answerId: myAnswer[0]._id,
    };
    requests.push(request);
  }

  res.json(requests);
});

exports.getTopRequestsMadeToMe = asyncHandler(async (req, res) => {
  const answer = await Answer.find({ user: req.user._id })
    .limit(5)
    .sort({ createdAt: -1 });

  const requestIds = [];

  for (let index = 0; index < answer.length; index++) {
    const element = answer[index];
    requestIds.push(mongoose.Types.ObjectId(element.request));
  }

  const myRequests = await Request.find({ _id: { $in: requestIds } }).populate(
    "user"
  );

  const requests = [];

  for (let index = 0; index < myRequests.length; index++) {
    const element = myRequests[index];
    const myAnswer = await Answer.find({
      $and: [
        { user: mongoose.Types.ObjectId(req.user._id) },
        { request: mongoose.Types.ObjectId(element._id) },
      ],
    });

    let business = [];

    if (myAnswer[0].business) {
      business = await Business.aggregate([
        {
          $match: {
            $and: [
              {
                _id: {
                  $eq: mongoose.Types.ObjectId(myAnswer[0].business),
                },
              },
            ],
          },
        },
        {
          $lookup: {
            from: "recommendations",
            let: { business: "$_id" },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ["$business", "$$business"] }],
                  },
                },
              },
            ],
            as: "recommendation",
          },
        },
        {
          $lookup: {
            from: "tags",
            localField: "tags",
            foreignField: "_id",
            as: "tags",
          },
        },
        {
          $project: {
            name: 1,
            userCreator: 1,
            ownerUser: 1,
            image: 1,
            images: 1,
            description: 1,
            mainAddress: 1,
            website: 1,
            phone: 1,
            address: 1,
            openingHours: 1,
            socialLinks: 1,
            location: 1,
            status: 1,
            categories: 1,
            tags: 1,
            createdAt: 1,
            updatedAt: 1,
            atlers: {
              $cond: {
                if: { $isArray: "$recommendation" },
                then: { $size: "$recommendation" },
                else: "NA",
              },
            },
          },
        },
      ]);
    } else {
      business = [];
    }

    const request = {
      _id: element._id,
      userSender: element.user._id,
      firstname: element.user.profile.firstname,
      lastname: element.user.profile.lastname,
      image: element.user.profile.image,
      distance: element.distance,
      location: element.location,
      createdAt: element.createdAt,
      typeRequest: element.typeRequest,
      lookingFor: element.lookingFor,
      status: myAnswer[0].status,
      business: business,
      answerId: myAnswer[0]._id,
    };
    requests.push(request);
  }

  res.json(requests);
});

exports.getAllMyAnswers = asyncHandler(async (req, res) => {
  const answer = await Answer.find({ user: req.user._id });

  if (answer) {
    res.json(answer);
  } else {
    res.json({ message: "Aswers not found" });
  }
});

exports.getAnswerForRequest = asyncHandler(async (req, res) => {
  const answer = await Answer.aggregate([
    {
      $match: {
        $and: [
          {
            request: { $eq: mongoose.Types.ObjectId(req.params.id) },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        let: { business: "$business" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ["$business", "$$business"] }],
              },
            },
          },
        ],
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $lookup: {
        from: "businesses",
        localField: "business",
        foreignField: "_id",
        as: "business",
      },
    },
    {
      $project: {
        status: 1,
        request: 1,
        createdAt: 1,
        updatedAt: 1,
        user: {
          _id: 1,
          username: 1,
          profile: { firstname: 1, lastname: 1, image: 1 },
        },
        business: 1,
        recommendation: 1,
        atlers: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (answer) {
    res.json(answer);
  } else {
    res.json([]);
  }
});

exports.getMyFriendsForRequests = asyncHandler(async (req, res) => {
  const user = await User.find({
    $and: [
      // {
      //   mainLocation: {
      //     $near: {
      //       $geometry: {
      //         type: "Point",
      //         coordinates: [req.query.longitude, req.query.latitude],
      //       },
      //       $maxDistance: req.query.distance * 1000,
      //     },
      //   },
      // },
      {
        friends: {
          $elemMatch: {
            user: mongoose.Types.ObjectId(req.user._id),
            status: 3,
          },
        },
      },
    ],
  })
    .select("-password")
    .select("-friends")
    .limit(100);

  const friends = [];

  user.forEach((element) => {
    const friend = {
      _id: element._id,
      username: element.username,
      firstname: element.profile.firstname,
      lastname: element.profile.lastname,
      image: element.profile.image,
      mainLocation: element.mainLocation,
      mainAddress: element.mainAddress,
      check: false,
    };

    friends.push(friend);
  });

  res.json(friends);
});

exports.getLocalsForRequests = asyncHandler(async (req, res) => {
  const latitude = req.query.latitude ? Number(req.query.latitude) : 0;
  const longitude = req.query.longitude ? Number(req.query.longitude) : 0;
  const distance = req.query.distance ? Number(req.query.distance) : 10;

  const user = await User.find({
    $and: [
      {
        mainLocation: {
          $near: {
            $geometry: {
              type: "Point",
              coordinates: [longitude, latitude],
            },
            $maxDistance: distance * 1000,
          },
        },
      },
      { "friends.user": { $ne: mongoose.Types.ObjectId(req.user._id) } },
      { _id: { $ne: mongoose.Types.ObjectId(req.user._id) } },
      { _id: { $ne: mongoose.Types.ObjectId(`${process.env.ADMIN_USER_ID}`) } },
    ],
  })
    .select("-password")
    .select("-friends")
    .limit(100);

  const locals = [];

  user.forEach((element) => {
    const local = {
      _id: element._id,
      username: element.username,
      firstname: element.profile.firstname,
      lastname: element.profile.lastname,
      image: element.profile.image,
      mainLocation: element.mainLocation,
      mainAddress: element.mainAddress,
      check: false,
    };

    locals.push(local);
  });

  if (locals) {
    res.json(locals);
  } else {
    res.json([]);
  }
});
