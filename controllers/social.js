const asyncHandler = require("express-async-handler");

const Social = require("../models/social");

exports.createSocial = asyncHandler(async (req, res) => {
  let { name, url, image } = req.body;

  const socialExists = await Social.findOne({ name: name });

  if (socialExists) {
    res.status(400);
    throw new Error("Social already exists");
  }

  const social = await Social.create({
    name: name,
    url: url,
    image: image,
  });

  if (social) {
    res.json(social);
  } else {
    res.status(400);
    throw new Error("Social not created");
  }
});

exports.getSocial = asyncHandler(async (req, res) => {
  const social = await Social.find();

  res.json(social);
});
