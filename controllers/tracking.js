const asyncHandler = require("express-async-handler");

const UserTracking = require("../models/userTracking");

exports.userTracking = asyncHandler(async (req, res) => {
  const { data } = req.body;
  let allDataUploaded = false;

  for (let index = 0; index < data.length; index++) {
    const element = data[index];

    await UserTracking.create({
      user: req.user._id,
      business: element.businessId,
      action: element.type,
      screen: element.screen,
      initialDate: element.initialDate,
      finalDate: element.finalDate,
    });
    allDataUploaded = true;
  }

  if (allDataUploaded) {
    res.json({ message: "All data was updated succesfully", status: 1 });
  } else {
    res.json({ message: "Error when uploading data", status: -1 });
  }
});
