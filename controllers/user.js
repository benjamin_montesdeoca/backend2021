require("dotenv").config();
const asyncHandler = require("express-async-handler");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const jwt_decode = require("jwt-decode");
const sgMail = require("@sendgrid/mail");

const generateToken = require("../utils/generateToken");
const { generateCode } = require("../utils/mathCalculations");
const mails = require("../helpers/mails");

const User = require("../models/user");
const AdminAudit = require("../models/adminAudit");
const Global = require("../models/global");
const Business = require("../models/business");
const Recommendation = require("../models/recommendation");
const Token = require("../models/token");
const Comment = require("../models/comment");
const Answer = require("../models/answer");
const Request = require("../models/request");
const RecommendationAudit = require("../models/recommendationAudit");
const RecommendationMetric = require("../models/recommendationMetric");
const BusinessAudit = require("../models/businessAudit");
const Notification = require("../models/notification");
const Report = require("../models/report");
const Behavior = require("../models/behavior");
const Claim = require("../models/claim");
const ErrorAudit = require("../models/errorAudit");
const UserTracking = require("../models/userTracking");

// ######################################################################
// User APIs
// ######################################################################
//#region
exports.userRegister = asyncHandler(async (req, res) => {
  const { username, email, password, notificationToken } = req.body;

  const userExists = await User.findOne({ email });

  if (userExists && userExists.isActive === false) {
    return res.json({
      _id: userExists._id,
      username: userExists.username,
      email: userExists.email,
      isActive: userExists.isActive,
      status: userExists.status,
      token: generateToken(userExists._id),
      notificationToken: userExists.notificationToken,
      roles: userExists.roles,
      type: userExists.type,
      profile: userExists.profile,
      mainLocation: userExists.mainLocation,
      optionalLocation01: userExists.optionalLocation01,
      optionalLocation02: userExists.optionalLocation02,
      mainAddress: userExists.mainAddress,
      optionalAddress01: userExists.optionalAddress01,
      optionalAddress02: userExists.optionalAddress02,
    });
  }

  if (userExists && userExists.isActive === true) {
    res.status(400);
    throw new Error("User already exists");
  }

  const usernameSplited = username.split(" ");

  const usernameJoined = usernameSplited.join("");

  const newUser = await User.create({
    username: usernameJoined,
    email,
    password,
    notificationToken,
    profile: {
      image: "public/images/system/user.png",
      firstname: "New",
      lastname: "User",
      phone: "",
    },
    mainLocation: { type: "Point", coordinates: [0, 0] },
    optionalLocation01: { type: "Point", coordinates: [0, 0] },
    optionalLocation02: { type: "Point", coordinates: [0, 0] },
  });

  await Global.create({
    user: newUser._id,
    ipaddress: "",
    location: { type: "Point", coordinates: [0, 0] },
    device: "",
    language: 0,
    isNewUser: 1,
  });

  const user = await User.findById(newUser._id);

  if (user) {
    res.status(201).json({
      _id: user._id,
      username: user.username,
      email: user.email,
      isActive: user.isActive,
      token: generateToken(user._id),
      notificationToken: user.notificationToken,
      roles: user.roles,
      type: user.type,
      status: user.status,
      profile: user.profile,
      mainLocation: user.mainLocation,
      optionalLocation01: user.optionalLocation01,
      optionalLocation02: user.optionalLocation02,
      mainAddress: user.mainAddress,
      optionalAddress01: user.optionalAddress01,
      optionalAddress02: user.optionalAddress02,
    });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

exports.userLogin = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    res.status(404);
    throw new Error("User not found");
  }

  if (user && user.authenticationMethod !== 1) {
    res.status(401);
    throw new Error("User registered with another authentication method");
  }

  if (user && user.status === 2) {
    res.status(401);
    throw new Error("User suspended");
  }

  if (user && (await user.matchPassword(password)) && user.isActive === false) {
    res.json({
      _id: user._id,
      username: user.name,
      email: user.email,
      isActive: user.isActive,
      token: generateToken(user._id),
      notificationToken: user.notificationToken,
      roles: user.roles,
      status: user.status,
      type: user.type,
      profile: user.profile,
      mainLocation: user.mainLocation,
      optionalLocation01: user.optionalLocation01,
      optionalLocation02: user.optionalLocation02,
      mainAddress: user.mainAddress,
      optionalAddress01: user.optionalAddress01,
      optionalAddress02: user.optionalAddress02,
    });
  } else if (
    user &&
    user.isActive === true &&
    (await user.matchPassword(password))
  ) {
    res.json({
      _id: user._id,
      username: user.name,
      email: user.email,
      isActive: user.isActive,
      status: user.status,
      token: generateToken(user._id),
      notificationToken: user.notificationToken,
      roles: user.roles,
      type: user.type,
      profile: user.profile,
      mainLocation: user.mainLocation,
      optionalLocation01: user.optionalLocation01,
      optionalLocation02: user.optionalLocation02,
      mainAddress: user.mainAddress,
      optionalAddress01: user.optionalAddress01,
      optionalAddress02: user.optionalAddress02,
    });
  } else {
    res.status(401);
    throw new Error("Invalid email or password");
  }
});

exports.sendActivationCode = asyncHandler(async (req, res) => {
  const { email, phone, activationMethod } = req.body;
  const code = generateCode();

  if (activationMethod === "email") {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: "no_reply@localsatlas.com",
      subject: "Locals Atlas Authentication",
      text: "Complete the authentication proccess",
      html: mails.emailToAuthenticateAccount(code),
    };

    sgMail
      .send(msg)
      .then(() => {
        console.log("Email sent");
      })
      .catch((error) => {
        console.error(error);
      });

    res.json({ code: code });
  }

  if (activationMethod === "sms") {
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;
    const client = require("twilio")(accountSid, authToken);

    client.messages
      .create({
        body: `Welcome to Locals Atlas. This is your code: ${code}`,
        messagingServiceSid: `${process.env.TWILIO_MESSAGING_SERVICES_ID}`,
        to: `${phone}`,
      })
      .then((message) => {
        console.log(message.sid);
        res.json({ code: code });
      })
      .catch((error) => {
        res.json({ error: error });
      });
  }
});

exports.isUserActive = asyncHandler(
  asyncHandler(async (req, res) => {
    const userExists = await User.findById(req.user._id);

    if (!userExists) {
      res.status(404);
      throw new Error("User not found");
    }

    if (userExists.status === 1) {
      res.json({ isActive: true });
    } else {
      res.json({ isActive: false });
    }
  })
);

exports.activateUser = asyncHandler(async (req, res) => {
  const userExists = await User.findById(req.params.id);

  if (!userExists) {
    res.status(404);
    throw new Error("User not found");
  }

  await User.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { isActive: true } }
  );

  const user = await User.findById(req.params.id);

  if (user && user.isActive === true) {
    res.json({
      _id: user._id,
      username: user.username,
      email: user.email,
      isActive: user.isActive,
      token: generateToken(user._id),
      notificationToken: user.notificationToken,
      roles: user.roles,
      type: user.type,
      profile: user.profile,
      mainLocation: user.mainLocation,
      optionalLocation01: user.optionalLocation01,
      optionalLocation02: user.optionalLocation02,
      mainAddress: user.mainAddress,
      optionalAddress01: user.optionalAddress01,
      optionalAddress02: user.optionalAddress02,
    });
  } else {
    res.status(401);
    throw new Error("The user was not activated");
  }
});

exports.userRegisterFacebook = asyncHandler(async (req, res) => {
  const { data } = req.body;

  const userExists = await User.findOne({ email: data.email });

  if (userExists && userExists.status === 2) {
    res.status(401);
    throw new Error("User suspended");
  }

  if (userExists && userExists.authenticationMethod !== 2) {
    res.status(401);
    throw new Error("User registered with another authentication method");
  }

  if (userExists) {
    res.json({
      _id: userExists._id,
      username: userExists.username,
      email: userExists.email,
      isActive: userExists.isActive,
      token: generateToken(userExists._id),
      notificationToken: userExists.notificationToken,
      roles: userExists.roles,
      type: userExists.type,
      status: userExists.status,
      profile: userExists.profile,
      mainLocation: userExists.mainLocation,
      optionalLocation01: userExists.optionalLocation01,
      optionalLocation02: userExists.optionalLocation02,
      mainAddress: userExists.mainAddress,
      optionalAddress01: userExists.optionalAddress01,
      optionalAddress02: userExists.optionalAddress02,
    });
  } else {
    const usernameSplited = data.name.split(" ");

    const usernameJoined = usernameSplited.join("");

    const newUser = await User.create({
      username: usernameJoined,
      email: data.email,
      password: `${data.id}-${process.env.FACEBOOK_USER_PASSWORD}`,
      isActive: true,
      status: 1,
      authenticationMethod: 2,
      notificationToken: data.notificationToken ? data.notificationToken : "",
      profile: {
        image: data.picture.data.url
          ? data.picture.data.url
          : "public/images/system/user.png",
        firstname: "New",
        lastname: "User",
        phone: "",
      },
      mainLocation: { type: "Point", coordinates: [0, 0] },
      optionalLocation01: { type: "Point", coordinates: [0, 0] },
      optionalLocation02: { type: "Point", coordinates: [0, 0] },
      roles: "user",
    });

    const userFacebook = await User.findById(newUser._id);

    if (userFacebook) {
      await Global.create({
        user: userFacebook._id,
        ipaddress: "",
        location: { type: "Point", coordinates: [0, 0] },
        device: "",
        language: 0,
        isNewUser: 1,
      });

      res.status(201).json({
        _id: userFacebook._id,
        username: userFacebook.username,
        email: userFacebook.email,
        isActive: userFacebook.isActive,
        token: generateToken(userFacebook._id),
        notificationToken: userFacebook.notificationToken,
        roles: userFacebook.roles,
        type: userFacebook.type,
        status: userFacebook.status,
        profile: userFacebook.profile,
        mainLocation: userFacebook.mainLocation,
        optionalLocation01: userFacebook.optionalLocation01,
        optionalLocation02: userFacebook.optionalLocation02,
        mainAddress: userFacebook.mainAddress,
        optionalAddress01: userFacebook.optionalAddress01,
        optionalAddress02: userFacebook.optionalAddress02,
      });
    } else {
      res.status(404);
      throw new Error("User not found");
    }
  }
});

exports.userRegisterGoogle = asyncHandler(async (req, res) => {
  const { data } = req.body;

  const userExists = await User.findOne({ email: data.user.email });

  if (userExists && userExists.status === 2) {
    res.status(401);
    throw new Error("User suspended");
  }

  if (userExists && userExists.authenticationMethod !== 3) {
    res.status(401);
    throw new Error("User registered with another authentication method");
  }

  if (userExists) {
    res.json({
      _id: userExists._id,
      username: userExists.username,
      email: userExists.email,
      isActive: userExists.isActive,
      token: generateToken(userExists._id),
      notificationToken: userExists.notificationToken,
      roles: userExists.roles,
      type: userExists.type,
      status: userExists.status,
      profile: userExists.profile,
      mainLocation: userExists.mainLocation,
      optionalLocation01: userExists.optionalLocation01,
      optionalLocation02: userExists.optionalLocation02,
      mainAddress: userExists.mainAddress,
      optionalAddress01: userExists.optionalAddress01,
      optionalAddress02: userExists.optionalAddress02,
    });
  } else {
    const usernameSplited = data.user.name.split(" ");

    const usernameJoined = usernameSplited.join("");

    const newUser = await User.create({
      username: usernameJoined,
      email: data.user.email,
      password: `${data.user.id}-${process.env.GOOGLE_USER_PASSWORD}`,
      isActive: true,
      status: 1,
      authenticationMethod: 3,
      mainLocation: { type: "Point", coordinates: [0, 0] },
      optionalLocation01: { type: "Point", coordinates: [0, 0] },
      optionalLocation02: { type: "Point", coordinates: [0, 0] },
      notificationToken: data.notificationToken ? data.notificationToken : "",
      profile: {
        image: data.user.photoUrl
          ? data.user.photoUrl
          : "public/images/system/user.png",
        firstname: "New",
        lastname: "User",
        phone: "",
      },
      roles: "user",
    });

    const userGoogle = await User.findById(newUser._id);

    if (userGoogle) {
      await Global.create({
        user: userGoogle._id,
        ipaddress: "",
        location: { type: "Point", coordinates: [0, 0] },
        device: "",
        language: 0,
        isNewUser: 1,
      });

      res.status(201).json({
        _id: userGoogle._id,
        username: userGoogle.username,
        email: userGoogle.email,
        isActive: userGoogle.isActive,
        token: generateToken(userGoogle._id),
        notificationToken: userGoogle.notificationToken,
        roles: userGoogle.roles,
        type: userGoogle.type,
        status: userGoogle.status,
        profile: userGoogle.profile,
        mainLocation: userGoogle.mainLocation,
        optionalLocation01: userGoogle.optionalLocation01,
        optionalLocation02: userGoogle.optionalLocation02,
        mainAddress: userGoogle.mainAddress,
        optionalAddress01: userGoogle.optionalAddress01,
        optionalAddress02: userGoogle.optionalAddress02,
      });
    } else {
      res.status(404);
      throw new Error("User not found");
    }
  }
});

exports.userRegisterApple = asyncHandler(async (req, res) => {
  const { data } = req.body;
  const code = generateCode();

  const decoded = jwt_decode(data.identityToken);

  const userExists = await User.findOne({ email: decoded.email });

  if (userExists && userExists.status === 2) {
    res.status(401);
    throw new Error("User suspended");
  }

  if (userExists && userExists.authenticationMethod !== 4) {
    res.status(401);
    throw new Error("User registered with another authentication method");
  }

  let username = "";

  if (data.fullName.familyName && data.fullName.givenName) {
    username = `${data.fullName.givenName}${data.fullName.familyName}`;
  } else {
    username = `NewUser${code}`;
  }

  const newUser = await User.create({
    username: username,
    email: decoded.email,
    password: decoded.sub,
    isActive: true,
    status: 1,
    authenticationMethod: 4,
    mainLocation: { type: "Point", coordinates: [0, 0] },
    optionalLocation01: { type: "Point", coordinates: [0, 0] },
    optionalLocation02: { type: "Point", coordinates: [0, 0] },
    notificationToken: data.notificationToken ? data.notificationToken : "",
    profile: {
      image: data.url ? data.url : "public/images/system/user.png",
      firstname: "New",
      lastname: "User",
      phone: "",
    },
    roles: "user",
  });

  const userApple = await User.findById(newUser._id);

  if (userApple) {
    await Global.create({
      user: newUser._id,
      ipaddress: "",
      location: { type: "Point", coordinates: [0, 0] },
      device: "",
      language: 0,
      isNewUser: 1,
    });

    res.json({
      _id: userApple._id,
      username: userApple.username,
      email: userApple.email,
      isActive: userApple.isActive,
      token: generateToken(userApple._id),
      notificationToken: userApple.notificationToken,
      roles: userApple.roles,
      type: userApple.type,
      status: userApple.status,
      profile: userApple.profile,
      mainLocation: userApple.mainLocation,
      optionalLocation01: userApple.optionalLocation01,
      optionalLocation02: userApple.optionalLocation02,
      mainAddress: userApple.mainAddress,
      optionalAddress01: userApple.optionalAddress01,
      optionalAddress02: userApple.optionalAddress02,
    });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

exports.userSignInApple = asyncHandler(async (req, res) => {
  const { data } = req.body;

  const decoded = jwt_decode(data.identityToken);

  const userExists = await User.findOne({ email: decoded.email });

  if (userExists && userExists.status === 2) {
    res.status(401);
    throw new Error("User suspended");
  }

  if (userExists && userExists.authenticationMethod !== 4) {
    res.status(401);
    throw new Error("User registered with another authentication method");
  }

  res.json({
    _id: userExists._id,
    username: userExists.username,
    email: userExists.email,
    isActive: userExists.isActive,
    token: generateToken(userExists._id),
    notificationToken: userExists.notificationToken,
    roles: userExists.roles,
    type: userExists.type,
    status: userExists.status,
    profile: userExists.profile,
    mainLocation: userExists.mainLocation,
    optionalLocation01: userExists.optionalLocation01,
    optionalLocation02: userExists.optionalLocation02,
    mainAddress: userExists.mainAddress,
    optionalAddress01: userExists.optionalAddress01,
    optionalAddress02: userExists.optionalAddress02,
  });
});

exports.getCurrentUser = asyncHandler(async (req, res) => {
  const user = await User.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.user._id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        localField: "_id",
        foreignField: "user",
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "categories",
        localField: "profile.mainInterests",
        foreignField: "_id",
        as: "mainInterests",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "friends.user",
        foreignField: "_id",
        as: "friends",
      },
    },
    {
      $project: {
        username: 1,
        email: 1,
        type: 1,
        facebookToken: 1,
        googleToken: 1,
        notificationToken: 1,
        isActive: 1,
        authenticationMethod: 1,
        mainLocation: 1,
        mainAddress: 1,
        optionalAddress01: 1,
        optionalAddress02: 1,
        optionalLocation01: 1,
        optionalLocation02: 1,
        createdAt: 1,
        roles: 1,
        status: 1,
        profile: {
          firstname: 1,
          lastname: 1,
          phone: 1,
          image: 1,
          mainInterests: "$mainInterests",
        },
        numberOfRecommendations: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
        numberOfFriends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
      },
    },
  ]);

  if (user.length > 0) {
    res.json(user[0]);
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

exports.resetPassword = asyncHandler(async (req, res) => {
  let { email } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    res.status(400);
    throw new Error("This email does not exist in our records");
  }

  crypto.randomBytes(32, async (err, buffer) => {
    if (err) {
      console.log(err);
      return;
    }

    const token = buffer.toString("hex");

    let resetToken = token;
    let expiresIn = Date.now() + 3600;
    let status = 0;

    const resetPassword = await Token.create({
      user: user._id,
      token: resetToken,
      expiresIn: expiresIn,
      status: status,
      reason: 1,
    });

    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: "no_reply@localsatlas.com",
      subject: "Locals Atlas Authentication",
      text: "Complete the authentication proccess",
      html: mails.emailToResetPassword(token),
    };

    sgMail
      .send(msg)
      .then(() => {
        console.log("Email sent");
      })
      .catch((error) => {
        console.error(error);
      });

    if (resetPassword) {
      res.json(resetPassword);
    } else {
      res.status(400);
      throw new Error("There was a problem sending the request");
    }
  });
});

exports.putNewPassword = asyncHandler(async (req, res) => {
  let { password } = req.body;

  const salt = await bcrypt.genSalt(10);

  const newPassword = await bcrypt.hash(password, salt);

  const reset = await Token.findOne({ token: req.params.token, status: 0 });

  if (!reset) {
    res.status(400);
    throw new Error("This token is invalid");
  }

  await User.findOneAndUpdate(
    { _id: reset.user },
    { $set: { password: newPassword } },
    { new: true, upsert: true }
  );

  await Token.findOneAndUpdate(
    { token: req.params.token },
    { $set: { status: 1 } },
    { new: true, upsert: true }
  );

  const user = await User.findById({ _id: reset.user }).select("-password");

  if (user) {
    res.json(user);
  } else {
    res.status(400);
    throw new Error("The password was not changed");
  }
});

exports.updateNotificationToken = asyncHandler(async (req, res) => {
  const { notificationToken } = req.body;

  const userExists = await User.findById(req.user.id);

  if (!userExists) {
    res.status(404);
    throw new Error("User not found");
  }

  if (
    notificationToken !== userExists.notificationToken ||
    userExists.notificationToken === "notToken" ||
    userExists.notificationToken === null ||
    userExists.notificationToken === ""
  ) {
    const user = await User.findByIdAndUpdate(
      { _id: req.user.id },
      { $set: { notificationToken: notificationToken } },
      { new: true, upsert: true }
    );

    if (user) {
      res.json({
        _id: user._id,
        username: user.name,
        email: user.email,
        isActive: user.isActive,
        token: generateToken(user._id),
        notificationToken: user.notificationToken,
        roles: user.roles,
        type: user.type,
        profile: user.profile,
        mainLocation: user.mainLocation,
        optionalLocation01: user.optionalLocation01,
        optionalLocation02: user.optionalLocation02,
        mainAddress: user.mainAddress,
        optionalAddress01: user.optionalAddress01,
        optionalAddress02: user.optionalAddress02,
      });
    }
  } else {
    res.json({
      _id: userExists._id,
      username: userExists.name,
      email: userExists.email,
      isActive: userExists.isActive,
      token: generateToken(userExists._id),
      notificationToken: userExists.notificationToken,
      roles: userExists.roles,
      type: userExists.type,
      profile: userExists.profile,
      mainLocation: userExists.mainLocation,
      optionalLocation01: userExists.optionalLocation01,
      optionalLocation02: userExists.optionalLocation02,
      mainAddress: userExists.mainAddress,
      optionalAddress01: userExists.optionalAddress01,
      optionalAddress02: userExists.optionalAddress02,
    });
  }
});

exports.compareContactsWithUsers = asyncHandler(async (req, res) => {
  const contacts = req.body.contacts;

  const phoneNumbersList = [];
  const users = [];

  try {
    for (let index = 0; index < contacts.length; index++) {
      const element = contacts[index];
      try {
        if (element.phoneNumbers) {
          for (let index = 0; index < element.phoneNumbers.length; index++) {
            const phones = element.phoneNumbers[index];

            phones.number = phones.number.split(" ").join("");
          }
          phoneNumbersList.push(element);
        }
      } catch (error) {
        console.log(error);
      }
    }

    const userList = [];

    for (let index = 0; index < phoneNumbersList.length; index++) {
      const element = phoneNumbersList[index];

      const duplicates = [];
      element.phoneNumbers = element.phoneNumbers.filter(function (e) {
        if (duplicates.indexOf(e.number) === -1) {
          duplicates.push(e.number);
          return true;
        } else {
          return false;
        }
      });
      userList.push(element);
    }

    for (let index = 0; index < userList.length; index++) {
      const element = userList[index];

      if (element.phoneNumbers[0].number.length > 0) {
        try {
          const user = await User.findOne({
            "profile.phone": new RegExp(
              element.phoneNumbers[0].number.slice(-6) + "$"
            ),
          }).select("-password");

          if (user) {
            element.exist = true;
            users.push(element);
          } else {
            element.exist = false;
            users.push(element);
          }
        } catch (error) {}
      }
    }
    res.json(users);
  } catch (error) {
    res.json(users);
  }
});

exports.getUserById = asyncHandler(async (req, res) => {
  const userExists = await User.findById(req.params.id).populate({
    path: "profile.mainInterests",
  });

  if (!userExists) {
    res.status(404);
    throw new Error("User not found");
  }

  const user = await User.aggregate([
    {
      $match: {
        $and: [
          {
            _id: {
              $eq: mongoose.Types.ObjectId(req.params.id),
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "recommendations",
        localField: "_id",
        foreignField: "user",
        as: "recommendation",
      },
    },
    {
      $lookup: {
        from: "categories",
        localField: "profile.mainInterests",
        foreignField: "_id",
        as: "mainInterests",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "friends.user",
        foreignField: "_id",
        as: "myFriends",
      },
    },
    {
      $project: {
        username: 1,
        email: 1,
        type: 1,
        profile: {
          firstname: 1,
          lastname: 1,
          phone: 1,
          image: 1,
          mainInterests: "$mainInterests",
        },
        myFriends: {
          _id: 1,
          username: 1,
          email: 1,
          profile: { firstname: 1, lastname: 1, phone: 1, image: 1 },
        },
        numberOfRecommendations: {
          $cond: {
            if: { $isArray: "$recommendation" },
            then: { $size: "$recommendation" },
            else: "NA",
          },
        },
        numberOfFriends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
        friend: {
          $filter: {
            input: "$friends",
            as: "friend",
            cond: {
              $eq: ["$$friend.user", mongoose.Types.ObjectId(req.user._id)],
            },
          },
        },
      },
    },
  ]);

  res.json(user[0]);
});

exports.deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    res.status(404);
    throw new Error("User does not exist");
  }

  await User.updateMany(
    {},
    { $pull: { friends: { user: mongoose.Types.ObjectId(req.user._id) } } }
  );

  await Business.deleteMany({
    $or: [
      { userCreator: { $eq: mongoose.Types.ObjectId(req.user._id) } },
      { ownerUser: { $eq: mongoose.Types.ObjectId(req.user._id) } },
    ],
  });

  await Comment.deleteMany({ user: mongoose.Types.ObjectId(req.user._id) });

  await Recommendation.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await RecommendationAudit.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await RecommendationMetric.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await Answer.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await Request.deleteMany({ user: mongoose.Types.ObjectId(req.user._id) });

  await Notification.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await Report.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await Behavior.deleteMany({
    $or: [
      { reportedUser: { $ne: mongoose.Types.ObjectId(req.user._id) } },
      { reportingUser: { $ne: mongoose.Types.ObjectId(req.user._id) } },
    ],
  });

  await Claim.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await Token.deleteMany({ user: mongoose.Types.ObjectId(req.user._id) });

  await ErrorAudit.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await UserTracking.deleteMany({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await Global.deleteOne({
    user: mongoose.Types.ObjectId(req.user._id),
  });

  await User.findByIdAndDelete(req.user._id);

  res.json({ message: "User deleted" });
});
//#endregion

// ######################################################################
// Administrator or operator APIs
// ######################################################################
//#region
exports.createUser = asyncHandler(async (req, res) => {
  const { username, email, password, firstname, lastname, roles, phone } =
    req.body;

  const userExists = await User.findOne({ email });

  if (userExists) {
    res.status(400);
    throw new Error("User already exists");
  }

  const user = await User.create({
    username,
    email,
    password,
    roles,
    notificationToken: "",
    profile: {
      image: "public/images/system/user.png",
      firstname: firstname,
      lastname: lastname,
      phone: phone,
    },
    status: 1,
    isActive: true,
    mainLocation: { type: "Point", coordinates: [0, 0] },
    optionalLocation01: { type: "Point", coordinates: [0, 0] },
    optionalLocation02: { type: "Point", coordinates: [0, 0] },
  });

  await AdminAudit.create({
    user: req.user._id,
    object: "user",
    objectId: `${user._id}`,
    observation: `User created as ${roles}`,
    action: 1,
  });

  if (user) {
    res.json(user);
  } else {
    res.json(null);
  }
});

exports.banUser = asyncHandler(async (req, res) => {
  const { observation } = req.body;

  const userExists = await User.findById(req.params.id);

  if (!userExists) {
    res.status(404);
    throw new Error("User does not exists");
  }

  await User.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { status: 2, isActive: false } },
    { new: true, upsert: true }
  );

  await AdminAudit.create({
    user: req.user._id,
    object: "user",
    objectId: `${userExists._id}`,
    observation: observation,
    field: "status",
    oldValues: [`${userExists.status}`],
    newValues: ["2"],
    action: "1",
  });

  await AdminAudit.create({
    user: req.user._id,
    object: "user",
    objectId: `${userExists._id}`,
    observation: observation,
    field: "isActive",
    oldValues: [`${userExists.isActive}`],
    newValues: ["false"],
    action: "1",
  });

  res.json({ message: "User was blocked" });
});

exports.activeUser = asyncHandler(async (req, res) => {
  const { observation } = req.body;

  const userExists = await User.findById(req.params.id);

  if (!userExists) {
    res.status(404);
    throw new Error("User does not exists");
  }

  await User.findByIdAndUpdate(
    { _id: req.params.id },
    { $set: { status: 1, isActive: true } },
    { new: true, upsert: true }
  );

  await AdminAudit.create({
    user: req.user._id,
    object: "user",
    objectId: `${userExists._id}`,
    observation: observation,
    field: "status",
    oldValues: [`${userExists.status}`],
    newValues: ["1"],
    action: "1",
  });

  await AdminAudit.create({
    user: req.user._id,
    object: "user",
    objectId: `${userExists._id}`,
    observation: observation,
    field: "isActive",
    oldValues: [`${userExists.isActive}`],
    newValues: ["true"],
    action: "1",
  });

  res.json({ message: "User was activated" });
});

exports.updateUser = asyncHandler(async (req, res) => {
  const { observation, firstname, lastname, roles, status } = req.body;

  const userExists = await User.findById(req.params.id);

  if (!userExists) {
    res.status(404);
    throw new Error("User does not exists");
  }

  await User.findByIdAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        roles: roles,
        "profile.firstname": firstname,
        "profile.lastname": lastname,
        status: status,
      },
    },
    { new: true, upsert: true }
  );

  if (userExists.roles !== roles) {
    await AdminAudit.create({
      user: req.user._id,
      object: "user",
      objectId: `${userExists._id}`,
      observation: observation,
      field: "roles",
      oldValues: [`${userExists.roles}`],
      newValues: [roles],
      action: "1",
    });
  } else if (userExists.profile.firstname !== firstname) {
    await AdminAudit.create({
      user: req.user._id,
      object: "user",
      objectId: `${userExists._id}`,
      observation: observation,
      field: "firstname",
      oldValues: [`${userExists.firstname}`],
      newValues: [firstname],
      action: "1",
    });
  } else if (userExists.profile.lastname !== lastname) {
    await AdminAudit.create({
      user: req.user._id,
      object: "user",
      objectId: `${userExists._id}`,
      observation: observation,
      field: "lastname",
      oldValues: [`${userExists.lastname}`],
      newValues: [lastname],
      action: "1",
    });
  } else if (userExists.status !== status) {
    await AdminAudit.create({
      user: req.user._id,
      object: "user",
      objectId: `${userExists._id}`,
      observation: observation,
      field: "status",
      oldValues: [`${userExists.status}`],
      newValues: [status],
      action: "1",
    });
  }

  res.json({ message: "User was updated" });
});

exports.deleteUserById = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user) {
    res.status(404);
    throw new Error("User does not exist");
  }

  await User.updateMany(
    {},
    { $pull: { friends: { user: mongoose.Types.ObjectId(req.params.id) } } }
  );

  await Business.deleteMany({
    $or: [
      { userCreator: { $eq: mongoose.Types.ObjectId(req.params.id) } },
      { ownerUser: { $eq: mongoose.Types.ObjectId(req.params.id) } },
    ],
  });

  await Comment.deleteMany({ user: mongoose.Types.ObjectId(req.params.id) });

  await Recommendation.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await RecommendationAudit.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await RecommendationMetric.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await Answer.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await Request.deleteMany({ user: mongoose.Types.ObjectId(req.params.id) });

  await Notification.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await Report.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await Behavior.deleteMany({
    $or: [
      { reportedUser: { $ne: mongoose.Types.ObjectId(req.params.id) } },
      { reportingUser: { $ne: mongoose.Types.ObjectId(req.params.id) } },
    ],
  });

  await Claim.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await Token.deleteMany({ user: mongoose.Types.ObjectId(req.params.id) });

  await ErrorAudit.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await UserTracking.deleteMany({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await Global.deleteOne({
    user: mongoose.Types.ObjectId(req.params.id),
  });

  await User.findByIdAndDelete(req.params.id);

  res.json({ message: "User deleted" });
});

exports.getUsers = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 5;
  const keyword = req.query.keyword
    ? {
        $or: [
          { "profile.firstname": { $regex: req.query.keyword, $options: "i" } },
          { "profile.lastname": { $regex: req.query.keyword, $options: "i" } },
        ],
      }
    : {};

  const totalDocuments = await User.countDocuments({ ...keyword });

  const users = await User.find({ ...keyword })
    .select("-password")
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({ createdAt: -1 });

  res.json({ users, pageNumber, pageSize, totalDocuments });
});

exports.getUserProfileById = asyncHandler(async (req, res) => {
  const userExists = await User.findById(req.params.id).select("-password");

  if (!userExists) {
    res.status(404);
    throw new Error("User not found");
  } else {
    res.json(userExists);
  }
});

exports.getUsersCoordinates = asyncHandler(async (req, res) => {
  const usersExist = await User.find().select("-password");

  const users = [];

  usersExist.forEach((element) => {
    users.push({
      _id: element._id,
      mainLocation: element.mainLocation,
    });
  });

  if (users.length > 0) {
    res.json(users);
  } else {
    res.json([]);
  }
});

exports.getTotalRegisteredUsers = asyncHandler(async (req, res) => {
  const user = await User.find();

  if (user.length > 0) {
    res.json({ registeredUsers: user.length });
  } else {
    res.json({ registeredUsers: 0 });
  }
});

exports.getTotalInactiveUsers = asyncHandler(async (req, res) => {
  const user = await User.find({ isActive: false });

  if (user.length > 0) {
    res.json({ inactiveUsers: user.length });
  } else {
    res.json({ inactiveUsers: 0 });
  }
});

exports.getTotalActiveUsers = asyncHandler(async (req, res) => {
  const user = await User.find({ isActive: true });

  if (user.length > 0) {
    res.json({ activeUsers: user.length });
  } else {
    res.json({ activeUsers: 0 });
  }
});

exports.getTotalReportedUsers = asyncHandler(async (req, res) => {
  const user = await User.find({ status: 3 });

  if (user.length > 0) {
    res.json({ reportedUsers: user.length });
  } else {
    res.json({ reportedUsers: 0 });
  }
});

exports.getTotalBusinessFromUser = asyncHandler(async (req, res) => {
  const pageNumber = Number(req.query.pagenumber) || 1;
  const pageSize = Number(req.query.pagesize) || 5;

  const totalDocuments = await Business.countDocuments();

  const businesses = await Business.find({
    $or: [{ userCreator: req.params.id }, { ownerUser: req.params.id }],
  })
    .limit(pageSize)
    .skip(pageSize * (pageNumber - 1))
    .sort({ createdAt: -1 });

  res.json({ businesses, pageNumber, pageSize, totalDocuments });
});

exports.getTotalFriendsFromUser = asyncHandler(async (req, res) => {
  const users = await User.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(req.params.id),
      },
    },
    {
      $project: {
        friends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
      },
    },
  ]);

  res.json(users[0]);
});

exports.getTotalRecommendationFromUser = asyncHandler(async (req, res) => {
  const recommendation = await Recommendation.aggregate([
    {
      $match: {
        user: mongoose.Types.ObjectId(req.params.id),
      },
    },
    {
      $group: { _id: null, recommendations: { $sum: 1 } },
    },
    { $project: { _id: 0 } },
  ]);

  if (recommendation.length > 0) {
    res.json(recommendation[0]);
  } else {
    res.json({ recommendations: 0 });
  }
});

exports.getTop5RecommenderUsers = asyncHandler(async (req, res) => {
  const user = await Recommendation.aggregate([
    {
      $group: {
        _id: "$user",
        count: { $sum: 1 },
      },
    },
    {
      $project: {
        user: "$_id",
        _id: 0,
        recommendations: "$count",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $sort: { recommendations: 1 },
    },
  ]);

  res.json(user);
});

exports.getTop5RecommenderUsers = asyncHandler(async (req, res) => {
  const user = await Recommendation.aggregate([
    {
      $group: {
        _id: "$user",
        count: { $sum: 1 },
      },
    },
    {
      $project: {
        user: "$_id",
        _id: 0,
        count: "$count",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $project: {
        user: { profile: { firstname: 1, lastname: 1 } },
        count: 1,
      },
    },
    {
      $sort: { count: -1 },
    },
  ]);

  res.json(user);
});

exports.getTopUserWithMoreFriends = asyncHandler(async (req, res) => {
  const user = await User.aggregate([
    {
      $project: {
        username: 1,
        profile: { firstname: 1, lastname: 1 },
        friends: {
          $cond: {
            if: { $isArray: "$friends" },
            then: { $size: "$friends" },
            else: "NA",
          },
        },
      },
    },
    {
      $sort: { friends: -1 },
    },
  ]);

  res.json(user);
});

//#endregion
