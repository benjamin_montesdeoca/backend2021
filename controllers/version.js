const asyncHandler = require("express-async-handler");

const Version = require("../models/version");

exports.createVersion = asyncHandler(async (req, res) => {
  const version = await Version.create({
    version: req.body.version,
    description: req.body.description,
    appLink: req.body.appLink,
    isActive: true,
  });

  res.json(version);
});

exports.getVersion = asyncHandler(async (req, res) => {
  const version = await Version.find().sort({ createdAt: -1 }).limit(1);

  res.json(version[0]);
});
