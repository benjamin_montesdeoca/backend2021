require("dotenv").config();
const asyncHandler = require("express-async-handler");
const sgmail = require("@sendgrid/mail");

sgmail.setApiKey(process.env.SENDGRID_API_KEY_MARKETING);

exports.sendMailByMarketing = asyncHandler(async (req, res) => {
  const { email, subject, text } = req.body;

  await sgmail.send({
    from: "marketing@fei.edu",
    to: email,
    subject: `${subject}`,
    html: text,
  });

  res.json({ message: "Email was succesfully sent" });
});

exports.sendMailsByMarketing = asyncHandler(async (req, res) => {
  const { emails, subject, text } = req.body;

  for (let index = 0; index < emails.length; index++) {
    const element = emails[index];

    await sgmail.send({
      from: "marketing@fei.edu",
      to: element,
      subject: `${subject}`,
      html: text,
    });
  }

  res.json({ message: "Email was succesfully sent" });
});

exports.sendMMS = asyncHandler(async (req, res) => {
  const { mediaUrl, phones, message } = req.body;

  const accountSid = process.env.TWILIO_ACCOUNT_SID_FEI;
  const authToken = process.env.TWILIO_AUTH_TOKEN_FEI;
  const client = require("twilio")(accountSid, authToken);

  for (let index = 0; index < phones.length; index++) {
    const element = phones[index];

    client.messages
      .create({
        body: `${message}`,
        from: "+18506798946",
        mediaUrl: [`${mediaUrl}`],
        to: `${element}`,
      })
      .then((message) => {
        console.log(message.sid);
      })
      .catch((error) => {
        res.json({ error: error });
      });
  }

  res.json({ message: `Message sent to all the numbers` });
});
