exports.notificationTitle = (number) => {
  switch (number) {
    case 1:
      return [
        "Your business has been approved",
        "Su negocio ha sido aprobado",
        "您的业​​务已获得批准",
        "Votre entreprise a été approuvée",
        "تمت الموافقة على عملك",
        "Ваш бизнес одобрен",
        "Sua empresa foi aprovada",
        "Ihr Unternehmen wurde genehmigt",
        "La tua attività è stata approvata",
        "Your business has been approved",
        "귀하의 비즈니스가 승인되었습니다",
      ];
    case 2:
      return [
        "Business temporarily banned",
        "Negocio temporalmente baneado",
        "暂时禁止营业",
        "Entreprise temporairement interdite",
        "الأعمال محظورة مؤقتا",
        "Бизнес временно запрещен",
        "Negócios temporariamente banidos",
        "Geschäft vorübergehend verboten",
        "Attività temporaneamente vietate",
        "一時的に禁止された事業",
        "사업 잠정 금지",
      ];
    case 3:
      return [
        "Your account has been restored",
        "Tu cuenta ha sido restaurada",
        "您的帐户已恢复",
        "Votre compte a été restauré",
        "تمت استعادة حسابك",
        "Ваш аккаунт восстановлен",
        "Sua conta foi restaurada",
        "Ihr Konto wurde wiederhergestellt",
        "Il tuo account è stato ripristinato",
        "アカウントが復元されました",
        "귀하의 계정이 복원되었습니다",
      ];
    case 4:
      return [
        "You have received a friend request",
        "Has recibido una solicitud de amistad",
        "您收到了一个好友请求",
        "Vous avez reçu une demande d'ami",
        "لقد تلقيت طلب صداقة",
        "Вы получили запрос на добавление в друзья",
        "Você recebeu um pedido de amizade",
        "Sie haben eine Freundschaftsanfrage erhalten",
        "Hai ricevuto una richiesta di amicizia",
        "友達リクエストを受け取りました",
        "친구 요청을 받았습니다",
      ];
    case 5:
      return [
        "Your friend request has been accepted",
        "Tu solicitud de amistad ha sido aceptada",
        "您的好友请求已被接受",
        "Votre demande d'ami a été acceptée",
        "تم قبول طلب الصداقة الخاص بك",
        "Ваш запрос на добавление в друзья принят",
        "Sua solicitação de amizade foi aceita",
        "Ihre Freundschaftsanfrage wurde angenommen",
        "La tua richiesta di amicizia è stata accettata",
        "友達リクエストが承認されました",
        "친구 요청이 수락되었습니다",
      ];
    case 6:
      return [
        "A user has sent you a business request",
        "Un usuario te ha enviado una solicitud de negocio",
        "用户向您发送了业务请求",
        "Un utilisateur vous a envoyé une demande commerciale",
        "أرسل لك مستخدم طلب عمل",
        "Пользователь отправил вам бизнес-запрос",
        "Um usuário enviou a você uma solicitação comercial",
        "Ein Benutzer hat Ihnen eine Geschäftsanfrage gesendet",
        "Un utente ti ha inviato una richiesta commerciale",
        "ユーザーからビジネスリクエストが送信されました",
        "사용자가 비즈니스 요청을 보냈습니다",
      ];
    case 7:
      return [
        "You have received an answer to your request",
        "Has recibido respuesta a tu solicitud",
        "您已收到對您的請求的答复",
        "Vous avez reçu une réponse à votre demande",
        "لقد تلقيت إجابة لطلبك",
        "Вы получили ответ на свой запрос",
        "Você recebeu uma resposta à sua solicitação",
        "Sie haben eine Antwort auf Ihre Anfrage erhalten",
        "Hai ricevuto risposta alla tua richiesta",
        "あなたはあなたの要求に対する答えを受け取りました",
        "귀하의 요청에 대한 답변을 받았습니다",
      ];
    case 8:
      return [
        "You have reported a business",
        "Has denunciado un negocio",
        "您举报了一家企业",
        "Vous avez signalé une entreprise",
        "لقد أبلغت عن عمل",
        "Вы сообщили о бизнесе",
        "Você denunciou uma empresa",
        "Sie haben ein Geschäft gemeldet",
        "Hai segnalato un'attività",
        "あなたはビジネスを報告しました",
        "비즈니스를 신고했습니다",
      ];
    case 9:
      return [
        "Your business has been recommended",
        "Su negocio ha sido recomendado",
        "您的商家已被推荐",
        "Votre entreprise a été recommandée",
        "وقد أوصى عملك",
        "Ваш бизнес был рекомендован",
        "Sua empresa foi recomendada",
        "Ihr Unternehmen wurde empfohlen",
        "La tua attività è stata consigliata",
        "あなたのビジネスが推奨されています",
        "귀하의 비즈니스가 추천되었습니다",
      ];
    case 10:
      return [
        "You have been reported",
        "Has sido reportado",
        "你被举报了",
        "Vous avez été signalé",
        "تم الإبلاغ عنك",
        "О вас сообщили",
        "Você foi denunciado",
        "Sie wurden gemeldet",
        "Sei stato segnalato",
        "あなたは報告されました",
        "당신은보고되었습니다",
      ];
    case 11:
      return [
        "You have reported a user",
        "Has reportado a un usuario",
        "你举报了一个用户",
        "Vous avez signalé un utilisateur",
        "لقد أبلغت عن مستخدم",
        "Вы сообщили о пользователе",
        "Você denunciou um usuário",
        "Sie haben einen Benutzer gemeldet",
        "Hai segnalato un utente",
        "ユーザーを報告しました",
        "사용자를 신고했습니다.",
      ];
    case 12:
      return [
        "Your business has no longer been recommended by a user",
        "Su negocio ya no ha sido recomendado por un usuario",
        "用户不再推荐您的商家",
        "Votre entreprise n'a plus été recommandée par un utilisateur",
        "لم يعد عملك موصى به من قبل المستخدم",
        "Ваш бизнес больше не был рекомендован пользователем",
        "Sua empresa não foi mais recomendada por um usuário",
        "Ihr Unternehmen wurde nicht mehr von einem Benutzer empfohlen",
        "La tua attività non è più stata consigliata da un utente",
        "あなたのビジネスはもはやユーザーから推薦されていません",
        "사용자가 더 이상 귀하의 비즈니스를 추천하지 않았습니다",
      ];
    case 13:
      return [
        "Someone send you a recommendation",
        "Alguien te envió una recomendación",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
        "Someone send you a recommendation",
      ];
    default:
      return [
        "No notification title",
        "Sin titulo de notificacion",
        "没有通知标题",
        "Aucun titre de notification",
        "لا يوجد عنوان إعلام",
        "Нет заголовка уведомления",
        "Sem título de notificação",
        "Kein Benachrichtigungstitel",
        "Nessun titolo della notifica",
        "通知タイトルなし",
        "알림 제목 없음",
      ];
  }
};

exports.notificationTitleForAdmin = (number) => {
  switch (number) {
    case 1:
      return ["", "", "", "", "", "", "", "", "", ""];
    case 2:
      return [
        "Business reported",
        "Negocio reportado",
        "业务报告",
        "Affaires signalées",
        "ذكرت الأعمال",
        "Бизнес сообщил",
        "Negócios relatados",
        "Geschäft gemeldet",
        "Affari segnalati",
        "報告されたビジネス",
        "보고된 사업",
      ];
    case 3:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 4:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 5:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 6:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 7:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 8:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 9:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 10:
      return [
        "User reported",
        "Usuario reportado",
        "用户举报",
        "Utilisateur signalé",
        "أبلغ المستخدم",
        "Пользователь сообщил",
        "Benutzer gemeldet",
        "Utente segnalato",
        "ユーザーからの報告",
        "사용자 신고",
      ];
    case 11:
      return ["", "", "", "", "", "", "", "", "", ""];
    case 12:
      return ["", "", "", "", "", "", "", "", "", ""];
    default:
      return ["", "", "", "", "", "", "", "", "", ""];
  }
};

exports.notificationMessage = (number, name) => {
  switch (number) {
    case 1:
      return [
        `Your business "${name}" has been approved`,
        `Tu empresa "${name}" ha sido aprobada`,
        `贵公司“${name}”已获批`,
        `Votre entreprise "${name}" a été agrééex`,
        `تمت الموافقة على شركة "${name}" الخاصة بشركتك`,
        `Ваша компания "${name}" одобрена`,
        `Sua empresa "${name}" foi aprovada`,
        `Ihr Unternehmen "${name}" wurde genehmigt`,
        `La tua azienda "${name}" è stata approvata`,
        `あなたの会社の「${name}」が承認されました`,
        `귀하의 회사 "${name}"가 승인되었습니다`,
      ];
    case 2:
      return [
        `The business "${name}" has been reported`,
        `La empresa "${name}" ha sido reportada`,
        `公司“${name}”已被举报`,
        `La société "${name}" a été signalée`,
        `تم الإبلاغ عن شركة "${name}"`,
        `Компания «${name}» сообщила`,
        `A empresa "${name}" foi relatada`,
        `Die Firma "${name}" wurde gemeldet`,
        `La società "${name}" è stata segnalata`,
        `会社「${name}」が報告されました`,
        `회사 "${name}"가보고되었습니다.`,
      ];
    case 3:
      return ["", "", "", "", "", "", "", "", "", "", ""];
    case 4:
      return [
        `"${name}" has sent you a friend request`,
        `"${name}" te ha enviado una solicitud de amistad`,
        `“${name}”给你发了一个好友请求`,
        `"${name}" vous a envoyé une demande d'ami`,
        `"${name}" أرسل إليك طلب صداقة`,
        `"${name}" отправил вам запрос на добавление в друзья`,
        `"${name}" enviou-lhe um pedido de amizade`,
        `"${name}" hat dir eine Freundschaftsanfrage geschickt`,
        `"${name}" ti ha inviato una richiesta di amicizia`,
        `「${name}」から友達リクエストが届きました`,
        `"${name}"이(가) 친구 요청을 보냈습니다.`,
      ];
    case 5:
      return [
        `You and "${name}" are now friends`,
        `Tú y "${name}" ahora son amigos`,
        `你和“${name}”现在是朋友了`,
        `Toi et "${name}" êtes maintenant amis`,
        `أنت و "${name}" صديقان الآن`,
        `Ты и "${name}" теперь друзья`,
        `Você e "${name}" agora são amigos`,
        `Sie und "${name}" sind jetzt Freunde`,
        `Tu e "${name}" ora siete amici`,
        `あなたと「${name}」は友達になりました`,
        `당신과 "${name}"은 이제 친구입니다`,
      ];
    case 6:
      return [
        "A user has sent you a business request",
        "Un usuario te ha enviado una solicitud de negocio",
        "用户向您发送了业务请求",
        "Un utilisateur vous a envoyé une demande commerciale",
        "أرسل لك مستخدم طلب عمل",
        "Пользователь отправил вам бизнес-запрос",
        "Um usuário enviou a você uma solicitação comercial",
        "Ein Benutzer hat Ihnen eine Geschäftsanfrage gesendet",
        "Un utente ti ha inviato una richiesta commerciale",
        "ユーザーからビジネスリクエストが送信されました",
        "사용자가 비즈니스 요청을 보냈습니다",
      ];
    case 7:
      return [
        "You have received an answer to your request",
        "Has recibido respuesta a tu solicitud",
        "您已收到對您的請求的答复",
        "Vous avez reçu une réponse à votre demande",
        "لقد تلقيت إجابة لطلبك",
        "Вы получили ответ на свой запрос",
        "Você recebeu uma resposta à sua solicitação",
        "Sie haben eine Antwort auf Ihre Anfrage erhalten",
        "Hai ricevuto risposta alla tua richiesta",
        "あなたはあなたの要求に対する答えを受け取りました",
        "귀하의 요청에 대한 답변을 받았습니다",
      ];
    case 8:
      return [
        `"${name}" has been reported by you`,
        `"${name}" ha sido reportado por ti`,
        `“${name}”已被您举报`,
        `"${name}" a été signalé par vous`,
        `تم الإبلاغ عن "${name}" من قبلك`,
        `"Вы сообщили о "${name}"`,
        `"${name}" foi reportado por você`,
        `"${name}" wurde von Ihnen gemeldet`,
        `"${name}" è stato segnalato da te`,
        `"「${name}」はあなたから報告されました`,
        `"${name}"가 신고되었습니다.`,
      ];
    case 9:
      return [
        `Your business "${name}" has been recommended`,
        `Tu negocio "${name} ha sido recomendado"`,
        `您的商家“${name}”已被推荐`,
        `Votre entreprise "${name}" a été recommandée`,
        `وقد أوصى عملك "${name}"`,
        `Ваш бизнес "${name}" был рекомендован`,
        `Sua empresa "${name}" foi recomendada`,
        `Ihr Geschäft "${name}" wurde empfohlen`,
        `La tua attività "${name}" è stata consigliata`,
        `あなたのビジネス「${name}」が推奨されています`,
        `귀하의 비즈니스 "${name}"가 추천되었습니다`,
      ];
    case 10:
      return [
        `The user "${name}" has been reported for misbehaving`,
        `El usuario "${name}" ha sido reportado por mal comportamiento`,
        `用户“${name}”已被举报行为不端`,
        `L'utilisateur "${name}" a été signalé pour mauvaise conduite`,
        `تم الإبلاغ عن المستخدم "${name}" لسوء التصرف`,
        `Пользователь "${name}" был отправлен в суд за неподобающее поведение.`,
        `O usuário "${name}" foi denunciado por mau comportamento`,
        `Der Benutzer „${name}“ wurde wegen Fehlverhaltens gemeldet`,
        `L'utente "${name}" è stato segnalato per comportamento anomalo`,
        `ユーザー「${name}」が不正行為で報告されました`,
        `사용자 "${name}"이 잘못된 행동으로 신고되었습니다.`,
      ];
    case 11:
      return [
        "You have reported a user for misbehaving",
        "Has reportado a un usuario por mal comportamiento",
        "您已举报用户行为不端",
        "Vous avez signalé un utilisateur pour mauvaise conduite",
        "لقد أبلغت عن مستخدم لسوء التصرف",
        "Вы сообщили о ненадлежащем поведении пользователя",
        "Você denunciou um usuário por mau comportamento",
        "Sie haben einen Benutzer wegen Fehlverhaltens gemeldet",
        "Hai segnalato un utente per comportamento anomalo",
        "不正行為についてユーザーを報告しました",
        "잘못된 행동으로 사용자를 신고했습니다.",
      ];
    case 12:
      return [
        `Your business "${name}" has no longer been recommended by a user`,
        `Tu negocio "${name}" ya no ha sido recomendado por un usuario`,
        `用户不再推荐您的商家“${name}”`,
        `Votre entreprise "${name}" n'a plus été recommandée par un utilisateur`,
        `لم يعد أحد المستخدمين يوصى بشركة "${name}" الخاصة بشركتك`,
        `Ваш бизнес "${name}" больше не рекомендовался пользователем`,
        `Seu negócio "${name}" não foi mais recomendado por um usuário`,
        `Ihr Unternehmen "${name}" wurde nicht mehr von einem Benutzer empfohlen`,
        `La tua attività "${name}" non è più stata consigliata da un utente`,
        `あなたのビジネス「${name}」はもはやユーザーによって推薦されていません`,
        `귀하의 비즈니스 "${name}"는 더 이상 사용자가 추천하지 않습니다`,
      ];
    case 13:
      return [
        `"${name}" send you a recommendation`,
        `${name} te envió una recomendación`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
        `"${name}" send you a recommendation`,
      ];
    default:
      return ["", "", "", "", "", "", "", "", "", "", ""];
  }
};

exports.notificationPushMessage = (
  languageNumber,
  typeNotificationNumber,
  name
) => {
  switch (typeNotificationNumber) {
    case 1:
      switch (languageNumber) {
        case 0:
          return "Your business has been temporarily blocked due to our community policies";
        case 1:
          return "Tu negocio ha sido temporalmente bloqueado debido a nuestras politicas de comunidad";
        case 2:
          return "由于我们的社区政策，您的业务已被暂时禁止";
        case 3:
          return "Votre entreprise a été temporairement bloquée en raison de nos politiques communautaires";
        case 4:
          return "تم حظر عملك مؤقتًا بسبب سياسات المجتمع الخاصة بنا";
        case 5:
          return "Ваш бизнес временно заблокирован из-за правил нашего сообщества";
        case 6:
          return "Sua empresa foi temporariamente bloqueada devido às nossas políticas da comunidade";
        case 7:
          return "Ihr Unternehmen wurde aufgrund unserer Community-Richtlinien vorübergehend gesperrt";
        case 8:
          return "La tua attività è stata temporaneamente bloccata a causa delle nostre norme della community";
        case 9:
          return "コミュニティポリシーにより、お客様のビジネスは一時的にブロックされています";
        case 10:
          return "커뮤니티 정책으로 인해 귀하의 비즈니스가 일시적으로 차단되었습니다";
        default:
          return "Your business has been temporarily blocked due to our community policies";
      }
    case 2:
      switch (languageNumber) {
        case 0:
          return "Your business has been approved";
        case 1:
          return "Tu negocio ha sido aprobado";
        case 2:
          return "您的业务已获得批准";
        case 3:
          return "Votre entreprise a été approuvée";
        case 4:
          return "تمت الموافقة على عملك";
        case 5:
          return "Ваш бизнес одобрен";
        case 6:
          return "Sua empresa foi aprovada";
        case 7:
          return "Ihr Unternehmen wurde genehmigt";
        case 8:
          return "La tua attività è stata approvata";
        case 9:
          return "あなたのビジネスは承認されました";
        case 10:
          return "귀하의 비즈니스가 승인되었습니다";
        default:
          return "Your business has been approved";
      }
    case 3:
      switch (languageNumber) {
        case 0:
          return `Your business named "${name}" has been approved`;
        case 1:
          return `Tu empresa "${name}" ha sido aprobada`;
        case 2:
          return `贵公司“${name}”已获批`;
        case 3:
          return `Votre entreprise "${name}" a été agrééex`;
        case 4:
          return `تمت الموافقة على شركة "${name}" الخاصة بشركتك`;
        case 5:
          return `Ваша компания "${name}" одобрена`;
        case 6:
          return `Sua empresa "${name}" foi aprovada`;
        case 7:
          return `Ihr Unternehmen "${name}" wurde genehmigt`;
        case 8:
          return `La tua azienda "${name}" è stata approvata`;
        case 9:
          return `あなたの会社の「${name}」が承認されました`;
        case 10:
          return `귀하의 회사 "${name}"가 승인되었습니다`;
        default:
          return `Your business named "${name}" has been approved`;
      }
    case 4:
      switch (languageNumber) {
        case 0:
          return `Your business name "${name}" has been deactivated`;
        case 1:
          return `Tu negocio llamado "${name}" ha sido desactivado`;
        case 2:
          return `您名为“${name}”的商家已被停用`;
        case 3:
          return `Le nom de votre entreprise "${name}" a été désactivé`;
        case 4:
          return `تم إلغاء تنشيط اسم عملك التجاري "${name}"`;
        case 5:
          return `Название вашей компании "${name}" деактивировано`;
        case 6:
          return `O nome da sua empresa "${name}" foi desativado`;
        case 7:
          return `Ihr Firmenname "${name}" wurde deaktiviert`;
        case 8:
          return `Il nome della tua attività "${name}" è stato disattivato`;
        case 9:
          return `あなたの会社名「${name}」は無効になっています`;
        case 10:
          return `귀하의 비즈니스 이름 "${name}"가 비활성화되었습니다.`;
        default:
          return `Your business named "${name}" has been deactivated`;
      }
    case 5:
      switch (languageNumber) {
        case 0:
          return `"${name}" has sent you a friend request`;
        case 1:
          return `"${name}" te ha enviado una solicitud de amistad`;
        case 2:
          return `“${name}”给你发了一个好友请求`;
        case 3:
          return `"${name}" vous a envoyé une demande d'ami`;
        case 4:
          return `"${name}" أرسل إليك طلب صداقة`;
        case 5:
          return `"${name}" отправил вам запрос на добавление в друзья`;
        case 6:
          return `"${name}" enviou-lhe um pedido de amizade`;
        case 7:
          return `"${name}" hat dir eine Freundschaftsanfrage geschickt`;
        case 8:
          return `"${name}" ti ha inviato una richiesta di amicizia`;
        case 9:
          return `「${name}」から友達リクエストが届きました`;
        case 10:
          return `"${name}"이(가) 친구 요청을 보냈습니다`;
        default:
          return `"${name}" has sent you a friend request`;
      }
    case 6:
      switch (languageNumber) {
        case 0:
          return `Your business "${name}" has no longer been recommended by a user`;
        case 1:
          return `Tu negocio "${name}" ya no ha sido recomendado por un usuario`;
        case 2:
          return `用户不再推荐您的商家“${name}”`;
        case 3:
          return `Votre entreprise "${name}" n'a plus été recommandée par un utilisateur`;
        case 4:
          return `لم يعد أحد المستخدمين يوصى بشركة "${name}" الخاصة بشركتك`;
        case 5:
          return `Ваш бизнес "${name}" больше не рекомендовался пользователем`;
        case 6:
          return `Seu negócio "${name}" não foi mais recomendado por um usuário`;
        case 7:
          return `Ihr Unternehmen "${name}" wurde nicht mehr von einem Benutzer empfohlen`;
        case 8:
          return `La tua attività "${name}" non è più stata consigliata da un utente`;
        case 9:
          return `あなたのビジネス「${name}」はもはやユーザーによって推薦されていません`;
        case 10:
          return `귀하의 비즈니스 "${name}"는 더 이상 사용자가 추천하지 않습니다`;
        default:
          return `Your business "${name}" has no longer been recommended by a user`;
      }
    case 7:
      switch (languageNumber) {
        case 0:
          return `${name} has been recommended near to you`;
        case 1:
          return `${name} ha sido recomendado cerca de ti`;
        case 2:
          return `“${name}”已在您附近推荐`;
        case 3:
          return `"${name}" a été recommandé près de chez vous`;
        case 4:
          `أوصى "${name}" بالقرب منك`;
        case 5:
          return `«${name}» были рекомендованы рядом с вами`;
        case 6:
          return `"${name}" foi recomendado perto de você`;
        case 7:
          return `"${name}" wurde in Ihrer Nähe empfohlen`;
        case 8:
          return `"${name}" è stato consigliato vicino a te`;
        case 9:
          return `お近くで「${name}」をお勧めします`;
        case 10:
          return `"${name}"가 근처에 추천되었습니다`;
        default:
          return `${name} has been recommended near to you`;
      }
    case 8:
      switch (languageNumber) {
        case 0:
          return `The business "${name}" has been reported`;
        case 1:
          return `La empresa "${name}" ha sido reportada`;
        case 2:
          return `公司“${name}”已被举报`;
        case 3:
          return `La société "${name}" a été signalée`;
        case 4:
          return `تم الإبلاغ عن شركة "${name}"`;
        case 5:
          return `Компания «${name}» сообщила`;
        case 6:
          return `A empresa "${name}" foi relatada`;
        case 7:
          return `Die Firma "${name}" wurde gemeldet`;
        case 8:
          return `La società "${name}" è stata segnalata`;
        case 9:
          return `会社「${name}」が報告されました`;
        case 10:
          return `회사 "${name}"가보고되었습니다.`;
        default:
          return `Your business "${name}" has been reported`;
      }
    case 9:
      switch (languageNumber) {
        case 0:
          return `You have been reported for misbehaving`;
        case 1:
          return `Has sido reportado por mala conducta`;
        case 2:
          return `您因行为不端而被举报`;
        case 3:
          return `Vous avez été signalé pour mauvaise conduite`;
        case 4:
          return `تم الإبلاغ عن لك لسوء التصرف`;
        case 5:
          return `На вас поступило сообщение о неподобающем поведении`;
        case 6:
          return `Você foi denunciado por mau comportamento`;
        case 7:
          return `Sie wurden wegen Fehlverhaltens gemeldet`;
        default:
          return `You have been reported for misbehaving`;
        case 8:
          return `Sei stato segnalato per comportamento scorretto`;
        case 9:
          return `不正行為が報告されました`;
        case 10:
          return `잘못된 행동으로 신고되었습니다.`;
      }
    case 10:
      switch (languageNumber) {
        case 0:
          return `${name} ask you for a recommendation`;
        case 1:
          return `${name} te ha pedido una recomendación`;
        case 2:
          return `“${name}”向您寻求推荐 `;
        case 3:
          return `"${name}" vous a demandé une recommandation`;
        case 4:
          return `لقد طلب منك "${name}" توصية`;
        case 5:
          return `«${name}» попросил вас дать рекомендацию`;
        case 6:
          return `"${name}" pediu uma recomendação`;
        case 7:
          return `"${name}" hat Sie um eine Empfehlung gebeten`;
        case 8:
          return `"${name}" ti ha chiesto una raccomandazione`;
        case 9:
          return `「${name} 」はあなたに推薦を求めました`;
        case 10:
          return `"${name}"님이 추천을 요청했습니다.`;
        default:
          return `${name} ask you for a recommendation`;
      }
    case 11:
      switch (languageNumber) {
        case 0:
          return `Someone has answered your request`;
        case 1:
          return `Alguien a respondido tu petición`;
        case 2:
          return `有人回答了您的请求`;
        case 3:
          return `Quelqu'un a répondu à votre demande`;
        case 4:
          return `شخص ما أجاب على طلبك`;
        case 5:
          return `Кто-то ответил на ваш запрос`;
        case 6:
          return `Alguém respondeu sua solicitação`;
        case 7:
          return `Jemand hat Ihre Anfrage beantwortet`;
        case 8:
          return `Qualcuno ha risposto alla tua richiesta`;
        case 9:
          return `誰かがあなたの要求に答えました`;
        case 10:
          return `누군가가 귀하의 요청에 응답했습니다`;
        default:
          return `Someone has answered your request`;
      }
    case 12:
      switch (languageNumber) {
        case 0:
          return `"${name}" commented your recommendation`;
        case 1:
          return `"${name}" comentó tu recomendación`;
        case 2:
          return `“${name}”评论了你的推荐`;
        case 3:
          return `"${name}" a commenté votre recommandation`;
        case 4:
          return `علق "${name}" على توصيتك`;
        case 5:
          return `"${name}" прокомментировал вашу рекомендацию`;
        case 6:
          return `"${name}" comentou sua recomendação`;
        case 7:
          return `"${name}" hat Ihre Empfehlung kommentiert`;
        case 8:
          return `"${name}" ha commentato la tua raccomandazione`;
        case 9:
          return `「${name}」があなたの推薦にコメントしました`;
        case 10:
          return `"${name}"이 귀하의 추천에 댓글을 남겼습니다.`;
        default:
          return `${name} commented your recommendation`;
      }
    case 13:
      switch (languageNumber) {
        case 0:
          return `"${name}" send you a recommendation`;
        case 1:
          return `"${name}" te envió una recomendación`;
        case 2:
          return `"${name}" send you a recommendation`;
        case 3:
          return `"${name}" send you a recommendation`;
        case 4:
          return `"${name}" send you a recommendation`;
        case 5:
          return `"${name}" send you a recommendation`;
        case 6:
          return `"${name}" send you a recommendation`;
        case 7:
          return `"${name}" send you a recommendation`;
        case 8:
          return `"${name}" send you a recommendation`;
        case 9:
          return `"${name}" send you a recommendation`;
        case 10:
          return `"${name}" send you a recommendation`;
        default:
          return `"${name}" send you a recommendation`;
      }
    default:
      break;
  }
};
