const mongoose = require("mongoose");

const adminAuditSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    object: {
      type: String,
      enums: ["user", "business"],
    },
    objectId: {
      type: String,
    },
    field: {
      type: String,
      default: null,
    },
    oldValues: [
      {
        type: String,
      },
    ],
    newValues: [
      {
        type: String,
      },
    ],
    observation: {
      type: String,
      default: null,
    },
    action: {
      type: Number,
      enums: [
        1, // Update
        2, // Delete
        3, // New
      ],
    },
  },
  {
    timestamps: true,
  }
);

const AdminAudit = mongoose.model("adminAudits", adminAuditSchema);

module.exports = AdminAudit;
