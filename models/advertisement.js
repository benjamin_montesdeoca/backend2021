const mongoose = require("mongoose");

const advertisementSchema = mongoose.Schema(
  {
    business: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "businesses",
    },
    priority: {
      type: Number,
      enums: [
        0, // Sponsored forever
        1, // Sponsored - High
        2, // Sponsored - Medium
        3, // Sponsored - Low
      ],
    },
    description: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const Advertisement = mongoose.model("advertisements", advertisementSchema);

module.exports = Advertisement;
