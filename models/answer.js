const mongoose = require("mongoose");

const answerSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Users",
    },
    status: {
      type: Number,
      enums: [
        0, // No answered
        1, // Answered
        2, // Answered without business
      ],
      default: 0,
    },
    business: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Businesses",
    },
    request: {
      type: mongoose.Schema.Types.ObjectId,
      require: true,
      ref: "Requests",
    },
  },
  {
    timestamps: true,
  }
);

const Answer = mongoose.model("Answers", answerSchema);

module.exports = Answer;
