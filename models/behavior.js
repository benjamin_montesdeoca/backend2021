const mongoose = require("mongoose");

const behaviorSchema = mongoose.Schema(
  {
    reportedUser: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    reportingUser: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    behavior: {
      type: Number,
      enums: [
        1, // Good behavior
        2, // Normal
        3, // Bad behavior
      ],
      default: 2,
    },
    status: {
      type: Number,
      enums: [
        1, // Active
        2, // Reported
        3, // Banned
      ],
      default: 1,
    },
    reason: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "userreasonsreports",
      required: true,
    },
    review: {
      type: String,
    },
  },
  { timestamps: true }
);

const Behavior = mongoose.model("behaviors", behaviorSchema);

module.exports = Behavior;
