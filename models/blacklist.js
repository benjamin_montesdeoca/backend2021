const mongoose = require("mongoose");

const blacklistSchema = mongoose.Schema({
  word: {
    type: String,
  },
});

const BlackList = mongoose.model("blacklists", blacklistSchema);

module.exports = BlackList;
