const mongoose = require("mongoose");

const businessSchema = mongoose.Schema(
  {
    userCreator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    ownerUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    image: {
      type: String,
      default: "",
    },
    images: [
      {
        type: String,
      },
    ],
    name: {
      type: String,
      default: "",
    },
    description: {
      type: String,
      default: "",
    },
    mainAddress: {
      type: String,
      default: "",
    },
    phone: {
      type: String,
      default: "",
    },
    website: {
      type: String,
      default: "",
    },
    location: {
      type: { type: String, enums: ["Point"], required: true },
      coordinates: { type: [Number], required: true },
    },
    address: {
      type: String,
      default: "",
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    status: {
      type: Number,
      enums: [
        0, // Pending
        1, // Property of Locals Atlas or created
        2, // Claim pending
        3, // Claimed
      ],
      default: 1,
    },
    behavior: {
      type: Number,
      enums: [
        1, // Normal
        2, // Banned
        3, // Out of Service
        4, // Reported
      ],
      default: 1,
    },
    socialLinks: [
      {
        _id: false,
        social: { type: mongoose.Schema.Types.ObjectId, ref: "socials" },
        name: { type: String },
        image: { type: String },
        url: { type: String },
      },
    ],
    openingHours: {
      data: [
        {
          _id: { type: Number },
          schedules: [
            {
              _id: { type: Number },
              hour1: { type: Number },
              minute1: { type: Number },
              format1: { type: Number },
              hour2: { type: Number },
              minute2: { type: Number },
              format2: { type: Number },
            },
          ],
          isActive: { type: Boolean },
        },
      ],
      type: {
        type: Number,
        enums: [
          1, // Schedule
          2, // 24 hours
          3, // unified
        ],
      },
    },
    categories: [
      {
        _id: false,
        category: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Categories",
        },
        name: [
          {
            type: String,
          },
        ],
        subcategories: [
          {
            _id: false,
            subcategory: {
              type: mongoose.Schema.Types.ObjectId,
              ref: "Subcategories",
            },
            name: [
              {
                type: String,
              },
            ],
          },
        ],
      },
    ],
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tags",
      },
    ],
  },
  {
    timestamps: true,
  }
);

businessSchema.index({ location: "2dsphere" });

const Business = mongoose.model("Businesses", businessSchema);

module.exports = Business;
