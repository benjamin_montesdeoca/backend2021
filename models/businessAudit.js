const mongoose = require("mongoose");

const businessAuditSchema = mongoose.Schema(
  {
    business: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "businesses",
    },
    field: {
      type: String,
      required: true,
    },
    oldValues: [
      {
        type: String,
      },
    ],
    newValues: [
      {
        type: String,
      },
    ],
    action: {
      type: Number,
      enums: [
        1, // Update
        2, // Delete
        3, // New
      ],
    },
  },
  {
    timestamps: true,
  }
);

const BusinessAudit = mongoose.model("businessAudits", businessAuditSchema);

module.exports = BusinessAudit;
