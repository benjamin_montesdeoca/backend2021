const mongoose = require("mongoose");

const businessReasonsReportSchema = mongoose.Schema({
  description: [
    {
      type: String,
    },
  ],
});

const BusinessReasonsReport = mongoose.model(
  "businessreasonsreports",
  businessReasonsReportSchema
);

module.exports = BusinessReasonsReport;
