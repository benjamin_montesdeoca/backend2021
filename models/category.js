const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  name: [
    {
      type: String,
    },
  ],
});

const Category = mongoose.model("Categories", categorySchema);

module.exports = Category;
