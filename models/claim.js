const mongoose = require("mongoose");

const claimSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Users",
    },
    business: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Businesses",
    },
    location: {
      type: { type: String, enums: ["Point"], required: true },
      coordinates: { type: [Number], required: true },
    },
    businessInfo: {
      email: { type: String, required: true },
      phone: { type: String, required: true },
    },
    status: {
      type: Number,
      enums: [
        0, // Pending verification
        1, // Verified
        2, // Need more information
        3, // Conflict with other user who paid the claim
        4, // Conflict with other user who has not paid the claim yet
      ],
      required: true,
    },
    paid: {
      type: Number,
      enums: [
        0, // No
        1, // Yes
        2, // Refund
      ],
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Claim = mongoose.model("Claims", claimSchema);

module.exports = Claim;
