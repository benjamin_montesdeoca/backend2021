const mongoose = require("mongoose");

const countrySchema = mongoose.Schema({
  official: {
    type: String,
  },
  common: {
    type: String,
  },
  cca2: {
    type: String,
  },
  cca3: {
    type: String,
  },
  cioc: {
    type: String,
  },
  root: {
    type: String,
  },
  suffixes: {
    type: String,
  },
  uri: {
    type: String,
  },
});

const Country = mongoose.model("Countries", countrySchema);

module.exports = Country;
