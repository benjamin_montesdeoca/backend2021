const mongoose = require("mongoose");

const errorAuditSchema = mongoose.Schema({
  errorScreen: {
    type: String,
  },
  errorFunction: {
    type: String,
  },
  errorName: {
    type: String,
  },
  errorText: {
    type: String,
  },
  deviceBrand: {
    type: String,
  },
  deviceModel: {
    type: String,
  },
  deviceOsVersion: {
    type: String,
  },
  deviceOsName: {
    type: String,
  },
  errorDate: {
    type: String,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Users",
  },
});

const errorAudit = mongoose.model("errorAudits", errorAuditSchema);

module.exports = errorAudit;
