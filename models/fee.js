const mongoose = require("mongoose");

const feeSchema = mongoose.Schema({
  name: {
    type: String,
    unique: true,
  },
  price: {
    type: Number,
  },
  description: {
    type: String,
  },
});

const Fee = mongoose.model("Fees", feeSchema);

module.exports = Fee;
