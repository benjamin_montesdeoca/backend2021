const mongoose = require("mongoose");

const globalSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    ipaddress: {
      type: String,
    },
    location: {
      type: { type: String, enums: ["Point"], required: true },
      coordinates: { type: [Number], required: true },
    },
    device: {
      type: String,
    },
    language: {
      type: Number,
      enums: [
        0, // English
        1, // Spanish
        2, // Mandarin chinese
        3, // French
        4, // Arabic
        5, // Russian
        6, // Portuguese
        7, // German
        8, // Italian
        9, // Japanese
        10, // Korean
      ],
    },
    isNewUser: {
      type: Number,
      enums: [
        0, // Old users
        1, // New user
      ],
    },
  },
  {
    timestamps: true,
  }
);

const Global = mongoose.model("globals", globalSchema);

module.exports = Global;
