const mongoose = require("mongoose");

const notificationSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    title: [
      {
        type: "String",
      },
    ],
    message: [
      {
        type: String,
      },
    ],
    review: {
      type: String,
      default: null,
    },
    type: {
      type: Number,
      enum: [
        1, // Your business has been approved
        2, // Your business has been reported and temporarily banned
        3, // Your account has been restored
        4, // You have received a friend request
        5, // Your friend request has been accepted
        6, // A user has sent you a business request
        7, // You have received an answer to your request
        8, // You have reported a business
        9, // Your business has been recommended
        10, // You have been reported for misbehaving
        11, // You have reported a user
        12, // Your business has no longer been recommended by a user
        13, // Someone send you a recommendation
      ],
    },
    isState: {
      type: Number,
      enum: [
        0, // No open
        1, // Open
      ],
    },
  },
  { timestamps: true }
);

const Notification = mongoose.model("notifications", notificationSchema);

module.exports = Notification;
