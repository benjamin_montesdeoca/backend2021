const mongoose = require("mongoose");

const recommendationAuditSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    business: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "businesses",
    },
    typeRecommendation: {
      type: Number,
      enums: [
        0, // Pending my decision
        1, // Simply the best
        2, // Highly recommended
        3, // Excellent service
      ],
    },
    askFor: {
      type: String,
      default: "",
    },
    tryThis: {
      type: String,
      default: "",
    },
    best: {
      type: String,
      default: "",
    },
    isShown: {
      type: Number,
      enums: [
        0, // Anyone,
        1, // Only for my friends and me
        2, // Keep this recommendation private. Only I can share with other
      ],
      default: 0,
    },
    usedAs: {
      type: Number,
      enums: [
        0, // Personal
        1, // Professional
        2, // Both
      ],
    },
    recommendedBy: {
      type: Number,
      enum: [
        1, // Local
        2, // Traveller
      ],
    },
    review: {
      type: String,
      default: "",
    },
    status: {
      type: Number,
      enums: [
        0, // archived
        1, // active
      ],
      default: 0,
    },
    reason: {
      type: String,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

const RecommendationAudit = mongoose.model(
  "recommendationAudits",
  recommendationAuditSchema
);

module.exports = RecommendationAudit;
