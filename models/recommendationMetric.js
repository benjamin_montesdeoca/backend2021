const mongoose = require("mongoose");

const recommendationMetricSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Users",
    },
    business: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Businesses",
    },
    score: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const RecommendationMetric = mongoose.model(
  "RecommendationMetrics",
  recommendationMetricSchema
);

recommendationMetricSchema.index({ createdAt: 1 });

module.exports = RecommendationMetric;
