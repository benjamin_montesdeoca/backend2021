const mongoose = require("mongoose");

const reportSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
      required: true,
    },
    business: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "businesses",
      required: true,
    },
    type: {
      type: Number,
      enum: [
        1, // Inappropriate content
        2, // Fake user
      ],
    },
    reason: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "businessreasonsreports",
      required: true,
    },
    review: {
      type: String,
    },
    email: {
      type: String,
    },
    phone: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const Report = mongoose.model("reports", reportSchema);

module.exports = Report;
