const mongoose = require("mongoose");

const requestSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Users",
    },
    lookingFor: {
      type: String,
      required: true,
    },
    location: {
      type: { type: String, enums: ["Point"], required: true },
      coordinates: { type: [Number], required: true },
    },
    distance: {
      type: Number,
      required: true,
    },
    status: {
      type: Number,
      enums: [
        0, // no answered
        1, // answered
      ],
      default: 0,
    },
    typeRequest: {
      type: Number,
      enum: [
        0, // locals
        1, // friends
        2, // direct
        3, // without request
      ],
    },
  },
  {
    timestamps: true,
  }
);

const Request = mongoose.model("Requests", requestSchema);

module.exports = Request;
