const mongoose = require("mongoose");

const socialSchema = mongoose.Schema({
  name: {
    type: String,
  },
  url: {
    type: String,
  },
  image: {
    type: String,
  },
});

const Social = mongoose.model("socials", socialSchema);

module.exports = Social;
