const mongoose = require("mongoose");

const subcategorySchema = mongoose.Schema({
  name: [
    {
      type: String,
    },
  ],
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Categories",
    required: true,
  },
});

const Subcategory = mongoose.model("Subcategories", subcategorySchema);

module.exports = Subcategory;
