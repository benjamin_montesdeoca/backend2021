const mongoose = require("mongoose");

const tagSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
});

const Tag = mongoose.model("Tags", tagSchema);

module.exports = Tag;
