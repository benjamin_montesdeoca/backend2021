const mongoose = require("mongoose");

const tokenSchema = mongoose.Schema(
  {
    user: {
      type: String,
      ref: "Users",
      required: true,
    },
    token: {
      type: String,
      required: true,
    },
    expiresIn: {
      type: Date,
    },
    status: {
      type: Number,
      enums: [
        0, // Was not used
        1, // Was used
      ],
    },
    reason: {
      type: Number,
      enums: [
        1, // reset password
      ],
    },
  },
  { timestamps: { createdAt: true, updatedAt: false } }
);

const Token = mongoose.model("Tokens", tokenSchema);

module.exports = Token;
