const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    mainLocation: {
      type: { type: String, enums: ["Point"] },
      coordinates: { type: [Number] },
    },
    mainAddress: {
      type: String,
      default: "",
    },
    optionalLocation01: {
      type: { type: String, enums: ["Point"] },
      coordinates: { type: [Number] },
    },
    optionalAddress01: {
      type: String,
      default: "",
    },
    optionalLocation02: {
      type: { type: String, enums: ["Point"] },
      coordinates: { type: [Number] },
    },
    optionalAddress02: {
      type: String,
      default: "",
    },
    facebookToken: {
      type: String,
      default: "",
    },
    googleToken: {
      type: String,
      default: "",
    },
    notificationToken: {
      type: String,
      default: "",
    },
    isActive: {
      type: Boolean,
      default: false,
    },
    authenticationMethod: {
      type: Number,
      enums: [
        1, // Normal
        2, // Facebook
        3, // Google
        4, // Apple
      ],
      default: 1,
    },
    status: {
      type: Number,
      enums: [
        1, // Normal
        2, // Banned
        3, // Reported
      ],
      default: 1,
    },
    profile: {
      image: {
        type: String,
      },
      firstname: {
        type: String,
        required: true,
      },
      lastname: {
        type: String,
        required: true,
      },
      phone: {
        type: String,
        default: true,
      },
      mainInterests: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Categories",
        },
      ],
    },
    roles: {
      type: String,
      enums: ["user", "admin", "operator"],
      default: "user",
    },
    type: {
      type: String,
      enums: ["private", "public"],
      default: "public",
    },
    friends: [
      {
        user: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Users",
        },
        status: {
          type: Number,
          enums: [
            0, // Add friend
            1, // Requested
            2, // Pending
            3, // Friends
          ],
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

userSchema.methods.matchPassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    next();
  }

  const salt = await bcrypt.genSalt(10);

  this.password = await bcrypt.hash(this.password, salt);
});

userSchema.index({ mainLocation: "2dsphere" });
userSchema.index({ optionalLocation01: "2dsphere" });
userSchema.index({ optionalLocation02: "2dsphere" });

const User = mongoose.model("Users", userSchema);

module.exports = User;
