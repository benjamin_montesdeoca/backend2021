const mongoose = require("mongoose");

const userAuditSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "users",
    },
    field: {
      type: String,
      required: true,
    },
    oldValues: [
      {
        type: String,
      },
    ],
    newValues: [
      {
        type: String,
      },
    ],
    action: {
      type: Number,
      enums: [
        1, // Update
        2, // Delete
        3, // New
      ],
    },
  },
  {
    timestamps: true,
  }
);

const UserAudit = mongoose.model("userAudits", userAuditSchema);

module.exports = UserAudit;
