const mongoose = require("mongoose");

const userReasonsReportSchema = mongoose.Schema({
  description: [
    {
      type: String,
    },
  ],
});

const UserReasonsReport = mongoose.model(
  "userreasonsreports",
  userReasonsReportSchema
);

module.exports = UserReasonsReport;
