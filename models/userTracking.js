const mongoose = require("mongoose");

const userTrackingSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
  },
  action: {
    type: Number,
    enums: [
      1, // visit a business
      2, // recommend a business
      3, // send a business in a request
      4, // receive a business in a request
      5, // call the business
      6, // open map for business
      7, // visit the business website
      8, // open business info
      9, // open statistics
      10, // open services
      11, // open gallery
      12, // most recommended
      13, // most related
      14, // delete a recommend
    ],
  },
  screen: {
    type: String,
  },
  business: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "businesses",
  },
  initialDate: {
    type: String,
  },
  finalDate: {
    type: String,
  },
});

const UserTracking = mongoose.model("userTrackings", userTrackingSchema);

module.exports = UserTracking;
