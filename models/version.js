const mongoose = require("mongoose");

const versionSchema = mongoose.Schema(
  {
    version: {
      type: String,
    },
    description: {
      type: String,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    appLinkAndroid: {
      type: "String",
    },
    appLinkIOS: {
      type: "String",
    },
    appLink: {
      type: "String",
    },
  },
  {
    timestamps: true,
  }
);

const Version = mongoose.model("versions", versionSchema);

module.exports = Version;
