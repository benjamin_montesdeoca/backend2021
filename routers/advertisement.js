const express = require("express");
const router = express.Router();

const adController = require("../controllers/advertisement");

router.post("/", adController.createBusinessAd);

module.exports = router;
