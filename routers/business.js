const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");
const { uploadBusinessImage } = require("../utils/uploadImages");

const businessController = require("../controllers/business");

// @route   POST api/businesses
// @desc    Create a new business from the current user
// @access  Private
// @role    User
router.post("/", protection, businessController.createBusiness);

// @route   POST api/businesses
// @desc    Create a new business from the web
// @access  Public
// @role    User
router.post("/prelaunch", businessController.createBusinessPreLaunch);

// @route   PUT api/businesses
// @desc    Save the business hours, it is divided into the 7 days of the week
// @access  Private
// @role    User
router.put(
  "/openinghours/:id",
  protection,
  businessController.updateBusinessOpeningHours
);

// @route   PUT api/businesses
// @desc    Update the categories and subcategories of the business
// @access  Private
// @role    User
router.put(
  "/features/:id",
  protection,
  businessController.updateBusinessCategoriesAndTags
);

// @route   PUT api/businesses
// @desc    Update the categories and subcategories of the business
// @access  Private
// @role    User
router.put("/social/:id", protection, businessController.updateBusinessSocials);

// @route   PUT api/businesses
// @desc    Update the categories and subcategories of the business
// @access  Private
// @role    User
router.put("/:id", protection, businessController.updateBusiness);

// @route   PUT api/businesses
// @desc    Update the main image of the business
// @access  Private
// @role    User
router.put(
  "/image/:id",
  [protection, uploadBusinessImage.single("image")],
  businessController.updateBusinessImage
);

// @route   PUT api/businesses
// @desc    Update the secondary images of the business
// @access  Private
// @role    User
router.put(
  "/images/:id",
  [protection, uploadBusinessImage.array("images")],
  businessController.updateBusinessImages
);

// @route   DELETE api/businesses
// @desc    Delete a business image from the server directory --Check later--
// @access  Private
// @role    User
router.delete(
  "/images/:id",
  protection,
  businessController.deleteBusinessImages
);

// @route   PUT api/businesses
// @desc    Save the user's browsing history in the app
// @access  Private
// @role    User
router.post("/save", protection, businessController.saveBusinessVisitedByUser);

// @route   GET api/businesses
// @desc    Returns the current user’s business, whether created or claimed
// @access  Private
// @role    User
router.get("/me", protection, businessController.getAllMyBusiness);

// @route   GET api/businesses
// @desc    Returns the businesses with the most recommendations near the current user
// @access  Private
// @role    User
router.get("/top", protection, businessController.getTopRecommendedBusiness);

// @route   GET api/businesses
// @desc    Returns businesses with better chances of being recommended to the user
// @access  Private
// @role    User
router.get("/ads", protection, businessController.getBusinessForCarousel);

// @route   GET api/businesses
// @desc    Returns the businesses according to the interests of the current user
// @access  Private
// @role    User
router.get("/sales", protection, businessController.getBusinessesForSale);

// @route   GET api/businesses
// @desc    Returns the businesses that have been recommended by the current user
// @access  Private
// @role    User
router.get("/myatlas", protection, businessController.getBusinessForMyAtlas);

// @route   GET api/businesses
// @desc    Returns the businesses that are stored within the database and that have not been recommended by the current user
// @access  Private
// @role    User
router.get(
  "/localsatlas",
  protection,
  businessController.getBusinessForLocalsAtlas
);

// @route   GET api/businesses
// @desc    Return businesses that friends have recommended
// @access  Private
// @role    User
router.get(
  "/friendsatlas",
  protection,
  businessController.getBusinessForFriendsAtlas
);

// @route   GET api/businesses
// @desc    Returns the most recommended businesses on the platform
// @access  Private
// @role    User
router.get(
  "/homeatlas",
  protection,
  businessController.getBusinessRecommendedForHome
);

// @route   GET api/businesses
// @desc    Returns the recommendations of the business according to its id
// @access  Private
// @role    User
router.get(
  "/recommendations/:id",
  protection,
  businessController.getRecommendationsFromBusiness
);

// @route   GET api/businesses
// @desc    Returns all the businesses created in the app for the administrator
// @access  Private
// @role    Admin, operator
router.get(
  "/",
  [protection, allowedTo("admin", "operator")],
  businessController.getAllBusiness
);

// @route   GET api/businesses
// @desc    Returns the business by its id to the administrator
// @access  Private
// @role    Admin, operator
router.get(
  "/:id",
  [protection, allowedTo("admin", "operator")],
  businessController.getBusinessById
);

// @route   GET api/businesses
// @desc    Returns the business creator user
// @access  Private
// @role    Admin, operator
router.get(
  "/:id/user-creator",
  [protection, allowedTo("admin", "operator")],
  businessController.getUserCreatorFromBusiness
);

// @route   GET api/businesses
// @desc    Returns the business creator user
// @access  Private
// @role    Admin, operator
router.get(
  "/:id/owner-user",
  [protection, allowedTo("admin", "operator")],
  businessController.getOwnerUserFromBusiness
);

// @route   GET api/businesses
// @desc    Returns the recommendation count of a business
// @access  Private
// @role    Admin, operator
router.get(
  "/recommendations/:id/count",
  [protection, allowedTo("admin", "operator")],
  businessController.getBusinessRecommendationCount
);

// @route   PUT api/businesses
// @desc    Activate a business
// @access  Private
// @role    Admin, operator
router.put(
  "/:id/approve",
  [protection, allowedTo("admin", "operator")],
  businessController.activateBusiness
);

// @route   PUT api/businesses
// @desc    Update a business by Id
// @access  Private
// @role    Admin, operator
router.put(
  "/:id/update",
  [protection, allowedTo("admin", "operator")],
  businessController.updateBusinessById
);

// @route   PUT api/businesses
// @desc
// @access  Private
// @role    Admin, operator
router.get(
  "/recommendations/top",
  [protection, allowedTo("admin", "operator")],
  businessController.getTop5Business
);

module.exports = router;
