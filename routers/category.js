const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const categoryController = require("../controllers/category");

// @route   GET api/categories
// @desc    Returns all the categories of the application
// @access  Private
// @role    User
router.get("/", categoryController.getAllCategories);

// @route   GET api/categories
// @desc    Returns all the subcategories of the application
// @access  Private
// @role    User
router.get("/subcategories", categoryController.getAllSubcategories);

// @route   GET api/categories
// @desc    Returns all the categories with their subcategories of the application
// @access  Private
// @role    User
router.get(
  "/withsubcategories",
  categoryController.getAllCategoriesWithSubcategories
);

// @route   GET api/categories
// @desc    Return all categories
// @access  Private
// @role    User
router.get(
  "/all",
  [protection, allowedTo("admin")],
  categoryController.getCategories
);

// @route   GET api/categories
// @desc    Return category and subcategories
// @access  Private
// @role    User
router.get(
  "/all/:id/subcategories",
  [protection, allowedTo("admin")],
  categoryController.getCategoryAndSubcategories
);

// @route   GET api/categories
// @desc    Return category by ID
// @access  Private
// @role    User
router.get(
  "/:id",
  [protection, allowedTo("admin")],
  categoryController.getCategoryById
);

// @route   POST api/categories
// @desc    Allows to create a category
// @access  Private
// @role    User
router.post(
  "/",
  [protection, allowedTo("admin")],
  categoryController.createCategory
);

// @route   POST api/categories
// @desc    Allows to create subcategories for a category
// @access  Private
// @role    User
router.post(
  "all/:id/subcategories",
  [protection, allowedTo("admin")],
  categoryController.createSubcategoriesForCategory
);

module.exports = router;
