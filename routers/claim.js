const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const claimController = require("../controllers/claim");

// @route   GET api/claims
// @desc    Send a code to the current user to be able to claim a business
// @access  Private
// @role    User
router.post("/authenticate", claimController.sendClaimActivationCode);

// @route   GET api/claims
// @desc    Returns the claim status of a selected business
// @access  Private
// @role    User
router.get("/status/:id", protection, claimController.businessClaimStatus);

// @route   POST api/claims
// @desc    Claim a business that exists on the platform and is part of the current user’s busines
// @access  Private
// @role    User
router.post("/:id", protection, claimController.createBusinessClaim);

// @route   POST api/claims --- Not in use
// @desc    Allows to pay for claiming a business to the current user
// @access  Public
// @role    User
router.post("/payment", claimController.paymentToClaimBusiness);

// @route   PUT api/claims --- Not in use
// @desc    Redirection of the payment page to the success page in the claim of a business made by the current user
// @access  Public
// @role    User
router.put("/success", claimController.updateBusinessStatusAfterClaim);

// @route   GET api/claims
// @desc    Get all claims
// @access  Private
// @role    Admin, Operator
router.get(
  "/",
  [protection, allowedTo("admin", "operator")],
  claimController.getAllClaims
);

// @route   GET api/claims
// @desc    Get the claim for the ID
// @access  Private
// @role    Admin, Operator
router.get(
  "/:id",
  [protection, allowedTo("admin", "operator")],
  claimController.getClaimById
);

// @route   PUST api/claims
// @desc    Get the claim for the ID
// @access  Private
// @role    Admin, Operator
router.put(
  "/:id/approve",
  [protection, allowedTo("admin", "operator")],
  claimController.approveBusinessClaim
);

module.exports = router;
