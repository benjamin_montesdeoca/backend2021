const express = require("express");
const router = express.Router();

const countryController = require("../controllers/country");

// @route   GET api/friends
// @desc    Returns a list of countries that is used in the creation of user profiles, specifically for phone numbers
// @access  Private
// @role    User
router.get("/", countryController.getCountries);

module.exports = router;
