const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const errorController = require("../controllers/error");

// @route   POST api/errors
// @desc    If there is an error in the app, it saves it
// @access  Private
// @role    User
router.post("/", protection, errorController.createError);

// @route   GET api/errors
// @desc    Return all errors
// @access  Private
// @role    Admin
router.get("/", [protection, allowedTo("admin")], errorController.getErrors);

// @route   GET api/errors
// @desc    Return a error by ID
// @access  Private
// @role    Admin
router.get(
  "/:id",
  [protection, allowedTo("admin")],
  errorController.getErrorById
);

module.exports = router;
