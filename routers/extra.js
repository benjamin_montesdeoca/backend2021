const express = require("express");
const router = express.Router();

const extraController = require("../extras/extra");

router.post("/", extraController.sendMailByMarketing);

router.post("/massive", extraController.sendMailsByMarketing);

router.post("/mms", extraController.sendMMS);

module.exports = router;
