const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const feeController = require("../controllers/fee");

router.get("/claim", feeController.getClaimFee);

// ######################################################################
// Administrator or operators API
// ######################################################################

router.post("/", [protection, allowedTo("admin")], feeController.createFee);

router.put(
  "/claim",
  [protection, allowedTo("admin")],
  feeController.updateClaimFee
);

module.exports = router;
