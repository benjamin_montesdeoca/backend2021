const express = require("express");
const router = express.Router();

const { protection } = require("../middleware/auth");

const friendController = require("../controllers/friend");

// @route   PUT api/friends
// @desc    Send a friend request to a user
// @access  Private
// @role    User
router.put("/:id", protection, friendController.createFriendRequest);

// @route   PUT api/friends
// @desc    Accept a friend request from a user
// @access  Private
// @role    User
router.put("/accept/:id", protection, friendController.acceptFriendRequest);

// @route   DELETE api/friends
// @desc    Remove a friend request from the current user
// @access  Private
// @role    User
router.delete("/reject/:id", protection, friendController.deleteFriendRequest);

// @route   DELETE api/friends
// @desc    Remove the friend of the current user
// @access  Private
// @role    User
router.delete("/unfriend/:id", protection, friendController.deleteFriendship);

// @route   GET api/friends
// @desc    Returns all users that are friends of the current user
// @access  Private
// @role    User
router.get("/me", protection, friendController.getFriendsFromCurrentUser);

// @route   GET api/friends
// @desc    Returns all friend requests for the current user
// @access  Private
// @role    User
router.get("/requests/me", protection, friendController.getMyFriendRequests);

// @route   GET api/friends
// @desc    Returns all users who are not friends of the current user
// @access  Private
// @role    User
router.get("/nonfriends", protection, friendController.getNonFriendsUsers);

module.exports = router;
