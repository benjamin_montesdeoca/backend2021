const express = require("express");
const router = express.Router();

const { protection } = require("../middleware/auth");

const globalController = require("../controllers/global");

// @route   GET api/globals
// @desc    Returns the user's language
// @access  Private
// @role    User
router.get("/language", protection, globalController.getLanguageOfUser);

// @route   POST api/globals
// @desc    Returns the value if you are a new user
// @access  Private
// @role    User
router.get("/isnewuser", protection, globalController.getIsNewUser);

// @route   PUT api/globals
// @desc    Save the value for the user language for settings
// @access  Private
// @role    User
router.put(
  "/language",
  protection,
  globalController.updateGlobalLanguageForUser
);

// @route   PUT api/errors
// @desc    Save the value for the user state for settings
// @access  Private
// @role    User
router.put("/isnewuser", protection, globalController.updateIsNewUser);

module.exports = router;
