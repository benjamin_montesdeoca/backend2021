const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const notificationController = require("../controllers/notification");

// @route   PUT api/notifications
// @desc    Update the status of a notification
// @access  Private
// @role    User
router.put("/:id", protection, notificationController.updateNotificationStatus);

// @route   GET api/notifications
// @desc    Returns all notifications for the current user
// @access  Private
// @role    User
router.get("/me", protection, notificationController.getUserNotifications);

// @route   GET api/notifications
// @desc    Get all notifications
// @access  Private
// @role    Admin, Operator
router.get(
  "/",
  [protection, allowedTo("admin", "operator")],
  notificationController.getNotifications
);

// @route   GET api/notifications
// @desc
// @access  Private
// @role    User
router.get("/count", protection, notificationController.getNotificationCount);

module.exports = router;
