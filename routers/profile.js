const express = require("express");
const router = express.Router();

const { protection } = require("../middleware/auth");
const { uploadProfileImage } = require("../utils/uploadImages");

const profileController = require("../controllers/profile");

// @route   PUT api/profiles
// @desc    Update the user profile with more data necessary for the proper functioning of the application
// @access  Private
// @role    User
router.put("/", protection, profileController.updateProfile);

// @route   PUT api/profiles
// @desc    Update the user’s profile with the interests or favorite categories for the correct functioning of the application
// @access  Private
// @role    User
router.put("/interests", protection, profileController.updateProfileInterests);

// @route   PUT api/profiles
// @desc    Update user image
// @access  Private
// @role    User
router.put(
  "/image",
  [protection, uploadProfileImage.single("image")],
  profileController.updateProfileImage
);

module.exports = router;
