const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const recommendationController = require("../controllers/recommendation");

// @route   GET api/recommendations
// @desc    Returns a result that indicates if the current user has made a recommendation to the business
// @access  Private
// @role    User
router.get(
  "/recommended/:id",
  protection,
  recommendationController.isThisBusinessRecommendedByMe
);

// @route   GET api/recommendations
// @desc    Returns the recommendation count of a business for the current user
// @access  Private
// @role    User
router.get(
  "/count/:id",
  protection,
  recommendationController.countIfLocalsOrTravellers
);

// @route   GET api/recommendations
// @desc    Returns the number of recommendations of a business divided by months
// @access  Private
// @role    User
router.get(
  "/monthly/:id",
  protection,
  recommendationController.countMonthlyAtlers
);

// @route   GET api/recommendations
// @desc    Returns the number of recommendations of a business divided by weeks
// @access  Private
// @role    User
router.get(
  "/weekly/:id",
  protection,
  recommendationController.countWeeklyAtlers
);

// @route   GET api/recommendations
// @desc    Returns a recommendation made by a user of a business for the current user
// @access  Private
// @role    User
router.get(
  "/:user/:business",
  protection,
  recommendationController.getRecommendationFromUser
);

// @route   POST api/recommendations
// @desc    Recommend a business selected by the current user and earn a point in the recommendation history
// @access  Private
// @role    User
router.post("/", protection, recommendationController.createRecommendation);

// @route   POST api/recommendations
// @desc
// @access  Private
// @role    User
router.post(
  "/contact",
  protection,
  recommendationController.createRecommendationFromContact
);

// @route   PUT api/recommendations
// @desc    Update a recommendation made by the current user
// @access  Private
// @role    User
router.put("/:id", protection, recommendationController.updateRecommendation);

// @route   PUT api/recommendations
// @desc    Delete a recommendation selected by the current user and a point is lost in the recommendation history
// @access  Private
// @role    User
router.put(
  "/remove/:id",
  protection,
  recommendationController.removeRecommendation
);

// @route   POST api/recommendations
// @desc    Add a comment to a recommendation chosen by the current user
// @access  Private
// @role    User
router.post("/comment", protection, recommendationController.createComment);

// @route   POST api/recommendations
// @desc
// @access  Private
// @role    Admin
router.post(
  "/:id",
  [protection, allowedTo("admin")],
  recommendationController.createRecommendationByAdmin
);

module.exports = router;
