const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const reportController = require("../controllers/report");

// @route   POST api/reports
// @desc    Allows the current user to report a business
// @access  Private
// @role    User
router.post("/business", protection, reportController.reportABusiness);

// @route   POST api/reports
// @desc    Allows the current user to report to a different user
// @access  Private
// @role    User
router.post("/user/:id", protection, reportController.reportAUser);

// @route   POST api/reports
// @desc    Create a new business report reason
// @access  Private
// @role    Admin
router.post(
  "/reasons/business",
  [protection, allowedTo("admin")],
  reportController.createBusinessReasonsReport
);

// @route   GET api/reports
// @desc    Get all the reasons to report a business
// @access  Public
// @role    All
router.get("/reasons/business", reportController.getBusinessReasonsReport);

// @route   POST api/reports
// @desc    Create a new user report reason
// @access  Private
// @role    Admin
router.post(
  "/reasons/user",
  [protection, allowedTo("admin")],
  reportController.createUserReasonsReport
);

// @route   GET api/reports
// @desc    Get all the reasons to report a user
// @access  Public
// @role    All
router.get("/reasons/user", reportController.getUserReasonsReport);

module.exports = router;
