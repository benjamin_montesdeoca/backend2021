const express = require("express");
const router = express.Router();

const { protection } = require("../middleware/auth");

const requestController = require("../controllers/request");

// @route   POST api/requests
// @desc    Make a request about a business to several users made by the current user
// @access  Private
// @role    User
router.post("/", protection, requestController.createRequest);

// @route   POST api/requests
// @desc
// @access  Private
// @role    User
router.post("/directs", protection, requestController.createRequestAndAnswer);

// @route   PUT api/requests
// @desc    The current user answers a request with a business made by some user
// @access  Private
// @role    User
router.put("/answer/:id", protection, requestController.answerRequest);

// @route   PUT api/requests
// @desc    Mark an answer as answered
// @access  Private
// @role    User
router.put(
  "/answer/:id/status",
  protection,
  requestController.markAnswerAsAnswered
);

// @route   GET api/requests
// @desc    Returns all requests made by the current user
// @access  Private
// @role    User
router.get("/me", protection, requestController.getAllMyRequests);

// @route   GET api/requests
// @desc    Returns all requests made to the current user
// @access  Private
// @role    User
router.get("/me/request", protection, requestController.getAllRequestsMadeToMe);

// @route   GET api/requests
// @desc    Returns the last requests made to the current user
// @access  Private
// @role    User
router.get(
  "/me/request/top",
  protection,
  requestController.getTopRequestsMadeToMe
);

// @route   GET api/requests
// @desc    Returns the last requests made to the current user
// @access  Private
// @role    User
router.get("/answer/me", protection, requestController.getAllMyAnswers);

// @route   GET api/requests
// @desc    Returns the answer of a request
// @access  Private
// @role    User
router.get("/answer/me/:id", protection, requestController.getAnswerForRequest);

// @route   GET api/requests
// @desc    Returns the friends to whom a request can be made
// @access  Private
// @role    User
router.get(
  "/me/friends",
  protection,
  requestController.getMyFriendsForRequests
);

// @route   GET api/requests
// @desc    Returns all users who can answer a request
// @access  Private
// @role    User
router.get("/me/locals", protection, requestController.getLocalsForRequests);

module.exports = router;
