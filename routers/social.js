const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const socialController = require("../controllers/social");

// @route   GET api/socials
// @desc    Get all social network
// @access  Public
// @role    User
router.get("/", socialController.getSocial);

// @route   POST api/socials
// @desc    Create a new social network
// @access  Private
// @role    Admin, Operator
router.post(
  "/",
  [protection, allowedTo("admin", "operator")],
  socialController.createSocial
);

module.exports = router;
