const express = require("express");
const router = express.Router();

const { protection } = require("../middleware/auth");

const trackingController = require("../controllers/tracking");

// @route   POST /api/trackings
// @desc    Save user browsing data
// @access  Private
// @role    User
router.post("/user", protection, trackingController.userTracking);

module.exports = router;
