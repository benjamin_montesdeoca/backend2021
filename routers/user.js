const express = require("express");
const router = express.Router();

const { protection, allowedTo } = require("../middleware/auth");

const userController = require("../controllers/user");

// @route   PUT api/users
// @desc    Save the new notifications token
// @access  Private
// @role    User
router.put(
  "/notificationtoken",
  protection,
  userController.updateNotificationToken
);

// @route   POST api/users
// @desc    Send a text message or email with the authentication code to the user
// @access  Public
// @role    User
router.post("/authenticate", userController.sendActivationCode);

// @route   GET api/users
// @desc
// @access  Private
// @role    User
router.get("/isactive", protection, userController.isUserActive);

// @route   GET api/users
// @desc    Returns the number of users registered in the app
// @access  Private
// @role    Admin, Operator
router.get(
  "/registered/count",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalRegisteredUsers
);

// @route   GET api/users
// @desc    Returns the number of users registered in the app
// @access  Private
// @role    Admin, Operator
router.get(
  "/active/count",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalActiveUsers
);

// @route   GET api/users
// @desc    Returns the number of inactive users in the app
// @access  Private
// @role    Admin, Operator
router.get(
  "/inactive/count",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalInactiveUsers
);

// @route   GET api/users
// @desc    Returns the number of inactive users in the app
// @access  Private
// @role    Admin, Operator
router.get(
  "/reported/count",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalReportedUsers
);

// @route   GET api/users
// @desc    Returns the user's profile by ID
// @access  Private
// @role    Admin, Operator
router.get(
  "/:id/profile",
  [protection, allowedTo("admin", "operator")],
  userController.getUserProfileById
);

// @route   GET api/users
// @desc    Return all users of the app
// @access  Private
// @role    Admin, Operator
router.get(
  "/",
  [protection, allowedTo("admin", "operator")],
  userController.getUsers
);

// @route   POST api/users
// @desc    Allows the administrator to create users
// @access  Private
// @role    Admin, Operator
router.post(
  "/",
  [protection, allowedTo("admin", "operator")],
  userController.createUser
);

// @route   POST api/users
// @desc    Allows the administrator to ban users
// @access  Private
// @role    Admin, Operator
router.put(
  "/:id/ban",
  [protection, allowedTo("admin", "operator")],
  userController.banUser
);

// @route   PUT api/users
// @desc    Allows the administrator to activate users
// @access  Private
// @role    Admin, Operator
router.put(
  "/:id/active",
  [protection, allowedTo("admin", "operator")],
  userController.activeUser
);

// @route   PUT api/users
// @desc    Allows the administrator to modify users
// @access  Private
// @role    Admin, Operator
router.put(
  "/:id",
  [protection, allowedTo("admin", "operator")],
  userController.updateUser
);

// @route   GET api/users
// @desc    Gets the number of business of a user
// @access  Private
// @role    Admin, Operator
router.get(
  "/:id/businesses",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalBusinessFromUser
);

// @route   POST api/users
// @desc
// @access  Private
// @role    User
router.delete("/", protection, userController.deleteUser);

// @route   POST api/users
// @desc
// @access  Private
// @role    Admin
router.delete(
  "/:id",
  [protection, allowedTo("admin")],
  userController.deleteUserById
);

// @route   GET api/users
// @desc    Get the number of friends of a user
// @access  Private
// @role    Admin, Operator
router.get(
  "/:id/friends",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalFriendsFromUser
);

// @route   GET api/users
// @desc    Get the number of recommendations of a user
// @access  Private
// @role    Admin, Operator
router.get(
  "/:id/recommendations",
  [protection, allowedTo("admin", "operator")],
  userController.getTotalRecommendationFromUser
);

// @route   GET api/users
// @desc    Get the coordinates of the users
// @access  Private
// @role    Admin, Operator
router.get(
  "/coordinates",
  [protection, allowedTo("admin", "operator")],
  userController.getUsersCoordinates
);

// @route   GET api/users
// @desc
// @access  Private
// @role    Admin, Operator
router.get(
  "/recommendations/top",
  [protection, allowedTo("admin", "operator")],
  userController.getTop5RecommenderUsers
);

// @route   GET api/users
// @desc
// @access  Private
// @role    Admin, Operator
router.get(
  "/friends/top",
  [protection, allowedTo("admin", "operator")],
  userController.getTopUserWithMoreFriends
);

// @route   GET api/users
// @desc    Get the data of the current user
// @access  Private
// @role    User
router.get("/me", protection, userController.getCurrentUser);

// @route   GET api/users
// @desc    Returns the data of a user according to their ID
// @access  Private
// @role    User
router.get("/:id", protection, userController.getUserById);

// @route   POST api/users
// @desc    Allows the creation of new users
// @access  Public
// @role    User
router.post("/register", userController.userRegister);

// @route   POST api/users
// @desc    Obtains the user’s data together with the unique token of each user to interact with other APIs
// @access  Public
// @role    User
router.post("/login", userController.userLogin);

// @route   POST api/users
// @desc    Send an email to the user in which there is a URL where the password can be changed
// @access  Public
// @role    User
router.post("/reset/password", userController.resetPassword);

// @route   PUT api/users
// @desc    Allows the change of password for a new one
// @access  Public
// @role    User
router.put("/reset/password/:token", userController.putNewPassword);

// @route   POST api/users
// @desc
// @access  Public
// @role    User
router.post("/register/facebook", userController.userRegisterFacebook);

// @route   POST api/users
// @desc
// @access  Public
// @role    User
router.post("/register/google", userController.userRegisterGoogle);

// @route   POST api/users
// @desc
// @access  Public
// @role    User
router.post("/register/apple", userController.userRegisterApple);

// @route   POST api/users
// @desc
// @access  Public
// @role    User
router.post("/login/apple", userController.userSignInApple);

// @route   PUT api/users
// @desc    Verify that the user has a valid email and can interact with the application
// @access  Public
// @role    User
router.put("/activate/:id", userController.activateUser);

// @route   PUT api/users
// @desc    Read the contacts received from the cell phone and verify if that contact is a user of the app
// @access  Private
// @role    User
router.post("/contacts", protection, userController.compareContactsWithUsers);

module.exports = router;
