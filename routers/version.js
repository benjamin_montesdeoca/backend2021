const express = require("express");
const router = express.Router();

const versionController = require("../controllers/version");

// @route   PUT api/versions
// @desc    Create the version of the app
// @access  Public
// @role    User
router.post("/", versionController.createVersion);

// @route   PUT api/versions
// @desc    Returns the latest version of the app
// @access  Public
// @role    User
router.get("/", versionController.getVersion);

module.exports = router;
