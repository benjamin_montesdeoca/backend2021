require("mongoose");
const dotenv = require("dotenv");
const bcrypt = require("bcryptjs");
const fs = require("fs");

const countries = require("./public/data/countries");
// const files = require("./private/data/miami_data.json");
// const blacklistspa = require("./private/data/api_black-list-spa.json");
// const blacklisteng = require("./private/data/api_black-list-eng.json");

const User = require("./models/user");
const UserAudit = require("./models/userAudit");
const UserReasonsReport = require("./models/userReasonsReport");
const UserTracking = require("./models/userTracking");
const Country = require("./models/country");
const Category = require("./models/category");
const Subcategory = require("./models/subcategory");
const BlackList = require("./models/blacklist");
const Tag = require("./models/tag");
const Business = require("./models/business");
const BusinessAudit = require("./models/businessAudit");
const BusinessReasonsReport = require("./models/businessReasonsReport");
const Recommendation = require("./models/recommendation");
const RecommendationAudit = require("./models/recommendationAudit");
const RecommendationMetric = require("./models/recommendationMetric");
const Request = require("./models/request");
const Answer = require("./models/answer");
const Comment = require("./models/comment");
const Notification = require("./models/notification");
const Report = require("./models/report");
const Behavior = require("./models/behavior");
const AdminAudit = require("./models/adminAudit");
const Global = require("./models/global");
const Claim = require("./models/claim");
const Token = require("./models/token");
const ErrorAudit = require("./models/errorAudit");
const Social = require("./models/social");

const connectDB = require("./config/db");

dotenv.config();

connectDB();

const importDataFromCountries = async () => {
  try {
    await Country.insertMany(countries);

    console.log("Countries imported!");
    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromUsers = async () => {
  try {
    const users = [];

    const salt = await bcrypt.genSalt(10);

    const userPassword = await bcrypt.hash("123456", salt);

    for (let index = 0; index < 4; index++) {
      users.push({
        username: `atler ${index}`,
        email: `atler${index}@localsatlas.com`,
        password: userPassword,
        mainLocation: {
          type: "Point",
          coordinates: [Number(Math.random() - 81), Number(Math.random() + 25)],
        },
        optionalLocation01: {
          type: "Point",
          coordinates: [Number(Math.random() - 81), Number(Math.random() + 25)],
        },
        optionalLocation02: {
          type: "Point",
          coordinates: [Number(Math.random() - 81), Number(Math.random() + 25)],
        },
        isActive: true,
        authenticationMethod: 1,
        profile: {
          image: "public/images/system/user.png",
          firstname: "atler",
          lastname: `${index}`,
          phone: `${Math.floor(Math.random() * 10000000000)}`,
        },
      });
    }

    await User.insertMany(users);

    console.log("Users imported successfully");
    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromBusiness = async () => {
  try {
    const user = await User.find({ username: "Super administrator" });

    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      const business = [];

      let file = JSON.parse(
        fs.readFileSync(`./private/data/${element.nameFile}`, "utf-8")
      );

      file.forEach((element) => {
        business.push({
          userCreator: user[0]._id,
          ownerUser: user[0]._id,
          image: "public/images/system/business.png",
          name: element.Business_Name ? element.Business_Name : "",
          mainAddress: element.Address ? element.Address : "",
          location: {
            type: "Point",
            coordinates: [element.longitude, element.latitude],
          },
          web: element.PrimaryWebURL ? element.PrimaryWebURL : "",
          status: 1,
          behavior: 1,
          isActive: true,
        });
      });

      await Business.insertMany(business);

      console.log(`Businesses imported from ${element.nameFile}`);
    }

    console.log("Businesses imported successfully");
    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromBusinessCreatedManually = async () => {
  try {
    const user = await User.find({ username: "SuperAdministrator" });

    const business = [];

    for (let index = 0; index < 1000; index++) {
      business.push({
        userCreator: user[0]._id,
        ownerUser: user[0]._id,
        image: "public/images/system/business.png",
        name: `business ${index + 1}`,
        mainAddress: `business ${index + 1}`,
        location: {
          type: "Point",
          coordinates: [Number(Math.random() - 79), Number(Math.random() - 2)],
        },
        web: `https://www.business${index + 1}.com`,
        status: 1,
        behavior: 1,
        isActive: true,
      });
    }

    await Business.insertMany(business);

    console.log("Businesses created successfully");
    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromBlackList = async () => {
  try {
    let file = JSON.parse(
      fs.readFileSync(`./private/data/api_black-list-spa.json`, "utf-8")
    );

    const filteredFile = file.filter(function (item, index) {
      if (file.indexOf(item) == index) return item;
    });

    const badwords = [];

    filteredFile.forEach((element) => {
      badwords.push({
        word: element,
      });
    });

    await BlackList.insertMany(badwords);
    console.log("Badwords imported successfully");

    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromCategories = async () => {
  try {
    const categories = [];

    let file = JSON.parse(
      fs.readFileSync(`./private/data/api_categories.json`, "utf-8")
    );

    file.forEach((element) => {
      categories.push({
        name: element.category,
      });
    });

    await Category.insertMany(categories);

    console.log("Categories imported successfully");

    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromReasonsUsers = async () => {
  try {
    const reasons = [];

    let file = JSON.parse(
      fs.readFileSync(`./private/data/api_report_reasons_user.json`, "utf-8")
    );

    file.forEach((element) => {
      reasons.push({
        description: element.reason,
      });
    });

    await UserReasonsReport.insertMany(reasons);

    console.log("User reason report imported successfully");

    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const importDataFromReasonsBusiness = async () => {
  try {
    const reasons = [];

    let file = JSON.parse(
      fs.readFileSync(
        `./private/data/api_report_reasons_business.json`,
        "utf-8"
      )
    );

    file.forEach((element) => {
      reasons.push({
        description: element.reason,
      });
    });

    await BusinessReasonsReport.insertMany(reasons);

    console.log("User reason report imported successfully");

    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const addFieldToBusiness = async () => {
  try {
    await Business.updateMany({}, { $set: { socialLinks: [] } });
    console.log("Field added successfully");

    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const addFieldToUser = async () => {
  try {
    await User.updateMany({}, { $set: { status: 1 } });
    console.log("Field added successfully");

    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const deleteDataCategoryUser = async () => {
  try {
    await User.updateMany({}, { $set: { "profile.mainInterests": [] } });
    console.log("Field category updated successfully");
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

const destroyData = async () => {
  try {
    await User.deleteMany();
    // await BlackList.deleteMany();
    // await UserReasonsReport.deleteMany();
    // await BusinessReasonsReport.deleteMany();
    await Global.deleteMany();
    // await Category.deleteMany();
    // await Subcategory.deleteMany();
    // await Social.deleteMany();
    //*********ALLOW TO DELETE********* */
    await UserAudit.deleteMany();
    await UserTracking.deleteMany();
    await Answer.deleteMany();
    await Request.deleteMany();
    await Recommendation.deleteMany();
    await RecommendationAudit.deleteMany();
    await RecommendationMetric.deleteMany();
    await Tag.deleteMany();
    await Business.deleteMany();
    await BusinessAudit.deleteMany();
    await Notification.deleteMany();
    await Comment.deleteMany();
    await AdminAudit.deleteMany();
    await Report.deleteMany();
    await Behavior.deleteMany();
    await Claim.deleteMany();
    await Token.deleteMany();
    await ErrorAudit.deleteMany();

    console.log("Data Destroyed!");
    process.exit();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

if (process.argv[2] === "-d") {
  destroyData();
} else {
  // importDataFromCountries();
  // importDataFromUsers();
  // importDataFromBlackList();
  // importDataFromCategories();
  // importDataFromReasonsBusiness();
  // importDataFromReasonsUsers();
  // importDataFromBusiness();
  // importDataFromBusinessCreatedManually();
  // addFieldToBusiness();
  // addFieldToUser();
  deleteDataCategoryUser();
}
