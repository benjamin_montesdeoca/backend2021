const { URL } = require("url");

exports.stringIsAValidUrl = (string, protocols) => {
  try {
    const url = new URL(string);
    return protocols
      ? url.protocol
        ? protocols.map((x) => `${x.toLowerCase()}:`).includes(url.protocol)
        : false
      : true;
  } catch (error) {
    return false;
  }
};

exports.stringIsSocialUrl = (string) => {
  let isValid = false;

  if (string.includes("tiktok")) {
    return (isValid = true);
  } else if (string.includes("facebook")) {
    return (isValid = true);
  } else if (string.includes("twitter")) {
    return (isValid = true);
  } else if (string.includes("instagram")) {
    return (isValid = true);
  } else {
    return (isValid = false);
  }
};
