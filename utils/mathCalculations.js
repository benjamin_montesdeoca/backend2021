exports.generateCode = () => {
  const code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
  return code;
};

exports.getMilles = (lat1, lon1, lat2, lon2) => {
  function rad(x) {
    return (x * Math.PI) / 180;
  }
  const R = 6378.137;
  let dLat = rad(lat2 - lat1);
  let dLong = rad(lon2 - lon1);
  let a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(rad(lat1)) *
      Math.cos(rad(lat2)) *
      Math.sin(dLong / 2) *
      Math.sin(dLong / 2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = R * c;
  d = d * 0.621371;
  return d;
};

exports.completeMonthsForYear = (yearArray) => {
  let len = yearArray.length;

  if (len <= 1) {
    return yearArray;
  }

  let auxArray = [];

  for (let i = 1; i < len; i++) {
    let diff = yearArray[i]._id - yearArray[i - 1]._id;

    if (diff > 1) {
      let val = 0;
      diff--;

      for (let j = 0; j < diff; j++) {
        val = yearArray[i - 1]._id + j + 1;
        auxArray.push({ _id: val, y: 0 });
      }
    }

    auxArray.push({ _id: yearArray[i]._id, y: yearArray[i].y });
  }

  return auxArray;
};

exports.completeWeeksForYear = (yearArray) => {
  let len = yearArray.length;

  if (len <= 1) {
    return yearArray;
  }

  let auxArray = [];

  for (let i = 1; i < len; i++) {
    let diff = yearArray[i]._id - yearArray[i - 1]._id;

    if (diff > 1) {
      let val = 0;
      diff--;

      for (let j = 0; j < diff; j++) {
        val = yearArray[i - 1]._id + j + 1;
        auxArray.push({ _id: val, y: 0 });
      }
    }

    auxArray.push({ _id: yearArray[i]._id, y: yearArray[i].y });
  }

  return auxArray;
};
