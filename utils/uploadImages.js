const path = require("path");
const multer = require("multer");

const profileImageStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, "public/images/profiles");
  },
  filename(req, file, cb) {
    cb(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

const businessImageStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, "public/images/businesses");
  },
  filename(req, file, cb) {
    cb(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

function checkFileType(file, cb) {
  const filetypes = /jpg|jpeg|png/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  let uploadError = new Error("Invalid image type");

  if (extname && mimetype) {
    return cb(null, true);
  } else {
    return cb(uploadError, true);
  }
}

exports.uploadProfileImage = multer({
  storage: profileImageStorage,
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});

exports.uploadBusinessImage = multer({
  storage: businessImageStorage,
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});
